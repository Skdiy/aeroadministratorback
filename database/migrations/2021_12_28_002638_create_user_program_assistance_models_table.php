<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserProgramAssistanceModelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_program_assistance', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('user_program_id')->unsigned();
            $table->bigInteger('program_class_id')->unsigned();
            $table->integer('assistance')->unsigned()->nullable();
            $table->integer('hours')->unsigned()->nullable();
            $table->timestamps();

            $table->foreign('user_program_id')->references('id')
            ->on('user_program_models')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('program_class_id')->references('id')
            ->on('program_class')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_program_assistance');
    }
}
