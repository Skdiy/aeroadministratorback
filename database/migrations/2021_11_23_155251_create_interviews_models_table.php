<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInterviewsModelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('interviews', function (Blueprint $table) {
            $table->id();

            //Empresa
            $table->string('razonSocial')->nullable();
            $table->string('pais')->nullable();
            $table->string('ciudad')->nullable();
            $table->string('cargo')->nullable();
            $table->text('description')->nullable();
            $table->string('category')->nullable();
            $table->date('dateIni')->nullable();
            $table->integer('spots')->nullable();
            $table->boolean('experience')->nullable();
            $table->string('Levelexperience')->nullable();

            //Carrer
            $table->string('typeDoc');
            $table->integer('document');
            $table->string('lastname1')->nullable();
            $table->string('lastname2')->nullable();
            $table->string('names');
            $table->date('birthday')->nullable();
            $table->string('sex')->nullable();
            $table->string('nacionality')->nullable();
            $table->string('adress')->nullable();
            $table->string('urbanization')->nullable();
            $table->string('district')->nullable();
            $table->string('province')->nullable();
            $table->string('homephone');
            $table->string('phone');
            $table->string('civilStatus')->nullable();
            $table->string('email');

            //Representante legal
            $table->string('relationAp')->nullable();
            $table->integer('documentAp')->nullable();
            $table->string('lastname1Ap')->nullable();
            $table->string('lastname2Ap')->nullable();
            $table->string('nameAp')->nullable();
            $table->date('birthdayAp')->nullable();
            $table->string('ocupationAp')->nullable();
            $table->string('nacionalityAp')->nullable();
            $table->string('emailAp')->nullable();
            $table->string('phoneAp')->nullable();
            $table->integer('type');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('interviews');
    }
}
