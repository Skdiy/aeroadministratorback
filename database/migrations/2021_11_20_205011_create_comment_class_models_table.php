<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCommentClassModelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('comment_class', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('class_real_time_id')->unsigned();
            $table->bigInteger('user_id')->unsigned();
            $table->string('comment')->default('');
            $table->integer('type')->default(1);
            $table->timestamps();

            $table->foreign('class_real_time_id')->references('id')
                ->on('class_real_times')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('user_id')->references('id')
                ->on('users')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('comment_class');
    }
}
