<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProyectsCoursesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('proyects_courses', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('course_id')->unsigned();
            $table->string('title');
            $table->text('description');
            $table->text('descriptionProyect');
            $table->timestamps();

            $table->foreign('course_id')->references('id')
                ->on('courses')->onUpdate('cascade')->onDelete('cascade');
            /* $table->foreign('resource_id')->references('id')
                ->on('courses')->onUpdate('cascade')->onDelete('cascade'); */
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('proyects_courses');
    }
}
