<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateZoomMeetingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('zoom_meetings', function (Blueprint $table) {
            $table->id();
            $table->string('topic');
            $table->dateTime('start_time');
            $table->integer('duration');
            $table->string('agenda');
            $table->boolean('host_video')->default(false);
            $table->boolean('participant_video')->default(false);
            $table->bigInteger('zoom_id');
            $table->string('join_url');
            $table->string('password');
            $table->timestamps();


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('zoom_meetings');
    }
}
