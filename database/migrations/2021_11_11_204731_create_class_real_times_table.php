<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateClassRealTimesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('class_real_times', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('course_id')->unsigned();
            $table->dateTime('nextclass');
            $table->bigInteger('zoom_id')->unsigned();;
            $table->timestamps();

            $table->foreign('course_id')->references('id')
                ->on('courses')->onUpdate('cascade')->onDelete('cascade');

            $table->foreign('zoom_id')->references('id')
                ->on('zoom_meetings')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('class_real_times');
    }
}
