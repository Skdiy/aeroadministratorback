<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEventSubscriptionModelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('event_subscription', function (Blueprint $table) {
            $table->id();
            $table->string('bussines');
            $table->string('country');
            $table->string('city');
            $table->string('phone');
            $table->string('web');
            $table->string('rubro');
            $table->string('code');
            $table->string('representante');
            $table->string('cargo');
            $table->string('contact_number');
            $table->string('email');
            $table->string('others')->nullable();
            $table->integer('integrants');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('event_subscription');
    }
}
