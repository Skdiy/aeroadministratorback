<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateClassTemariesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('class_temaries', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('temary_id')->unsigned();
            $table->string('name');
            $table->string('state');
            $table->string('urlRecord');
            $table->timestamps();

            $table->foreign('temary_id')->references('id')
                ->on('temary_courses')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('class_temaries');
    }
}
