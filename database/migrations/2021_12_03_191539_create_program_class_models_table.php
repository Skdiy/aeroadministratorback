<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProgramClassModelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('program_class', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('program_temary_id')->unsigned();
            $table->string('name');
            $table->string('state');
            $table->string('urlRecord');
            $table->string('teacher');
            $table->string('description');
            $table->string('dateIni');
            $table->string('dateEnd');
            $table->bigInteger('zoom_id')->unsigned()->nullable();
            $table->timestamps();

            $table->foreign('zoom_id')->references('id')
                ->on('zoom_meetings')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('program_temary_id')->references('id')
                ->on('program_temary')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('program_class');
    }
}
