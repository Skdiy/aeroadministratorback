<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProyectsUserModelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('proyects_user_models', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('proyect_id')->unsigned();
            $table->bigInteger('file_id')->unsigned();
            $table->bigInteger('user_id')->unsigned();
            $table->text('response');
            $table->boolean('status')->default(false);
            $table->timestamps();

            $table->foreign('user_id')->references('id')
                ->on('users')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('proyect_id')->references('id')
                ->on('proyects_courses')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('file_id')->references('id')
                ->on('files')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('proyects_user_models');
    }
}
