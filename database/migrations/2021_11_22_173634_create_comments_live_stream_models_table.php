<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCommentsLiveStreamModelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('comments_live_stream', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('live_stream_id')->unsigned();
            $table->bigInteger('user_id')->unsigned();
            $table->string('comment')->default('');
            $table->integer('type')->default(1);
            $table->timestamps();

            $table->foreign('live_stream_id')->references('id')
                ->on('live_stream')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('user_id')->references('id')
                ->on('users')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('comments_live_stream');
    }
}
