<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProyectResourcesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('proyect_resources', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('proyect_id')->unsigned();
            $table->bigInteger('file_id')->unsigned();
            $table->timestamps();

            $table->foreign('proyect_id')->references('id')
                ->on('proyects_courses')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('file_id')->references('id')
                ->on('files')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('proyect_resources');
    }
}
