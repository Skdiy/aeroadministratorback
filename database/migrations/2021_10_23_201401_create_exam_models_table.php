<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateExamModelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('exam', function (Blueprint $table) {
            $table->id();
            $table->string('title');
            $table->bigInteger('course_id')->unsigned();
            $table->date('DateInit');
            $table->date('DateEnd');
            $table->integer('duration');
            $table->integer('maxNote');
            $table->boolean('state');
            $table->timestamps();

            $table->foreign('course_id')->references('id')
                ->on('courses')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('exam');
    }
}
