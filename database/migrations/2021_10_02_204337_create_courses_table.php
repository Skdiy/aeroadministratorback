<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCoursesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('courses', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->Time('timeClass');
            $table->date('date_ini')->nullable();
            $table->integer('spots')->nullable();
            $table->string('level');
            $table->text('description');
            $table->integer('minAVG')->default(80);
            $table->string('abstract');
            $table->string('price');
            $table->string('knowledge');
            $table->string('language');
            $table->string('video');
            $table->string('image');
            $table->string('type');
            $table->bigInteger('teacher_id')->unsigned();
            $table->timestamps();

            $table->foreign('teacher_id')->references('id')
                ->on('users')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('courses');
    }
}
