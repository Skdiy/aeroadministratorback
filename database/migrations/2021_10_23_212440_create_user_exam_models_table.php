<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserExamModelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_exam_models', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('user_id')->unsigned();
            $table->bigInteger('exam_id')->unsigned();
            $table->bigInteger('exam_questions_id')->unsigned();
            $table->text("answer")->nullable();
            $table->text("comment")->nullable();
            $table->integer("points")->default(0);
            $table->timestamps();

            $table->foreign('exam_id')->references('id')
            ->on('exam')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('exam_questions_id')->references('id')
                ->on('exam_questions')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('user_id')->references('id')
                ->on('users')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_exam_models');
    }
}
