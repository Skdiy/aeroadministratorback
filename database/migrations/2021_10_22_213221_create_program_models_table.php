<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProgramModelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('program', function (Blueprint $table) {
            $table->id();
            $table->string('certificate');
            $table->time('duration');
            $table->date('dateIni');
            $table->string('title');
            $table->string('videoUrl');
            $table->longText('description');
            $table->decimal('price');
            $table->string('language');
            $table->string('abstract');
            $table->integer('spots');
            $table->longText('requeriments');
            $table->bigInteger('teacher_id')->unsigned();
            $table->timestamps();

            $table->foreign('teacher_id')->references('id')
            ->on('users')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('program');
    }
}
