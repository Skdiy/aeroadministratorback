<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateExamQuestionsAlternativesModelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('exam_questions_alternatives', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('exam_questions_id')->unsigned();
            $table->string("alternative");
            $table->timestamps();

            $table->foreign('exam_questions_id')->references('id')
                ->on('exam_questions')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('exam_questions_alternatives');
    }
}
