<?php

namespace Database\Seeders;

use App\Models\BlogCategoryModel;
use Illuminate\Database\Seeder;

class BlogCategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [];
        array_push($data, [
            'name' => 'Análisis de Marcas',
        ]);
        array_push($data, [
            'name' => 'Branding',
        ]);
        array_push($data, [
            'name' => 'Recursos de diseño',
        ]);
        BlogCategoryModel::insert($data);
    }
}
