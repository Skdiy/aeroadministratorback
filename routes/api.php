<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\BlogController;
use App\Http\Controllers\CertificateController;
use App\Http\Controllers\CommentsController;
use App\Http\Controllers\ContractController;
use App\Http\Controllers\CourseController;
use App\Http\Controllers\ExamController;
use App\Http\Controllers\FeriaController;
use App\Http\Controllers\FrequentQuestionsController;
use App\Http\Controllers\LiveStreamController;
use App\Http\Controllers\MeetingController;
use App\Http\Controllers\PaymentController;
use App\Http\Controllers\ProgramController;
use App\Http\Controllers\PublicityController;
use App\Http\Controllers\RealTimeController;
use App\Http\Controllers\TemaryController;
use App\Models\CommentClassModel;
use App\Models\NotesModel;
use App\Models\ProyectsCourse;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//AUTENTIFICACION
Route::group(['prefix' => 'auth'], function () {
    Route::post('login', [AuthController::class, 'login']);
    Route::post('register', [AuthController::class, 'register']);
    Route::post('register/teacher', [AuthController::class, 'register_teacher'])->middleware('jwt.auth');
    Route::post('logout', [AuthController::class, 'logout']);
    Route::get('refresh', [AuthController::class, 'refresh']);
    Route::get('user-profile', [AuthController::class, 'userProfile']);
    Route::get('createNewToken', [AuthController::class, 'createNewToken']);

    Route::get('course', [CourseController::class, 'getUserCourse']);
    Route::get('program', [ProgramController::class, 'getUserCourse']);
    Route::patch('update', [AuthController::class, 'update']);

    Route::post('payment', [PaymentController::class, 'add']);
    Route::get('payment', [PaymentController::class, 'getPayment']);
});
//RUTAS ADMINISTRATIVAS
Route::group(['prefix' => 'admi','middleware' => 'jwt.auth'], function () {
    Route::group(['prefix' => 'list'], function () {
        Route::get('users', [AuthController::class, 'list_users']);
        Route::get('teachers', [AuthController::class, 'list_teachers']);
        Route::get('business', [AuthController::class, 'list_business']);
        Route::get('status/{user_id}', [AuthController::class, 'change_status']);
    });
    //FERIA
    Route::group(['prefix' => 'event'], function () {
        Route::post('bussines/form', [FeriaController::class, 'create_form']);
        Route::post('bussines/catalogue', [FeriaController::class, 'create_catalogue']);
        Route::post('bussines/info', [FeriaController::class, 'bussiness_info']);

        Route::post('contact/part', [FeriaController::class, 'create_contact_part']);
        Route::post('contact/info', [FeriaController::class, 'update_contact_info']);

        Route::post('delegation/form', [FeriaController::class, 'create_delegation_form']);
        Route::post('delegation/conference', [FeriaController::class, 'create_delegation_conference']);
        Route::post('delegation/info', [FeriaController::class, 'update_delegation_info']);

        Route::post('question', [FeriaController::class, 'create_question']);

        Route::post('sponsor', [FeriaController::class, 'create_sponsor']);

        Route::post('subscription', [FeriaController::class, 'create_subscription']);

        Route::post('notice', [FeriaController::class, 'create_notice']);
    });

    //CONTRACTS TEACHER
    Route::group(['prefix' => 'teacher'], function () {
        Route::post('contract', [ContractController::class, 'create_contract']);
        Route::post('contract/pay', [ContractController::class, 'create_contract_pay']);

    });


    //COURSES
    Route::group(['prefix' => 'course'], function () {
        Route::post('', [CourseController::class, 'add']);
        Route::post('class', [CourseController::class, 'addResource']);
        Route::post('definitions', [CourseController::class, 'addDefinition']);
        Route::patch('', [CourseController::class, 'update']);
        Route::delete('{course_id}', [CourseController::class, 'delete']);
        //TEMARY
        Route::group(['prefix' => 'temary'], function () {
            Route::post('{course_id}', [TemaryController::class, 'add']);
            Route::post('class/{teamry_id}', [TemaryController::class, 'addClass']);
            Route::post('class/{class_id}', [TemaryController::class, 'aupdateClass']);
        });
        //QUESTIONS
        Route::group(['prefix' => 'questions'], function () {
            Route::post('', [FrequentQuestionsController::class, 'store']);
        });
        Route::group(['prefix' => 'realtime'], function () {

            //COMMENTS IN REAL CLASS
            Route::group(['prefix' => 'comment'], function () {
                Route::get('{course_id}', [RealTimeController::class, 'get_comments']);
                Route::post('{course_id}', [RealTimeController::class, 'add_comment']);
                Route::delete('', [RealTimeController::class, 'delete_comment']);
            });
            Route::group(['prefix' => 'notes'], function () {
                Route::get('{course_id}', [RealTimeController::class, 'get_apunte']);
                Route::post('{course_id}', [RealTimeController::class, 'add_apunte']);
                Route::delete('', [RealTimeController::class, 'delete_apunte']);
            });
        });
    });
    //PROGRAMS
    Route::group(['prefix' => 'program'], function () {
        Route::post('', [ProgramController::class, 'add']);
        Route::patch('', [ProgramController::class, 'update']);
        Route::delete('{program_id}', [ProgramController::class, 'delete']);
        //TEMARY
        Route::group(['prefix' => 'temary'], function () {
            Route::post('{program_id}', [TemaryController::class, 'add_temary_program']);
            Route::post('class/{teamry_id}', [TemaryController::class, 'addClass_program']);
            Route::post('class/{class_id}', [TemaryController::class, 'updateClass_program']);
        });
        //QUESTIONS
        Route::group(['prefix' => 'questions'], function () {
            Route::post('', [FrequentQuestionsController::class, 'store_program']);
        });
    });
    //PUBLICITY
    Route::group(['prefix' => 'publicity'], function () {
        Route::post('', [PublicityController::class, 'store']);
        Route::post('opinion', [PublicityController::class, 'storeOpinion']);
    });
    //EXAMS
    Route::group(['prefix' => 'exam'], function () {
        Route::post('', [ExamController::class, 'add']);
        Route::post('{exam_id}', [ExamController::class, 'addQuestion']);
    });
    //NOTES
    Route::group(['prefix' => 'notes'], function () {
        Route::get('{course_id}', [NotesModel::class, 'getAllusers']);
        Route::post('user', [NotesModel::class, 'addNote']);
    });
    //PROYECTS
    Route::group(['prefix' => 'proyects'], function () {
        Route::get('{course_id}', [ProyectsCourse::class, 'getAllProyects']);
        Route::get('resources/{proyect_id}', [ProyectsCourse::class, 'getResourcesProyects']);
        Route::get('user/{course_id}/{user_id}', [ProyectsCourse::class, 'getUserProyects']);
        Route::post('', [ProyectsCourse::class, 'addProyect']);
        Route::post('rate', [ProyectsCourse::class, 'RateProyect']);
    });
    //MEETINGS
    Route::group(['prefix' => 'meeting'], function () {
        Route::post('', [MeetingController::class, 'store']);
        Route::patch('/{id}', [MeetingController::class, 'updateMeet']);
        Route::get('/{id}', [MeetingController::class, 'get']);
    });
    //BLOG
    Route::group(['prefix' => 'blog'], function () {
        Route::post('', [BlogController::class, 'create_blog']);
    });
    //CERTIFICATE
    Route::group(['prefix' => 'certificate'], function () {
        Route::post('', [CertificateController::class, 'create_certificate']);
    });
    //LIVESTREAM
    Route::group(['prefix' => 'liveStream'], function () {
        Route::post('', [LiveStreamController::class, 'store']);
        Route::post('video', [LiveStreamController::class, 'upload_video']);

    });
    //REALTIME
    Route::group(['prefix' => 'realtime'], function () {
        Route::post('{course_id}', [RealTimeController::class, 'store']);
    });
});

//COURSES
Route::group(['prefix' => 'course'], function () {
    //SEARCH
    Route::get('search/{search}', [CourseController::class, 'Search']);
    Route::get('searchAll/{search}', [CourseController::class, 'SearchAll']);

    Route::get('class/{class_id}', [CourseController::class, 'getClass']);

    Route::get('', [CourseController::class, 'get']);
    Route::get('home', [CourseController::class, 'getHome']);
    Route::get('{course_id}', [CourseController::class, 'getCourse']);
    Route::post('registerUser', [CourseController::class, 'RegisterUser']);
    //TEMARY
    Route::group(['prefix' => 'temary'], function () {
        Route::get('{course_id}', [TemaryController::class, 'get']);

    });
    //QUESTIONS
    Route::group(['prefix' => 'questions'], function () {
        Route::get('{course_id}', [FrequentQuestionsController::class, 'get']);
    });
    //ASSESSMENTS
    Route::group(['prefix' => 'AssesmentCourse'], function () {
        Route::get('{course_id}', [CourseController::class, 'GetAssesmentCourse']);
        Route::post('', [CourseController::class, 'AssesmentCourse']);
    });
});

//PROGRAM
Route::group(['prefix' => 'program'], function () {
    //SEARCH
    Route::get('search/{search}', [ProgramController::class, 'Search']);
    Route::get('searchAll/{search}', [ProgramController::class, 'SearchAll']);

    Route::get('', [ProgramController::class, 'get']);
    Route::get('home', [ProgramController::class, 'getHome']);
    Route::get('{program_id}', [ProgramController::class, 'getProgram']);
    Route::get('my/{program_id}', [ProgramController::class, 'getMyProgram'])->middleware('jwt.auth');
    Route::post('registerUser', [ProgramController::class, 'RegisterUser']);
    //TEMARY
    Route::group(['prefix' => 'temary'], function () {
        Route::get('{program_id}', [TemaryController::class, 'get_temary_program']);

    });
    //QUESTIONS
    Route::group(['prefix' => 'questions'], function () {
        Route::get('{program_id}', [FrequentQuestionsController::class, 'get_program']);
    });

});

//PUBLICITY
Route::group(['prefix' => 'publicity'], function () {
    Route::get('carousel', [PublicityController::class, 'getCarousel']);
    Route::get('opinions', [PublicityController::class, 'getUserOpinion']);
});

Route::group(['prefix' => 'publicity'], function () {
    Route::get('carousel/{course_id}', [CertificateController::class, 'validate']);
});

//EXAMS
Route::group(['prefix' => 'exam','middleware' => 'jwt.auth'], function () {
    Route::get('', [ExamController::class, 'get']);
    Route::get('{exam_id}', [ExamController::class, 'getExam']);
});

//NOTES
Route::group(['prefix' => 'notes'], function () {
    Route::get('{course_id}', [NotesModel::class, 'getMynotes']);
    Route::get('user/{course_id}/{user_id}', [NotesModel::class, 'getUsernotes']);
});

//PROYECTS
Route::group(['prefix' => 'proyects','middleware' => 'jwt.auth'], function () {
    Route::get('user/{course_id}', [ProyectsCourse::class, 'getMyProyects']);
    Route::post('', [ProyectsCourse::class, 'addResponseProyect']);
});

//BLOG
Route::group(['prefix' => 'blog'], function () {
    Route::get('', [BlogController::class, 'get_home']);
    Route::get('{blog_id}', [BlogController::class, 'get_blog']);
});

//CERTIFICATE
Route::group(['prefix' => 'certificate'], function () {
    Route::get('{course_id}', [CertificateController::class, 'validate']);
});

//LIVESTREAM
Route::group(['prefix' => 'liveStream'], function () {
    Route::get('', [LiveStreamController::class, 'get_recent']);
    Route::get('all', [LiveStreamController::class, 'get_all']);
    Route::get('{live_stream_id}', [LiveStreamController::class, 'get']);
    Route::get('view/{live_stream_id}}', [LiveStreamController::class, 'add_view']);
    Route::group(['prefix' => 'comment'], function () {
        Route::post('{liveStream_id}', [CommentsController::class, 'store'])->middleware('jwt.auth');
    });
});


//REALTIME
Route::get('/realtime/{course_id}', [RealTimeController::class, 'getCarousel']);
Route::get('/realtime/course/{course_id}', [ExamController::class, 'getExamCourse']);
Route::group(['prefix' => 'realtime','middleware' => 'jwt.auth'], function () {
    Route::get('comment/{course_id}', [RealTimeController::class, 'get_comments']);
    Route::post('comment/{course_id}', [RealTimeController::class, 'add_comment']);
    Route::delete('comment/{comment_id}', [RealTimeController::class, 'delete_comment']);

    Route::get('apuntes/{course_id}', [RealTimeController::class, 'get_apunte']);
    Route::post('apuntes/{course_id}', [RealTimeController::class, 'add_apunte']);
    Route::delete('apuntes/{apuntes_id}', [RealTimeController::class, 'delete_apunte']);

});






//EVENT
Route::group(['prefix' => 'event'], function () {
    Route::get('bussines/form', [FeriaController::class, 'get_form']);
    Route::get('bussines/catalogue', [FeriaController::class, 'get_catalogue']);
    Route::get('bussines/info', [FeriaController::class, 'get_info']);

    Route::get('contact/part', [FeriaController::class, 'get_contact_part']);
    Route::get('contact/info', [FeriaController::class, 'get_contact_info']);

    Route::get('delegation/conference', [FeriaController::class, 'get_delegation_conference']);
    Route::get('delegation/form', [FeriaController::class, 'get_delegation_form']);
    Route::get('delegation/info', [FeriaController::class, 'get_delegation_info']);

    Route::get('question', [FeriaController::class, 'get_questions']);

    Route::get('sponsor', [FeriaController::class, 'get_sponsor']);

    Route::get('subscription', [FeriaController::class, 'get_subscription']);

    Route::get('notice', [FeriaController::class, 'get_notice']);
});


//CONTRACTS TEACHER
Route::group(['prefix' => 'teacher', 'middleware' => 'jwt.auth'], function () {
    Route::get('contract', [ContractController::class, 'get_Contract']);
    Route::get('contract/pay', [ContractController::class, 'get_payments']);

});
