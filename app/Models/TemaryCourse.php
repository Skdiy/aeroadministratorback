<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TemaryCourse extends Model
{
    use HasFactory;

    protected $table = 'temary_courses';

    protected $primaryKey = 'id';

    public $incrementing = true;
    public $timestamps = true;

    protected $fillable = [
        'course_id',
        'name',
        'state',
    ];
    protected $casts = [
        'course_id' => 'integer',
        'name' => 'string',
        'state' => 'boolean',
    ];
}
