<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProgramModel extends Model
{
    use HasFactory;
    protected $table = 'program';

    protected $primaryKey = 'id';

    public $incrementing = true;
    public $timestamps = true;

    protected $fillable = [
        'certificate',
        'duration',
        'dateIni',
        'title',
        'videoUrl',
        'description',
        'price',
        'language',
        'abstract',
        'spots',
        'requeriments',
        'teacher_id'
    ];
    protected $casts = [
        'teacher_id' => 'integer',
        'title' => 'string',
        'certificate' => 'string',
        'duration' => 'datetime:H:i',
        'dateIni' => 'date',
        'videoUrl' => 'string',
        'description' => 'string',
        'price' => 'integer',
        'language' => 'string',
        'abstract' => 'string',
        'spots' => 'integer',
        'requeriments' => 'string',
    ];
}
