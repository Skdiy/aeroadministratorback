<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EventBusinessFormModel extends Model
{
    use HasFactory;

    protected $table = 'event_business_form';

    protected $primaryKey = 'id';

    public $incrementing = true;
    public $timestamps = true;

    protected $fillable = [
        'country',
        'name',
        'email',
        'phone',
        'message',
    ];
    protected $casts = [
        'country' => 'string',
        'name' => 'string',
        'email' => 'string',
        'phone' => 'string',
        'message' => 'string',
    ];
}
