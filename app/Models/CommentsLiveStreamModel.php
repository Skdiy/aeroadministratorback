<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CommentsLiveStreamModel extends Model
{
    use HasFactory;

    protected $table = 'comments_live_stream';

    const APUNTE = 1;
    const COMMENT = 2;

    protected $primaryKey = 'id';

    public $incrementing = true;
    public $timestamps = true;

    protected $fillable = [
        'live_stream_id',
        'user_id',
        'comment',
        'type'
    ];
    protected $casts = [
        'live_stream_id' => 'integer',
        'user_id' => 'integer',
        'comment' => 'string',
        'type' => 'integer'
    ];
}
