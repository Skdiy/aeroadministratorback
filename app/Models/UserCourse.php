<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserCourse extends Model
{
    use HasFactory;

    protected $table = 'user_courses';

    protected $primaryKey = 'id';

    public $incrementing = true;
    public $timestamps = true;

    protected $fillable = [
        'user_id',
        'course_id',
        'codPay'
    ];
    protected $casts = [
        'user_id' => 'integer',
        'course_id' => 'integer',
        'codPay' => 'string'
    ];
}
