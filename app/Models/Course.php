<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Course extends Model
{
    use HasFactory;

    const NORMAL = "normal";
    const REALTIME = "real_time";

    const BASICO = "Basico";
    const INTERMEDIO = "Intermedio";
    const AVANZADO = "Avanzado";

    protected $table = 'courses';

    protected $primaryKey = 'id';

    public $incrementing = true;
    public $timestamps = true;

    protected $fillable = [
        'name',
        'timeClass',
        'date_ini',
        'spots',
        'level',
        'description',
        'abstract',
        'price',
        'knowledge',
        'language',
        'video',
        'image',
        'type',
        'teacher_id',
        'minAVG'
    ];
    protected $casts = [
        'name' => 'string',
        'timeClass' => 'datetime:H:i',
        'spots' => 'integer',
        'level' => 'string',
        'description' => 'string',
        'abstract' => 'string',
        'price' => 'string',
        'knowledge' => 'string',
        'language' => 'string',
        'video' => 'string',
        'image' => 'string',
        'type' => 'string',
        'teacher_id' => 'integer',
        'minAVG' => 'integer'
    ];

    /*
    |------------------------------------------------------------------------------------
    | Validations
    |------------------------------------------------------------------------------------
    */
    public static function rules($update = false, $id=null)
    {
        return [
            'name' => 'required',
            'timeClass' => 'required',
            'spots' => 'required',
            'level' => 'required',
            'description' => 'required',
            'abstract' => 'required',
            'price' => 'required',
            'knowledge' => 'required',
            'language' => 'required',
            'type' => 'required',
            'teacher_id' => 'required',
            'video' => 'required|video|mimes:mp4',
        ];
    }

    /*
    |------------------------------------------------------------------------------------
    | Relations
    |------------------------------------------------------------------------------------
    */

    /*
    |------------------------------------------------------------------------------------
    | Scopes
    |------------------------------------------------------------------------------------
    */

    /*
    |------------------------------------------------------------------------------------
    | Attributes
    |------------------------------------------------------------------------------------
    */
}
