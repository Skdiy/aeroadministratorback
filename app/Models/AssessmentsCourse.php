<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AssessmentsCourse extends Model
{
    use HasFactory;

    protected $table = 'assessments_courses';

    protected $primaryKey = 'id';

    public $incrementing = true;
    public $timestamps = true;

    protected $fillable = [
        'course_id',
        'user_id',
        'assessment',
        'comment',
    ];
    protected $casts = [
        'course_id' => 'integer',
        'user_id' => 'integer',
        'assessment' => 'decimal',
        'comment' => 'string',
    ];
}
