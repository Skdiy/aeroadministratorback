<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LiveStreamModel extends Model
{
    use HasFactory;

    protected $table = 'live_stream';

    const SININICIAR = 1;
    const FINALIZADO = 2;


    protected $primaryKey = 'id';

    public $incrementing = true;
    public $timestamps = true;

    protected $fillable = [
        'title',
        'description',
        'photo',
        'url',
        'teacher_id',
        'date',
        'time',
        'views',
        'video',
        'status',
    ];
    protected $casts = [
        'title' => 'string',
        'description' => 'string',
        'photo' => 'string',
        'url' => 'string',
        'teacher_id' => 'integer',
        'date' => 'date',
        'time' => 'datetime:H:i',
        'views' => 'integer',
        'video' => 'string',
        'status' => 'integer',
    ];
}
