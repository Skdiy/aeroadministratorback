<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EventQuestionsModel extends Model
{
    use HasFactory;

    protected $table = 'event_questions';

    protected $primaryKey = 'id';

    public $incrementing = true;
    public $timestamps = true;

    protected $fillable = [
        'question',
        'answer',
        'state',

    ];
    protected $casts = [
        'question' => 'string',
        'answer' => 'string',
        'state' => 'boolean',

    ];
}
