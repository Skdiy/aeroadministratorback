<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserProgramAssistanceModel extends Model
{
    use HasFactory;

    protected $table = 'user_program_assistance';

    protected $primaryKey = 'id';

    public $incrementing = true;
    public $timestamps = true;

    protected $fillable = [
        'user_program_id',
        'program_class_id',
        'assistance',
        'hours'
    ];
    protected $casts = [
        'user_program_id' => 'integer',
        'program_class_id' => 'integer',
        'assistance' => 'integer',
        'hours' => 'integer'
    ];
}
