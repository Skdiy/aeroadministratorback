<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EventDelegationInfoModel extends Model
{
    use HasFactory;

    protected $table = 'event_delegation_info';

    protected $primaryKey = 'id';

    public $incrementing = true;
    public $timestamps = true;

    protected $fillable = [
        'file',

    ];
    protected $casts = [
        'file' => 'string',

    ];
}
