<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ExamQuestionsModel extends Model
{
    use HasFactory;

    const INPUT = 1;
    const TEXTAREA = 2;
    const ALTERNATIVE = 3;
    const MULTIALTERNATIVE = 4;

    protected $table = 'exam_questions';

    protected $primaryKey = 'id';

    public $incrementing = true;
    public $timestamps = true;

    protected $fillable = [
        'exam_id',
        'question',
        'type',
        'resources',
    ];
    protected $casts = [
        'exam_id' => 'integer',
        'question'  => 'string',
        'type'  => 'integer',
        'resources'  => 'string'
    ];
}
