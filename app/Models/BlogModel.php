<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BlogModel extends Model
{
    use HasFactory;

    protected $table = 'blog';

    protected $primaryKey = 'id';

    public $incrementing = true;
    public $timestamps = true;

    protected $fillable = [
        'title',
        'photo',
        'banner',
        'content',
        'category_id'
    ];
    protected $casts = [
        'image' => 'string',
        'title' => 'string',
        'photo' => 'string',
        'banner' => 'string',
        'content' => 'string',
        'category_id' => 'integer'
    ];
}
