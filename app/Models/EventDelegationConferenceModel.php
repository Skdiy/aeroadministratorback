<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EventDelegationConferenceModel extends Model
{
    use HasFactory;

    protected $table = 'event_delegation_conference';

    protected $primaryKey = 'id';

    public $incrementing = true;
    public $timestamps = true;

    protected $fillable = [
        'name',
        'corporation',
        'date',
        'imgUrl',
        'state',
    ];
    protected $casts = [
        'name' => 'string',
        'corporation' => 'string',
        'imgUrl' => 'string',
        'date' => 'date',
        'state' => 'string',

    ];
}
