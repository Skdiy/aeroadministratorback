<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProgramClassModel extends Model
{
    use HasFactory;

    protected $table = 'program_class';

    protected $primaryKey = 'id';

    public $incrementing = true;
    public $timestamps = true;

    protected $fillable = [
        'program_temary_id',
        'name',
        'state',
        'teacher',
        'description',
        'dateIni',
        'dateEnd',
        'Url',
    ];
    protected $casts = [
        'program_temary_id' => 'integer',
        'name' => 'string',
        'state' => 'boolean',
        'urlRecord' => 'string',
        'teacher' => 'string',
        'description' => 'string',
        'dateIni' => 'string',
        'dateEnd' => 'string',
        'Url' => 'string',
    ];
}
