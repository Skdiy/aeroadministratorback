<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EventSubscriptionModel extends Model
{
    use HasFactory;

    protected $table = 'event_subscription';

    protected $primaryKey = 'id';

    public $incrementing = true;
    public $timestamps = true;

    protected $fillable = [
        'bussines',
        'country',
        'phone',
        'city',
        'web',
        'rubro',
        'code',
        'representante',
        'cargo',
        'contact_number',
        'email',
        'others',
        'integrants',
    ];
    protected $casts = [
        'bussines' => 'string',
        'country' => 'string',
        'city' => 'string',
        'phone' => 'string',
        'web' => 'string',
        'rubro' => 'string',
        'code' => 'string',
        'representante' => 'string',
        'cargo' => 'string',
        'contact_number' => 'string',
        'email' => 'string',
        'others' => 'string',
        'integrants' => 'integer',
    ];
}
