<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class NotesModel extends Model
{
    use HasFactory;
    protected $table = 'notes';

    protected $primaryKey = 'id';

    const EXAM = "1";
    const PROYECT = "2";

    public $incrementing = true;
    public $timestamps = true;

    protected $fillable = [
        'user_id',
        'course_id',
        'note',
        'percent',
        'exam_id',
        'proyect_id',
        'type'
    ];
    protected $casts = [
        'user_id' => 'integer',
        'course_id'  => 'integer',
        'exam_id' => 'integer',
        'proyect_id'  => 'integer',
        'type' => 'integer',
        'note'  => 'integer',
        'percent'  => 'decimal'
    ];
}
