<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProyectsCourse extends Model
{
    use HasFactory;
    protected $table = 'proyects_courses';

    protected $primaryKey = 'id';

    public $incrementing = true;
    public $timestamps = true;

    protected $fillable = [
        'course_id',
        'title',
        'description',
        'descriptionProyect',

    ];
    protected $casts = [
        'title' => 'string',
        'course_id'  => 'integer',
        'description'  => 'string',
        'descriptionProyect' => 'string',
    ];
}
