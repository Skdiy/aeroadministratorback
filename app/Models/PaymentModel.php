<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PaymentModel extends Model
{
    use HasFactory;

    protected $table = 'payments';

    protected $primaryKey = 'id';

    public $incrementing = true;
    public $timestamps = true;

    protected $fillable = [
        'user_id',
        'codPay',
        'price',
        'comment'
    ];
    protected $casts = [
        'user_id' => 'integer',
        'codPay' => 'string',
        'price' => 'decimal',
        'comment' => 'string'
    ];
}
