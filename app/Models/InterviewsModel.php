<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class InterviewsModel extends Model
{
    use HasFactory;

    protected $table = 'interviews';

    const CARRER = 1;
    const BUSSINES = 2;

    protected $primaryKey = 'id';

    public $incrementing = true;
    public $timestamps = true;

    protected $fillable = [
        'razonSocial',
        'pais',
        'ciudad',
        'cargo',
        'description',
        'category',
        'dateIni',
        'spots',
        'experience',
        'Levelexperience',

        'typeDoc',
        'document',
        'lastname1',
        'lastname2',
        'names',
        'birthday',
        'sex',
        'nacionality',
        'adress',
        'urbanization',
        'district',
        'province',
        'homephone',
        'phone',
        'civilStatus',
        'email',

        'relationAp',
        'documentAp',
        'lastname1Ap',
        'lastname2Ap',
        'nameAp',
        'birthdayAp',
        'ocupationAp',
        'nacionalityAp',
        'emailAp',
        'phoneAp',
        'type'
    ];
    protected $casts = [
        'razonSocial' => 'string',
        'pais' => 'string',
        'ciudad' => 'string',
        'cargo' => 'string',
        'description' => 'string',
        'category' => 'string',
        'dateIni' => 'date',
        'spots' => 'integer',
        'experience' => 'boolean',
        'Levelexperience' => 'string',

        'typeDoc' => 'string',
        'document' => 'integer',
        'lastname1' => 'string',
        'lastname2' => 'string',
        'names' => 'string',
        'birthday' => 'date',
        'sex' => 'string',
        'nacionality' => 'string',
        'adress' => 'string',
        'urbanization' => 'string',
        'district' => 'string',
        'province' => 'string',
        'homephone' => 'string',
        'phone' => 'string',
        'civilStatus' => 'string',
        'email' => 'string',

        'relationAp' => 'string',
        'documentAp' => 'integer',
        'lastname1Ap' => 'string',
        'lastname2Ap' => 'string',
        'nameAp' => 'string',
        'birthdayAp' => 'date',
        'ocupationAp' => 'string',
        'nacionalityAp' => 'string',
        'emailAp' => 'string',
        'phoneAp' => 'string',
        'type' => 'integer',
    ];
}

