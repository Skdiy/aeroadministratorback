<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CourseDefinitionsModel extends Model
{
    use HasFactory;
    protected $table = 'course_definitions';

    protected $primaryKey = 'id';

    public $incrementing = true;
    public $timestamps = true;

    protected $fillable = [
        'course_id',
        'title',
        'definition',

    ];
    protected $casts = [
        'course_id' => 'integer',
        'title' => 'string',
        'definition' => 'string',
    ];
}
