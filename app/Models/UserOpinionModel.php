<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserOpinionModel extends Model
{
    use HasFactory;

    protected $table = 'user_opinion';

    protected $primaryKey = 'id';

    public $incrementing = true;
    public $timestamps = true;

    protected $fillable = [
        'comment',
        'name',
        'profesion',
        'photo'
    ];
    protected $casts = [
        'comment' => 'string',
        'name' => 'string',
        'profesion' => 'string',
        'photo' => 'string'
    ];
}
