<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EventBusinessInfoModel extends Model
{
    use HasFactory;

    protected $table = 'event_business_info';

    protected $primaryKey = 'id';

    public $incrementing = true;
    public $timestamps = true;

    protected $fillable = [
        'professional_visitors',
        'official_delegations',
        'countries',
        'file',

    ];
    protected $casts = [
        'professional_visitors' => 'string',
        'official_delegations' => 'string',
        'countries' => 'string',
        'file' => 'string',
    ];
}
