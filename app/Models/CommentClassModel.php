<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CommentClassModel extends Model
{
    use HasFactory;

    protected $table = 'comment_class';

    const APUNTE = 1;
    const COMMENT = 2;

    protected $primaryKey = 'id';

    public $incrementing = true;
    public $timestamps = true;

    protected $fillable = [
        'class_real_time_id',
        'user_id',
        'comment',
        'type'
    ];
    protected $casts = [
        'class_real_time_id' => 'integer',
        'user_id' => 'integer',
        'comment' => 'string',
        'type' => 'integer'
    ];
}
