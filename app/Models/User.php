<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;
use Tymon\JWTAuth\Contracts\JWTSubject;

class User extends Authenticatable implements JWTSubject
{
    use HasApiTokens, HasFactory, Notifiable;

    const ROL_ADMIN = 10;
    const ROL_PROFESOR = 5;
    const ROL_EMPRESA = 3;
    const ROL_ALUMNO = 1;
    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'name',
        'surname',
        'num_doc',
        'phone',
        'ocupation',
        'genre',
        'bithday',
        'nationality',
        'country',
        'city',
        'photo',
        'bg_image',
        'description',
        'facebook',
        'instagram',
        'linkRecomendation',
        'qualification',
        'rol',
        'email',
        'personalEmail',
        'password',
        'status'
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'name' => 'string',
        'rol' => 'integer',
        'personalEmail' => 'string',
        'email' => 'string',
        'email_verified_at' => 'datetime',
        'surname'=> 'string',
        'num_doc'=> 'integer',
        'phone'=> 'integer',
        'ocupation'=> 'string',
        'genre'=> 'string',
        'bithday'=> 'date',
        'nationality'=> 'string',
        'country'=> 'string',
        'city'=> 'string',
        'photo'=> 'string',
        'bg_image'=> 'string',
        'description'=> 'string',
        'facebook'=> 'string',
        'instagram'=> 'string',
        'linkRecomendation'=> 'string',
        'qualification'=> 'string',
        'status' => 'boolean'
    ];
    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier() {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims() {
        return [];
    }
}
