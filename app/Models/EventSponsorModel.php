<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EventSponsorModel extends Model
{
    use HasFactory;

    protected $table = 'event_sponsor';

    protected $primaryKey = 'id';

    public $incrementing = true;
    public $timestamps = true;

    protected $fillable = [
        'name',
        'urlimg',
        'state',

    ];
    protected $casts = [
        'name' => 'string',
        'urlimg' => 'string',
        'state' => 'boolean',

    ];
}
