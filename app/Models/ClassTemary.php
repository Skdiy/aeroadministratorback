<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ClassTemary extends Model
{
    use HasFactory;

    protected $table = 'class_temaries';

    protected $primaryKey = 'id';

    public $incrementing = true;
    public $timestamps = true;

    protected $fillable = [
        'temary_id',
        'name',
        'state',
        'urlRecord'
    ];
    protected $casts = [
        'temary_id' => 'integer',
        'name' => 'string',
        'state' => 'boolean',
        'urlRecord' => 'string'
    ];
}
