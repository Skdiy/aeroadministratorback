<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProyectsUserModel extends Model
{
    use HasFactory;

    const PENDIENTE = 1;
    const REVISADO = 2;

    protected $table = 'proyects_user';



    protected $primaryKey = 'id';

    public $incrementing = true;
    public $timestamps = true;

    protected $fillable = [
        'proyect_id',
        'file_id',
        'user_id',
        'response',
        'status'
    ];
    protected $casts = [
        'file_id' => 'integer',
        'proyect_id'  => 'integer',
        'user_id'  => 'integer',
        'response' => 'string',
        'status' => 'boolean'
    ];
}
