<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ExamModel extends Model
{
    use HasFactory;

    const PENDIENTE = 1;
    const DESACTIVADO = 2;
    const ACTIVADO = 3;

    protected $table = 'exam';

    protected $primaryKey = 'id';

    public $incrementing = true;
    public $timestamps = true;

    protected $fillable = [
        'title',
        'DateInit',
        'DateEnd',
        'duration',
        'maxNote',
        'state',
        'course_id'
    ];
    protected $casts = [
        'title' => 'string',
        'DateInit' => 'date',
        'DateEnd' => 'date',
        'duration' => 'integer',
        'maxNote'=> 'integer',
        'state' => 'integer',
        'course_id' => 'integer'
    ];
}
