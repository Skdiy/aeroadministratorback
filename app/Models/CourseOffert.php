<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CourseOffert extends Model
{
    use HasFactory;

    protected $fillable = [
        'course_id',
        'name',
        'state',
    ];
    protected $casts = [
        'course_id' => 'integer',
        'name' => 'string',
        'state' => 'boolean',
    ];
}
