<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EventNoticesModel extends Model
{
    use HasFactory;

    protected $table = 'event_notices';

    protected $primaryKey = 'id';

    public $incrementing = true;
    public $timestamps = true;

    protected $fillable = [
        'name',
        'urlSocial',
        'type_social',

    ];
    protected $casts = [
        'name' => 'string',
        'urlSocial' => 'string',
        'type_social' => 'integer',

    ];
}
