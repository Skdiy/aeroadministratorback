<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ZoomMeeting extends Model
{
    use HasFactory;

    const MEETING_TYPE_INSTANT = 1;
    const MEETING_TYPE_SCHEDULE = 2;
    const MEETING_TYPE_RECURRING = 3;
    const MEETING_TYPE_FIXED_RECURRING_FIXED = 8;

    protected $table = 'zoom_meetings';

    protected $primaryKey = 'id';

    public $incrementing = true;
    public $timestamps = true;

    protected $fillable = [
        'topic',
        'start_time',
        'duration',
        'agenda',
        'host_video',
        'participant_video',
        'zoom_id',
        'join_url',
        'password'
    ];
    protected $casts = [
        'topic' => 'string',
        'start_time' => 'date',
        'duration' => 'integer',
        'agenda' => 'string',
        'host_video' => 'boolean',
        'participant_video' => 'boolean',
        'zoom_id' => 'integer',
        'join_url' => 'string',
        'password' => 'string'
    ];
}
