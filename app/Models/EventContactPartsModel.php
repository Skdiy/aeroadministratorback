<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EventContactPartsModel extends Model
{
    use HasFactory;

    protected $table = 'event_contact_parts';

    protected $primaryKey = 'id';

    public $incrementing = true;
    public $timestamps = true;

    protected $fillable = [
        'title',
        'urlimg',
        'abstract',
        'description',
        'languages',
        'type',

    ];
    protected $casts = [
        'title' => 'string',
        'urlimg' => 'string',
        'abstract' => 'string',
        'description' => 'string',
        'languages' => 'string',
        'type' => 'integer',
    ];
}
