<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProgramTemaryModel extends Model
{
    use HasFactory;

    protected $table = 'program_temary';

    protected $primaryKey = 'id';

    public $incrementing = true;
    public $timestamps = true;

    protected $fillable = [
        'program_id',
        'name',
        'state',
    ];
    protected $casts = [
        'program_id' => 'integer',
        'name' => 'string',
        'state' => 'boolean',
    ];
}
