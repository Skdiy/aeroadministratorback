<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EventBusinessCatalogueModel extends Model
{
    use HasFactory;

    protected $table = 'event_business_catalogue';

    protected $primaryKey = 'id';

    public $incrementing = true;
    public $timestamps = true;

    protected $fillable = [
        'title',
        'description',
        'urlfile',
        'state',

    ];
    protected $casts = [
        'title' => 'string',
        'description' => 'string',
        'urlfile' => 'string',
        'state' => 'boolean',
    ];
}
