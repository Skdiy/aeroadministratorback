<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserProgramModel extends Model
{
    use HasFactory;

    protected $table = 'user_program_models';

    protected $primaryKey = 'id';

    public $incrementing = true;
    public $timestamps = true;

    protected $fillable = [
        'user_id',
        'program_id',
        'payment_id'
    ];
    protected $casts = [
        'user_id' => 'integer',
        'program_id' => 'integer',
        'payment_id' => 'integer'
    ];
}
