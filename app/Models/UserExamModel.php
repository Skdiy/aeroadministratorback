<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserExamModel extends Model
{
    use HasFactory;

    protected $table = 'user_exam_models';

    protected $primaryKey = 'id';

    public $incrementing = true;
    public $timestamps = true;

    protected $fillable = [
        'user_id',
        'exam_questions_id',
        'answer',
        'comment',
        'points',
    ];
    protected $casts = [
        'exam_questions_id' => 'integer',
        'user_id'  => 'integer',
        'answer'  => 'string',
        'comment'  => 'string',
        'points'  => 'integer',
    ];
}
