<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TeacherPaymentsModel extends Model
{
    use HasFactory;
    protected $table = 'teacher_payments';

    protected $primaryKey = 'id';

    public $incrementing = true;
    public $timestamps = true;

    protected $fillable = [
        'contract_id',
        'name',
        'deposit',
        'pay',
        'state',
    ];
    protected $casts = [
        'contract_id' => 'integer',
        'name' => 'string',
        'deposit' => 'date',
        'pay' => 'integer',
        'state' => 'integer',
    ];
}
