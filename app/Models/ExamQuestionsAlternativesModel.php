<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ExamQuestionsAlternativesModel extends Model
{
    use HasFactory;

    protected $table = 'exam_questions_alternatives';

    protected $primaryKey = 'id';

    public $incrementing = true;
    public $timestamps = true;

    protected $fillable = [
        'exam_questions_id',
        'alternative',
    ];
    protected $casts = [
        'exam_questions_id' => 'integer',
        'alternative'  => 'string',
    ];
}
