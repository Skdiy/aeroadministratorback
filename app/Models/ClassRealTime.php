<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ClassRealTime extends Model
{
    use HasFactory;

    protected $table = 'class_real_times';

    protected $primaryKey = 'id';

    public $incrementing = true;
    public $timestamps = true;

    protected $fillable = [
        'course_id',
        'nextclass',
        'zoom_id',
    ];
    protected $casts = [
        'course_id' => 'integer',
        'nextclass' => 'datetime',
        'zoom_id' => 'integer',
    ];
}
