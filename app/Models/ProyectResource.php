<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProyectResource extends Model
{
    use HasFactory;
    protected $table = 'proyects_resources';

    protected $primaryKey = 'id';

    public $incrementing = true;
    public $timestamps = true;

    protected $fillable = [
        'proyect_id',
        'file_id',
    ];
    protected $casts = [
        'proyect_id'  => 'integer',
        'file_id'  => 'integer',
    ];
}
