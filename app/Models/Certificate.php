<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Certificate extends Model
{
    use HasFactory;

    protected $table = 'certificates';

    protected $primaryKey = 'id';

    public $incrementing = true;
    public $timestamps = true;

    protected $fillable = [
        'codCertificate',
        'file_id',
        'user_id',
    ];
    protected $casts = [
        'user_id' => 'integer',
        'file_id' => 'integer',
        'codCertificate' => 'string',
    ];
}
