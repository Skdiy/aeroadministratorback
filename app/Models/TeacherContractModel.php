<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TeacherContractModel extends Model
{
    use HasFactory;

    protected $table = 'teacher_contract';

    protected $primaryKey = 'id';

    public $incrementing = true;
    public $timestamps = true;

    protected $fillable = [
        'teacher_id',
        'course_code',
        'total',
        'emision',
        'course_type',
    ];
    protected $casts = [
        'teacher_id' => 'integer',
        'course_code' => 'string',
        'total' => 'integer',
        'emision' => 'date',
        'course_type' => 'integer',
    ];
}
