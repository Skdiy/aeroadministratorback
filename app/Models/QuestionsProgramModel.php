<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class QuestionsProgramModel extends Model
{
    use HasFactory;

    protected $table = 'questions_program';

    protected $primaryKey = 'id';

    public $incrementing = true;
    public $timestamps = true;

    protected $fillable = [
        'program_id',
        'question',
        'answer',
    ];
    protected $casts = [
        'program_id' => 'integer',
        'question' => 'string',
        'answer' => 'string',
    ];
}
