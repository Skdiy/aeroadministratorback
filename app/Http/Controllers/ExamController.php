<?php

namespace App\Http\Controllers;

use App\Models\Course;
use App\Models\ExamModel;
use App\Models\ExamQuestionsAlternativesModel;
use App\Models\ExamQuestionsModel;
use App\Models\UserExamModel;
use App\Traits\ApiResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ExamController extends Controller
{
    use ApiResponse;

    /**
     * @OA\Get(
     *     path="/exam",
     *     summary="Obetener todos los examenes.",
     *     tags={"Exams"},
     *
     *     @OA\Response(
     *         response=200,
     *         description="OK",
     *     ),
     *
     *     @OA\Response(
     *         response="400",
     *         description="Failed",
     *     ),
     *
     *     security={{"apiAuth": {} }},
     *
     *     deprecated=false
     * )
     */
    public function get()
    {
        try {
            $exam = ExamModel::all();
        } catch (\Exception $exception) {
            return $this->errorResponse($exception->getMessage(), 400);
        }

        return $this->successResponse([
            'status' => 200,
            'data' =>  $exam ,
        ]);
    }

    /**
     * @OA\Get(
     *     path="/exam/{exam_id}",
     *     summary="Obetener un examen.",
     *     tags={"Exams"},
     *     @OA\Parameter(
     *          name="exam_id",
     *          description="Identificador de examen",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *     @OA\Response(
     *         response=200,
     *         description="OK",
     *     ),
     *
     *     @OA\Response(
     *         response="400",
     *         description="Failed",
     *     ),
     *
     *     security={{"apiAuth": {} }},
     *
     *     deprecated=false
     * )
     */
    public function getExam($exam_id)
    {
        try {
            $exam = ExamModel::find($exam_id);
            if ($exam != null) {
                $exam['course'] = Course::where('id',$exam->course_id)->select('name')->get()->first()->name;
                $exam['questions'] = ExamQuestionsModel::where('exam_id',$exam_id)->get();

                foreach ($exam['questions'] as $key) {
                    if ($key->type == 3 || $key->type == 4) {
                        $key['alternative'] = ExamQuestionsAlternativesModel::where('exam_questions_id', $key->id)->get();
                    }
                }
            }


        } catch (\Exception $exception) {
            return $this->errorResponse($exception->getMessage(), 400);
        }

        return $this->successResponse([
            'status' => 200,
            'data' => $exam ,
        ]);
    }
    /**
     * @OA\Get(
     *     path="/realtime/course/{course_id}",
     *     summary="Obetener los examenes de un curso.",
     *     tags={"Realtime Class"},
     *     @OA\Parameter(
     *          name="course_id",
     *          description="Identificador de curso",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *     @OA\Response(
     *         response=200,
     *         description="OK",
     *     ),
     *
     *     @OA\Response(
     *         response="400",
     *         description="Failed",
     *     ),
     *
     *
     *
     *     deprecated=false
     * )
     */
    public function getExamCourse($course_id)
    {
        try {
            $exam = ExamModel::where('course_id',$course_id)->get();
        } catch (\Exception $exception) {
            return $this->errorResponse($exception->getMessage(), 400);
        }

        return $this->successResponse([
            'status' => 200,
            'data' => $exam ,
        ]);
    }
    /**
     * @OA\Get(
     *     path="user/exam",
     *     summary="Obetener mis examenes.",
     *     tags={"Exams"},
     *     @OA\Response(
     *         response=200,
     *         description="OK",
     *     ),
     *
     *     @OA\Response(
     *         response="400",
     *         description="Failed",
     *     ),
     *
     *     security={{"apiAuth": {} }},
     *     deprecated=false
     * )
     */
    public function getMyExam()
    {
        try {
            $exams = UserExamModel::join('exam','user_exam_models.exam_id','exam.id')
            ->where('user_exam_models.user_id',auth()->user()->id)
            ->select('exam.*')
            ->get();


        } catch (\Exception $exception) {
            return $this->errorResponse($exception->getMessage(), 400);
        }

        return $this->successResponse([
            'status' => 200,
            'data' =>  $exams ,
        ]);
    }

    /**
     * @OA\Get(
     *     path="admin/exam/{exam_id}",
     *     summary="Obetener los participantes de un examen.",
     *     tags={"Exams"},
     *     @OA\Response(
     *         response=200,
     *         description="OK",
     *     ),
     *
     *     @OA\Response(
     *         response="400",
     *         description="Failed",
     *     ),
     *
     *     security={{"apiAuth": {} }},
     *     deprecated=false
     * )
     */
    public function getAllResponseExam( $exam_id)
    {
        try {
            $exams = UserExamModel::where('user_exam_models.exam_id',$exam_id)
            ->join('users','user_exam_models.user_id','=','users.id')
            ->get();


        } catch (\Exception $exception) {
            return $this->errorResponse($exception->getMessage(), 400);
        }

        return $this->successResponse([
            'status' => 200,
            'data' =>  $exams ,
        ]);
    }

    /**
     * @OA\Get(
     *     path="admin/exam/user/{exam_id}",
     *     summary="Obetener las respuestas de un examen.",
     *     tags={"Exams"},
     *     @OA\Parameter(
     *          name="user_id",
     *          description="Usuario",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *     @OA\Response(
     *         response=200,
     *         description="OK",
     *     ),
     *
     *     @OA\Response(
     *         response="400",
     *         description="Failed",
     *     ),
     *
     *     security={{"apiAuth": {} }},
     *
     *     deprecated=false
     * )
     */
    public function getResponseExam(Request $request, $exam_id)
    {
        try {
            $exams = UserExamModel::join('exam_questions','exam_questions.id','user_exam_models.exam_questions_id')
            ->where('user_exam_models.exam_id',$exam_id)
            ->where('user_exam_models.user_id',$request->user_id)
            ->get();


        } catch (\Exception $exception) {
            return $this->errorResponse($exception->getMessage(), 400);
        }

        return $this->successResponse([
            'status' => 200,
            'data' =>  $exams ,
        ]);
    }
    /**
     * @OA\Post(
     *     path="/admi/exam",
     *     summary="Agrega un examen",
     *     tags={"Exams"},
     *
     *     @OA\RequestBody(
     *         required=true,
     *         @OA\MediaType(
     *             mediaType="multipart/form-data",
     *             @OA\Schema(
     *                  @OA\Property(
     *                     property="course_id",
     *                     type="integer",
     *                 ),
     *                 @OA\Property(
     *                     property="title",
     *                     type="string",
     *                 ),
     *                 @OA\Property(
     *                     property="DateInit",
     *                     type="Time"
     *                 ),
     *                 @OA\Property(
     *                     property="DateEnd",
     *                     type="Time"
     *                 ),
     *                 @OA\Property(
     *                     property="duration",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="maxNote",
     *                     type="string"
     *                 ),
     *
     *                 example={"title": "name", "DateInit": "2021-01-01T01:20:00", "DateEnd": "2021-01-01T01:20:00", "duration": "01:20:00" , "maxNote": 20}
     *             )
     *         )
     *     ),
     *
     *     @OA\Response(
     *         response=201,
     *         description="OK",
     *     ),
     *
     *     @OA\Response(
     *         response="400",
     *         description="Failed",
     *     ),
     *
     *     security={{"apiAuth": {} }},
     *
     *     deprecated=false
     * )
     */
    public function add(Request $request)
    {
        try {
            if (auth()->user()->rol <= 5 ) {
                return $this->errorResponse('No tiene acceso a esta opciones. Consulte con los administradores del sistema', 401);
            }

            $validator = Validator::make($request->all(), [
                'course_id' => 'required',
                'title' => 'required',
                'DateInit' => 'required',
                'DateEnd' => 'required',
                'duration' => 'required',
                'maxNote' => 'required'
            ]);

            if($validator->fails()){

                return $this->errorResponse($validator->errors()->first(), 400);
            }

            $course = Course::findOrFail($request->course_id);

            if ($course!= null) {
                $exam = new ExamModel();
                $exam->course_id = $request->course_id;
                $exam->title = $request->title;
                $exam->DateInit = $request->DateInit;
                $exam->DateEnd = $request->DateEnd;
                $exam->duration = $request->duration;
                $exam->maxNote = $request->maxNote;
                $exam->state = ExamModel::PENDIENTE;
                $exam->save();
            }else {
                return $this->errorResponse("Error con el identificador.", 400);
            }


            return $this->createResponse([
                'status' => 201,
                'message' => 'Usuario registrado',
                'data' => $exam,
            ]);
        } catch (\Exception $exception) {
            return $this->errorResponse($exception->getMessage(), 400);
        }
    }
    /**
     * @OA\Post(
     *     path="/admi/exam/{exam_id}",
     *     summary="Agrega una pregunta al examen",
     *     tags={"Exams"},
     *     @OA\Parameter(
     *          name="exam_id",
     *          description="Identificador de examen",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *     @OA\RequestBody(
     *         required=true,
     *         @OA\MediaType(
     *             mediaType="multipart/form-data",
     *             @OA\Schema(
     *                 @OA\Property(
     *                     property="question",
     *                     type="Time"
     *                 ),
     *                 @OA\Property(
     *                     property="type",
     *                     type="integer"
     *                 ),
     *                 @OA\Property(
     *                     property="resources",
     *                     type="file"
     *                 ),
     *                  @OA\Property(
     *                     property="alternatives",
     *                     type="string[]"
     *                 ),
     *                 example={"question": "name", "type": "1"}
     *             )
     *         )
     *     ),
     *
     *     @OA\Response(
     *         response=201,
     *         description="OK",
     *     ),
     *
     *     @OA\Response(
     *         response="400",
     *         description="Failed",
     *     ),
     *
     *     security={{"apiAuth": {} }},
     *
     *     deprecated=false
     * )
     */
    public function addQuestion(Request $request,$exam_id)
    {
        try {
            if (auth()->user()->rol <= 5 ) {
                return $this->errorResponse('No tiene acceso a esta opciones. Consulte con los administradores del sistema', 401);
            }

            $validator = Validator::make($request->all(), [
                'question' => 'required',
                'type' => 'required'
            ]);

            if($validator->fails()){

                return $this->errorResponse($validator->errors()->first(), 400);
            }


            $ExamQuestion = new ExamQuestionsModel();
            $ExamQuestion->exam_id = $exam_id;
            $ExamQuestion->question = $request->question;
            $ExamQuestion->type = $request->type;
            if ($image = $request->file('resources')) {
                $name = "image_ExamQuestion_". $request->name . '_' . time() . "." . $image->guessExtension();
                $rutaIMG = public_path("images/ExamQuestions/image/" . $name);
                copy($image, $rutaIMG);

                $ExamQuestion->resources = $name;
            }

            $ExamQuestion->save();
            if ($request->alternatives != null) {
                $Aalter = explode(',',$request->alternatives);
                if (($ExamQuestion->type == ExamQuestionsModel::ALTERNATIVE || $ExamQuestion->type == ExamQuestionsModel::MULTIALTERNATIVE)) {
                    foreach ($Aalter as $key) {
                        $alternative = new ExamQuestionsAlternativesModel();
                        $alternative->exam_questions_id = $ExamQuestion->id;
                        $alternative->alternative = $key;
                        $alternative->save();
                    }
                }
            }


            return $this->createResponse([
                'status' => 201,
                'message' => 'Pregunta registrada'
            ]);
        } catch (\Exception $exception) {
            return $this->errorResponse($exception->getMessage(), 400);
        }
    }

    /**
     * @OA\Post(
     *     path="/user/exam/{exam_questions_id}",
     *     summary="Responder una pregunta",
     *     tags={"Exams"},
     *
     *     @OA\RequestBody(
     *         required=true,
     *         @OA\MediaType(
     *             mediaType="multipart/form-data",
     *             @OA\Schema(
     *                  @OA\Property(
     *                     property="exam_id",
     *                     type="integer"
     *                 ),
     *                 @OA\Property(
     *                     property="answer",
     *                     type="string"
     *                 ),
     *                 example={"exam_id":1 ,"answer": "hola que hace?"}
     *             )
     *         )
     *     ),
     *
     *     @OA\Response(
     *         response=201,
     *         description="OK",
     *     ),
     *
     *     @OA\Response(
     *         response="400",
     *         description="Failed",
     *     ),
     *
     *     security={{"apiAuth": {} }},
     *
     *     deprecated=false
     * )
     */
    public function ResponseQuestion(Request $request,$exam_questions_id)
    {
        try {

            $validator = Validator::make($request->all(), [
                'answer' => 'required'
            ]);

            if($validator->fails()){

                return $this->errorResponse($validator->errors()->first(), 400);
            }

            $answer =  UserExamModel::where('user_id',auth()->user()->id)
            ->where('exam_questions_id',$exam_questions_id)
            ->get()->first();

            if ($answer == null) {
                $userAnswer = new UserExamModel();
                $userAnswer->user_id = auth()->user()->id;
                $userAnswer->exam_id = $request->exam_id;
                $userAnswer->exam_questions_id = $exam_questions_id;
                $userAnswer->answer = $request->answer;
                $userAnswer->save();
            }else{
                $answer->answer = $request->answer;
                $answer->update();
            }

            return $this->createResponse([
                'status' => 201,
                'message' => 'Respuesta guardada/actualizada'
            ]);
        } catch (\Exception $exception) {
            return $this->errorResponse($exception->getMessage(), 400);
        }
    }

    /**
     * @OA\Post(
     *     path="/admin/exam/{exam_questions_id}",
     *     summary="Calificar una pregunta",
     *     tags={"Exams"},
     *
     *     @OA\RequestBody(
     *         required=true,
     *         @OA\MediaType(
     *             mediaType="multipart/form-data",
     *             @OA\Schema(
     *                 @OA\Property(
     *                     property="comment",
     *                     type="string"
     *                 ),
     *                   @OA\Property(
     *                     property="points",
     *                     type="integer"
     *                 ),
     *                 example={"comment": "hola que hace?", "points":5}
     *             )
     *         )
     *     ),
     *
     *     @OA\Response(
     *         response=201,
     *         description="OK",
     *     ),
     *
     *     @OA\Response(
     *         response="400",
     *         description="Failed",
     *     ),
     *
     *     security={{"apiAuth": {} }},
     *
     *     deprecated=false
     * )
     */
    public function QualifyQuestion(Request $request,$exam_questions_id)
    {
        try {

            $validator = Validator::make($request->all(), [
                'comment' => 'required',
                'points' => 'required'
            ]);

            if($validator->fails()){

                return $this->errorResponse($validator->errors()->first(), 400);
            }

            $answer =  UserExamModel::where('exam_questions_id',$exam_questions_id)
            ->get()->first();

            if ($answer == null) {
                return $this->errorResponse('No se encontro pregunta', 400);
            }else{
                $answer->comment = $request->comment;
                $answer->points = $request->points;
                $answer->update();
            }

            return $this->createResponse([
                'status' => 201,
                'message' => 'Respuesta calificada'
            ]);
        } catch (\Exception $exception) {
            return $this->errorResponse($exception->getMessage(), 400);
        }
    }
}
