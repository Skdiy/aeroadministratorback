<?php

namespace App\Http\Controllers;

use App\Models\InterviewsModel;
use App\Traits\ApiResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Carbon;

class InterviewController extends Controller
{
    use ApiResponse;

    /**
     * @OA\Get(
     *     path="/admin/interview/carrer",
     *     summary="Obtener entrevistas a carreras",
     *     tags={"Interviews"},
     *     @OA\Response(
     *         response=200,
     *         description="OK",
     *     ),
     *
     *     @OA\Response(
     *         response="400",
     *         description="Failed",
     *     ),
     *
     *     security={{"apiAuth": {} }},
     *     deprecated=false
     * )
     */
    public function get_carrer()
    {
        try {
            $interviews = InterviewsModel::where('type',InterviewsModel::CARRER)->get();

            return $this->createResponse([
                'status' => 200,
                'data' => $interviews,
            ]);
        } catch (\Exception $exception) {
            return $this->errorResponse($exception->getMessage(), 400);
        }
    }

    /**
     * @OA\Get(
     *     path="/admin/interview/bussines",
     *     summary="Obtener entrevistas a empresas.",
     *     tags={"Interviews"},
     *     @OA\Response(
     *         response=200,
     *         description="OK",
     *     ),
     *
     *     @OA\Response(
     *         response="400",
     *         description="Failed",
     *     ),
     *
     *     security={{"apiAuth": {} }},
     *     deprecated=false
     * )
     */
    public function get_bussines()
    {
        try {
            $interviews = InterviewsModel::where('type',InterviewsModel::BUSSINES)->get();

            return $this->createResponse([
                'status' => 200,
                'data' => $interviews,
            ]);
        } catch (\Exception $exception) {
            return $this->errorResponse($exception->getMessage(), 400);
        }
    }


    /**
     * @OA\Post(
     *     path="/interview/carrer",
     *     summary="Registrar una entrevista",
     *     tags={"Interviews"},
     *
     *     @OA\RequestBody(
     *         required=true,
     *         @OA\MediaType(
     *             mediaType="multipart/form-data",
     *             @OA\Schema(
     *                 @OA\Property(
     *                     property="typeDoc",
     *                     type="string",
     *                 ),
     *                 @OA\Property(
     *                     property="document",
     *                     type="integer",
     *                 ),
     *                 @OA\Property(
     *                     property="lastname1",
     *                     type="string",
     *                 ),
     *                  @OA\Property(
     *                     property="lastname2",
     *                     type="string",
     *                 ),
     *                  @OA\Property(
     *                     property="names",
     *                     type="string",
     *                 ),
     *                   @OA\Property(
     *                     property="birthday",
     *                     type="date",
     *                 ),
     *                   @OA\Property(
     *                     property="sex",
     *                     type="string",
     *                 ),
     *                   @OA\Property(
     *                     property="nacionality",
     *                     type="string",
     *                 ),
     *                   @OA\Property(
     *                     property="adress",
     *                     type="string",
     *                 ),
     *                    @OA\Property(
     *                     property="urbanization",
     *                     type="string",
     *                 ),
     *                    @OA\Property(
     *                     property="district",
     *                     type="string",
     *                 ),
     *                    @OA\Property(
     *                     property="province",
     *                     type="string",
     *                 ),
     *                    @OA\Property(
     *                     property="homephone",
     *                     type="integer",
     *                 ),
     *                     @OA\Property(
     *                     property="phone",
     *                     type="integer",
     *                 ),
     *                     @OA\Property(
     *                     property="civilStatus",
     *                     type="string",
     *                 ),
     *                      @OA\Property(
     *                     property="email",
     *                     type="string",
     *                 ),
     *                      @OA\Property(
     *                     property="relationAp",
     *                     type="string",
     *                 ),
     *                       @OA\Property(
     *                     property="documentAp",
     *                     type="integer",
     *                 ),
     *                       @OA\Property(
     *                     property="lastname1Ap",
     *                     type="string",
     *                 ),
     *                       @OA\Property(
     *                     property="lastname2Ap",
     *                     type="string",
     *                 ),
     *                       @OA\Property(
     *                     property="nameAp",
     *                     type="string",
     *                 ),
     *                       @OA\Property(
     *                     property="birthdayAp",
     *                     type="date",
     *                 ),
     *                       @OA\Property(
     *                     property="ocupationAp",
     *                     type="string",
     *                 ),
     *                        @OA\Property(
     *                     property="nacionalityAp",
     *                     type="string",
     *                 ),
     *                        @OA\Property(
     *                     property="emailAp",
     *                     type="string",
     *                 ),
     *                        @OA\Property(
     *                     property="phoneAp",
     *                     type="integer",
     *                 ),
     *                 example={}
     *             )
     *         )
     *     ),
     *
     *     @OA\Response(
     *         response=201,
     *         description="OK",
     *     ),
     *
     *     @OA\Response(
     *         response="400",
     *         description="Failed",
     *     ),
     *
     *
     *     deprecated=false
     * )
     */
    public function store_carrer(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'typeDoc' => 'required|integer',
                'document' => 'required|string',
                'lastname1' => 'required|string',
                'lastname2' => 'required|string',
                'names' => 'required|string',
                'birthday' => 'required|date',
                'sex' => 'required|string',
                'nacionality' => 'required|string',
                'adress' => 'required|string',
                'urbanization' => 'required|string',
                'district' => 'required|string',
                'province' => 'required|string',
                'homephone' => 'required|string',
                'phone' => 'required|string',
                'civilStatus' => 'required|string',
                'email' => 'required|string',
                'relationAp' => 'string',
                'documentAp' => 'integer',
                'lastname1Ap' => 'string',
                'lastname2Ap' => 'string',
                'nameAp' => 'string',
                'birthdayAp' => 'date',
                'ocupationAp' => 'string',
                'nacionalityAp' => 'string',
                'emailAp' => 'string',
                'phoneAp' => 'string',
            ]);

            if($validator->fails()){

                return $this->errorResponse($validator->errors()->first(), 400);
            }


            $interview = new InterviewsModel();
            $interview->type = InterviewsModel::CARRER;
            $interview->typeDoc = $request->typeDoc;
            $interview->document = $request->document;
            $interview->lastname1 = $request->lastname1;
            $interview->lastname2 = $request->lastname2;
            $interview->names = $request->names;
            $interview->birthday = $request->birthday;
            $interview->sex = $request->sex;
            $interview->nacionality = $request->nacionality;
            $interview->adress = $request->adress;
            $interview->urbanization = $request->urbanization;
            $interview->district = $request->district;
            $interview->province = $request->province;
            $interview->homephone = $request->homephone;
            $interview->phone = $request->phone;
            $interview->civilStatus = $request->civilStatus;
            $interview->email = $request->email;

            $age = Carbon::parse($request->birthday)->age;

            if ($age<18) {
                $interview->relationAp = $request->relationAp;
                $interview->documentAp = $request->documentAp;
                $interview->lastname1Ap = $request->lastname1Ap;
                $interview->lastname2Ap = $request->lastname2Ap;
                $interview->nameAp = $request->nameAp;
                $interview->birthdayAp = $request->birthdayAp;
                $interview->ocupationAp = $request->ocupationAp;
                $interview->nacionalityAp = $request->nacionalityAp;
                $interview->emailAp = $request->emailAp;
                $interview->phoneAp = $request->phoneAp;
            }

            $interview->save();

            return $this->createResponse([
                'status' => 201,
                'message' => 'Entrevista registrada',
                'data' => $interview,
            ]);
        } catch (\Exception $exception) {
            return $this->errorResponse($exception->getMessage(), 400);
        }

    }
    /**
     * @OA\Post(
     *     path="/interview/bussines",
     *     summary="Registrar una entrevista",
     *     tags={"Interviews"},
     *
     *     @OA\RequestBody(
     *         required=true,
     *         @OA\MediaType(
     *             mediaType="multipart/form-data",
     *             @OA\Schema(
     *                 @OA\Property(
     *                     property="typeDoc",
     *                     type="string",
     *                 ),
     *                 @OA\Property(
     *                     property="document",
     *                     type="integer",
     *                 ),
     *                 @OA\Property(
     *                     property="razonSocial",
     *                     type="string",
     *                 ),
     *                  @OA\Property(
     *                     property="pais",
     *                     type="string",
     *                 ),
     *                  @OA\Property(
     *                     property="names",
     *                     type="string",
     *                 ),
     *                   @OA\Property(
     *                     property="email",
     *                     type="string",
     *                 ),
     *                   @OA\Property(
     *                     property="ciudad",
     *                     type="date",
     *                 ),
     *                   @OA\Property(
     *                     property="cargo",
     *                     type="string",
     *                 ),
     *                   @OA\Property(
     *                     property="phone",
     *                     type="string",
     *                 ),
     *                   @OA\Property(
     *                     property="phoneWhats",
     *                     type="string",
     *                 ),
     *                    @OA\Property(
     *                     property="description",
     *                     type="string",
     *                 ),
     *                    @OA\Property(
     *                     property="category",
     *                     type="string",
     *                 ),
     *                    @OA\Property(
     *                     property="dateIni",
     *                     type="string",
     *                 ),
     *                    @OA\Property(
     *                     property="spots",
     *                     type="integer",
     *                 ),
     *                     @OA\Property(
     *                     property="experience",
     *                     type="integer",
     *                 ),
     *                     @OA\Property(
     *                     property="Levelexperience",
     *                     type="string",
     *                 ),
     *
     *                 example={}
     *             )
     *         )
     *     ),
     *
     *     @OA\Response(
     *         response=201,
     *         description="OK",
     *     ),
     *
     *     @OA\Response(
     *         response="400",
     *         description="Failed",
     *     ),
     *
     *
     *     deprecated=false
     * )
     */
    public function store_bussines(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'typeDoc' => 'required|string',
                'document' => 'required|integer',
                'razonSocial' => 'required|string',
                'pais' => 'required|string',
                'names' => 'required|string',
                'email' => 'required|string',
                'ciudad' => 'required|string',
                'cargo' => 'required|string',
                'phone' => 'required|integer',
                'phoneWhats' => 'required|integer',
                'description' => 'required|string',
                'category' => 'required|string',
                'dateIni' => 'required|date',
                'spots' => 'required|integer',
                'experience' => 'boolean|string',
                'Levelexperience' => 'required|string',
            ]);

            if($validator->fails()){

                return $this->errorResponse($validator->errors()->first(), 400);
            }


            $interview = new InterviewsModel();
            $interview->type = InterviewsModel::BUSSINES;
            $interview->typeDoc = $request->typeDoc;
            $interview->document = $request->document;
            $interview->razonSocial = $request->razonSocial;
            $interview->pais = $request->pais;
            $interview->names = $request->names;
            $interview->ciudad = $request->ciudad;
            $interview->cargo = $request->cargo;

            $interview->phone = $request->phone;
            $interview->homephone = $request->phoneWhats;
            $interview->email = $request->email;

            $interview->description = $request->description;
            $interview->category = $request->category;
            $interview->dateIni = $request->dateIni;
            $interview->spots = $request->spots;
            $interview->experience = $request->experience;
            $interview->Levelexperience = $request->Levelexperience;

            $interview->save();

            return $this->createResponse([
                'status' => 201,
                'message' => 'Entrevista registrada',
                'data' => $interview,
            ]);
        } catch (\Exception $exception) {
            return $this->errorResponse($exception->getMessage(), 400);
        }

    }
}
