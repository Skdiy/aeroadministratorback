<?php

namespace App\Http\Controllers;

use App\Models\ZoomMeeting;
use App\Traits\ApiResponse;
use App\Traits\ZoomMeetingTrait;
use Illuminate\Http\Request;

class MeetingController extends Controller
{
    use ApiResponse;
    use ZoomMeetingTrait;

    const MEETING_TYPE_INSTANT = 1;
    const MEETING_TYPE_SCHEDULE = 2;
    const MEETING_TYPE_RECURRING = 3;
    const MEETING_TYPE_FIXED_RECURRING_FIXED = 8;

    /**
     * @OA\Get(
     *     path="/admi/meeting/{id}",
     *     summary="Obtener una reunión.",
     *     tags={"Meetings"},
     *     @OA\Parameter(
     *          name="id",
     *          description="Identificador de zoom",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *     @OA\Response(
     *         response=200,
     *         description="OK",
     *     ),
     *
     *     @OA\Response(
     *         response="400",
     *         description="Failed",
     *     ),
     *
     *     security={{"apiAuth": {} }},
     *     deprecated=false
     * )
     */

     public function show($id)
    {
        $meeting = $this->get($id);

        return $this->createResponse($meeting);
    }
    /**
     * @OA\Post(
     *     path="/admi/meeting/",
     *     summary="Agregar una reunión nueva",
     *     tags={"Meetings"},
     *
     *     @OA\RequestBody(
     *         required=true,
     *         @OA\MediaType(
     *             mediaType="multipart/form-data",
     *             @OA\Schema(
     *                 @OA\Property(
     *                     property="topic",
     *                     type="string",
     *                 ),
     *                 @OA\Property(
     *                     property="start_time",
     *                     type="dateTime"
     *                 ),
     *                 @OA\Property(
     *                     property="duration",
     *                     type="integer"
     *                 ),
     *                 @OA\Property(
     *                     property="agenda",
     *                     type="string"
     *                 ),
     *
     *                 example={"topic": 70, "start_time": 0.5, "duration": 1, "course_id": 1, "agenda":"ni idea"}
     *             )
     *         )
     *     ),
     *
     *     @OA\Response(
     *         response=201,
     *         description="OK",
     *     ),
     *
     *     @OA\Response(
     *         response="400",
     *         description="Failed",
     *     ),
     *
     *     security={{"apiAuth": {} }},
     *
     *     deprecated=false
     * )
     */
    public function store(Request $request)
    {

        $meeting = $this->create($request->all());

        if ($meeting['success']) {
            $Newmeeting = new ZoomMeeting();
            $Newmeeting->zoom_id = $meeting['data']['id'];
            $Newmeeting->join_url = $meeting['data']['join_url'];
            $Newmeeting->password = $meeting['data']['password'];
            $Newmeeting->topic = $request->topic;
            $Newmeeting->start_time = $request->start_time;
            $Newmeeting->duration = $request->duration;
            $Newmeeting->agenda = $request->agenda;
            $Newmeeting->save();
        }else {
            return $this->errorResponse('No se pudo crear la reunión.', 400);
        }

        return $this->createResponse([
            'status' => 201,
            'message' => 'Reunión registrada',
            'data' => $Newmeeting,
        ]);
    }
    /**
     * @OA\Patch(
     *     path="/admi/meeting/{id}",
     *     summary="Actualizar una reunión.",
     *     tags={"Meetings"},
     *      @OA\Parameter(
     *          name="id",
     *          description="Identificador de zoom",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *     @OA\RequestBody(
     *         required=true,
     *         @OA\MediaType(
     *             mediaType="multipart/form-data",
     *             @OA\Schema(
     *                 @OA\Property(
     *                     property="topic",
     *                     type="string",
     *                 ),
     *                 @OA\Property(
     *                     property="start_time",
     *                     type="dateTime"
     *                 ),
     *                 @OA\Property(
     *                     property="duration",
     *                     type="integer"
     *                 ),
     *                 @OA\Property(
     *                     property="agenda",
     *                     type="string"
     *                 ),
     *
     *                 example={"topic": 70, "start_time": 0.5, "duration": 1, "course_id": 1, "agenda":"ni idea"}
     *             )
     *         )
     *     ),
     *
     *     @OA\Response(
     *         response=201,
     *         description="OK",
     *     ),
     *
     *     @OA\Response(
     *         response="400",
     *         description="Failed",
     *     ),
     *
     *     security={{"apiAuth": {} }},
     *
     *     deprecated=false
     * )
     */
    public function updateMeet($id, Request $request)
    {
        $meet = ZoomMeeting::where('zoom_id',$id)->get()->first();
        if ($meet != null) {

            $meeting = $this->update($id, $request->all());

            if (!$meeting['success'])
            {
                return $this->errorResponse('No se pudo actualizar la reunión.', 400);
            }else{
                $meet->update($request->all());
            }
        }else {
            return $this->errorResponse('No se pudo actualizar la reunión.', 400);
        }
        return $this->createResponse([
            'status' => 201,
            'message' => 'Reunión actualizada',
            'data' => $meeting,
        ]);
    }
    /**
     * @OA\Delete(
     *     path="/admi/meeting/{id}",
     *     summary="Obtener una reunión.",
     *     tags={"Meetings"},
     *     @OA\Parameter(
     *          name="id",
     *          description="Identificador de zoom",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *     @OA\Response(
     *         response=201,
     *         description="OK",
     *     ),
     *
     *     @OA\Response(
     *         response="400",
     *         description="Failed",
     *     ),
     *
     *     security={{"apiAuth": {} }},
     *     deprecated=false
     * )
     */

    public function destroy($id)
    {
        $meet = ZoomMeeting::where('zoom_id',$id)->get()->first();
        if ($meet != null) {
            $meeting = $this->delete($id);
            if (!$meeting['success'])
            {
                return $this->errorResponse('No se pudo eliminar la reunión.', 400);
            }else{

                $meet->delete();
            }
        }


        return $this->createResponse([
            'status' => 201,
            'message' => 'Reunión eliminada',
        ]);
    }
}
