<?php

namespace App\Http\Controllers;

use App\Models\Course;
use App\Models\Files;
use App\Models\NotesModel;
use App\Models\ProyectResource;
use App\Models\ProyectsCourse;
use App\Models\ProyectsUserModel;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ProyectController extends Controller
{
    /**
     * @OA\Get(
     *     path="/admin/proyects/{course_id}",
     *     summary="Obetener todas los proyectos de un curso.",
     *     tags={"Proyects"},
     *
     *     @OA\Response(
     *         response=200,
     *         description="OK",
     *     ),
     *
     *     @OA\Response(
     *         response="400",
     *         description="Failed",
     *     ),
     *
     *
     *     deprecated=false
     * )
     */
    public function getAllProyects($course_id)
    {
        try {
            $proyects = ProyectsCourse::where('course_id',$course_id)
            ->select('proyects_courses.*','files.url')
            ->get();
        } catch (\Exception $exception) {
            return $this->errorResponse($exception->getMessage(), 400);
        }

        return $this->successResponse([
            'status' => 200,
            'data' => $proyects ,
        ]);
    }
    /**
     * @OA\Get(
     *     path="/admin/proyects/resources/{proyect_id}",
     *     summary="Obetener todas los recursos de un proyecto.",
     *     tags={"Proyects"},
     *
     *     @OA\Response(
     *         response=200,
     *         description="OK",
     *     ),
     *
     *     @OA\Response(
     *         response="400",
     *         description="Failed",
     *     ),
     *
     *
     *     deprecated=false
     * )
     */
    public function getResourcesProyects($proyect_id)
    {
        try {
            $resources = ProyectResource::where('proyect_id',$proyect_id)
            ->get();
        } catch (\Exception $exception) {
            return $this->errorResponse($exception->getMessage(), 400);
        }

        return $this->successResponse([
            'status' => 200,
            'data' => $resources
        ]);
    }
    /**
     * @OA\Get(
     *     path="/admin/proyects/user/{course_id}/{user_id}",
     *     summary="Obetener todas los proyectos de un alumno en un curso.",
     *     tags={"Proyects"},
     *
     *     @OA\Response(
     *         response=200,
     *         description="OK",
     *     ),
     *
     *     @OA\Response(
     *         response="400",
     *         description="Failed",
     *     ),
     *
     *
     *     deprecated=false
     * )
     */
    public function getUserProyects($course_id,$user_id)
    {
        try {
            $proyects = ProyectsUserModel::join('proyects_courses','proyects_user_models.proyect_id','proyects_courses.id')
            ->join('files','proyects_user_models.file_id','files.id')
            ->join('users','proyects_user_models.user_id','users.id')
            ->where('proyects_courses.course_id',$course_id)
            ->where('proyects_user_models.user_id',$user_id)
            ->select('proyects_user_models.*','files.url','users.name','users.surname')
            ->get();
        } catch (\Exception $exception) {
            return $this->errorResponse($exception->getMessage(), 400);
        }

        return $this->successResponse([
            'status' => 200,
            'data' => $proyects
        ]);
    }
    /**
     * @OA\Get(
     *     path="/proyects/user/{course_id}",
     *     summary="Obetener mis proyectos de un curso.",
     *     tags={"Proyects"},
     *
     *     @OA\Response(
     *         response=200,
     *         description="OK",
     *     ),
     *
     *     @OA\Response(
     *         response="400",
     *         description="Failed",
     *     ),
     *
     *
     *     deprecated=false
     * )
     */
    public function getMyProyects($course_id)
    {
        try {
            $proyects = ProyectsCourse::where('proyects_courses.course_id',$course_id)
            ->select('proyects_user_models.*','files.url')
            ->get();

            foreach ($proyects as $key ) {
                $user = ProyectsUserModel::where('user_id',auth()->user()->id)
                ->where('proyect_id',$key->id)
                ->get()->first();

                if ($user != null) {
                    $key['delivered'] = true;
                }else {
                    $key['delivered'] = false;
                }

            }

        } catch (\Exception $exception) {
            return $this->errorResponse($exception->getMessage(), 400);
        }

        return $this->successResponse([
            'status' => 200,
            'data' => $proyects
        ]);
    }
    /**
     * @OA\Post(
     *     path="/admin/proyect/",
     *     summary="Crear nuevo proyecto",
     *     tags={"Proyects"},
     *
     *     @OA\RequestBody(
     *         required=true,
     *         @OA\MediaType(
     *             mediaType="multipart/form-data",
     *             @OA\Schema(
     *                 @OA\Property(
     *                     property="title",
     *                     type="string",
     *                 ),
     *                 @OA\Property(
     *                     property="description",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="descriptionProyect",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="course_id",
     *                     type="integer"
     *                 ),
     *                 @OA\Property(
     *                     property="resources",
     *                     type="file[]"
     *                 ),
     *
     *                 example={"title": "Proyecto 1", "description": "Nuevo proyecto de prueba", "descriptionProyect": "Nuevo proyecto de prueba", "course_id": 1}
     *             )
     *         )
     *     ),
     *
     *     @OA\Response(
     *         response=201,
     *         description="OK",
     *     ),
     *
     *     @OA\Response(
     *         response="400",
     *         description="Failed",
     *     ),
     *
     *     security={{"apiAuth": {} }},
     *
     *     deprecated=false
     * )
     */
    public function addProyect(Request $request)
    {
        try {
            if (auth()->user()->rol != User::ROL_PROFESOR ) {
                return $this->errorResponse('No tiene acceso a esta opciones. Consulte con los administradores del sistema', 401);
            }

            $validator = Validator::make($request->all(), [
                'title' => 'required|string',
                'description' => 'required|string',
                'descriptionProyect' => 'required|string',
                'course_id' => 'required|integer',
            ]);

            if($validator->fails()){

                return $this->errorResponse($validator->errors()->first(), 400);
            }

            $course = Course::findOrFail($request->course_id);
            if ($course!= null) {
                $proyect = new ProyectsCourse();
                $proyect->course_id = $request->course_id;
                $proyect->title = $request->title;
                $proyect->description = $request->description;
                $proyect->descriptionProyect = $request->descriptionProyect;
                $proyect->save();

                if ($request->has('resources')) {
                    $files = $request->file('image');
                    foreach($files as $n_file){
                        $file = new Files();
                        $name = "proyect_" .$proyect->id.'_'. time() . "." . $n_file->guessExtension();
                        $ruta = public_path("images/proyects/" . $name);
                        copy($n_file, $ruta);
                        $file->url = "images/proyects/" . $name;
                        $file->save();

                        $resource = new ProyectResource();
                        $resource->proyect_id = $proyect->id;
                        $resource->file_id = $file->id;
                        $resource->save();
                    }
                }

            }else{
                return $this->errorResponse('Curso no encontrado.', 400);
            }

            return $this->createResponse([
                'status' => 201,
                'message' => 'Proyecto registrado',
                'data' => $proyect,
            ]);
        } catch (\Exception $exception) {
            return $this->errorResponse($exception->getMessage(), 400);
        }
    }

    /**
     * @OA\Post(
     *     path="/proyect/",
     *     summary="Entregar un proyecto",
     *     tags={"Proyects"},
     *
     *     @OA\RequestBody(
     *         required=true,
     *         @OA\MediaType(
     *             mediaType="multipart/form-data",
     *             @OA\Schema(
     *                 @OA\Property(
     *                     property="proyect_id",
     *                     type="integer",
     *                 ),
     *                 @OA\Property(
     *                     property="resource",
     *                     type="file"
     *                 ),
     *                 @OA\Property(
     *                     property="response",
     *                     type="string"
     *                 ),
     *
     *
     *                 example={"title": "Proyecto 1", "description": "Nuevo proyecto de prueba", "descriptionProyect": "Nuevo proyecto de prueba", "course_id": 1}
     *             )
     *         )
     *     ),
     *
     *     @OA\Response(
     *         response=201,
     *         description="OK",
     *     ),
     *
     *     @OA\Response(
     *         response="400",
     *         description="Failed",
     *     ),
     *
     *     security={{"apiAuth": {} }},
     *
     *     deprecated=false
     * )
     */
    public function addResponseProyect(Request $request)
    {
        try {

            $validator = Validator::make($request->all(), [
                'proyect_id' => 'required|integer',
                'resource' => 'file',
                'response' => 'required|string',
            ]);

            if($validator->fails()){

                return $this->errorResponse($validator->errors()->first(), 400);
            }

            $proyect = ProyectsCourse::findOrFail($request->proyect_id);
            if ($proyect!= null) {
                $Userproyect = new ProyectsUserModel();
                $Userproyect->proyect_id = $request->proyect_id;
                $Userproyect->user_id = auth()->user()->id;
                $Userproyect->response = $request->response;
                $Userproyect->status = ProyectsUserModel::PENDIENTE;


                if ($request->has('resource')) {
                    $files = $request->file('image');
                    foreach($files as $n_file){
                        $file = new Files();
                        $name = "proyect_" .$Userproyect->id.'_'. time() . "." . $n_file->guessExtension();
                        $ruta = public_path("images/proyects/" . $name);
                        copy($n_file, $ruta);
                        $file->url = "images/proyects/" . $name;
                        $file->save();
                        $Userproyect->file_id = $request->file_id;
                    }
                }else{
                    $Userproyect->file_id = '';
                }

                $Userproyect->save();

            }else{
                return $this->errorResponse('Proyecto no encontrado.', 400);
            }

            return $this->createResponse([
                'status' => 201,
                'message' => 'Respuesta registrada',
                'data' => $proyect,
            ]);
        } catch (\Exception $exception) {
            return $this->errorResponse($exception->getMessage(), 400);
        }
    }
    /**
     * @OA\Post(
     *     path="/admin/proyect/rate",
     *     summary="Calificar un proyecto",
     *     tags={"Proyects"},
     *
     *     @OA\RequestBody(
     *         required=true,
     *         @OA\MediaType(
     *             mediaType="multipart/form-data",
     *             @OA\Schema(
     *                 @OA\Property(
     *                     property="proyect_id",
     *                     type="integer",
     *                 ),
     *                 @OA\Property(
     *                     property="user_id",
     *                     type="integer"
     *                 ),
     *                 @OA\Property(
     *                     property="note",
     *                     type="integer"
     *                 ),
     *                 @OA\Property(
     *                     property="percent",
     *                     type="decimal"
     *                 ),
     *
     *
     *                 example={"proyect_id": 1, "user_id": 1, "note": 1, "percent": 0.4}
     *             )
     *         )
     *     ),
     *
     *     @OA\Response(
     *         response=201,
     *         description="OK",
     *     ),
     *
     *     @OA\Response(
     *         response="400",
     *         description="Failed",
     *     ),
     *
     *     security={{"apiAuth": {} }},
     *
     *     deprecated=false
     * )
     */
    public function RateProyect(Request $request)
    {
        try {
            if (auth()->user()->rol != User::ROL_PROFESOR ) {
                return $this->errorResponse('No tiene acceso a esta opciones. Consulte con los administradores del sistema', 401);
            }

            $validator = Validator::make($request->all(), [
                'proyect_id' => 'required|integer',
                'user_id' => 'required|integer',
                'note' => 'required|integer',
                'percent' => 'required|decimal',
            ]);

            if($validator->fails()){

                return $this->errorResponse($validator->errors()->first(), 400);
            }

            $proyect = ProyectsUserModel::where('proyect_id',$request->proyect_id)
            ->where('user_id',$request->user_id)
            ->get()->first();
            if ($proyect!= null && $proyect->status != ProyectsUserModel::REVISADO) {
                $note = new NotesModel();
                $note->user_id = $request->user_id;
                $note->course_id = $proyect->course_id;
                $note->proyect_id = $request->proyect_id;
                $note->note = $request->note;
                $note->percent = $request->percent;
                $note->type = NotesModel::PROYECT;
                $note->save();
                $proyect->status = ProyectsUserModel::REVISADO;
                $proyect->update();
            }else{
                return $this->errorResponse('Proyecto no encontrado.', 400);
            }

            return $this->createResponse([
                'status' => 201,
                'message' => 'Respuesta registrada',
                'data' => $note,
            ]);
        } catch (\Exception $exception) {
            return $this->errorResponse($exception->getMessage(), 400);
        }
    }
}
