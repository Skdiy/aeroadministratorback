<?php

namespace App\Http\Controllers;

use App\Models\CommentsLiveStreamModel;
use App\Models\LiveStreamModel;
use App\Models\User;
use App\Traits\ApiResponse;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class LiveStreamController extends Controller
{
    use ApiResponse;
    /**
     * @OA\Get(
     *     path="/liveStream/",
     *     summary="Obtener proximo liveStream",
     *     tags={"LiveStream"},
     *
     *     @OA\Response(
     *         response=200,
     *         description="OK",
     *     ),
     *
     *     @OA\Response(
     *         response="400",
     *         description="Failed",
     *     ),
     *
     *
     *     deprecated=false
     * )
     */
    public function get_recent()
    {
        try {
            $ldate = date('Y-m-d');
            $ltime = date('H:i:s');
            $liveStream = LiveStreamModel::join('users','users.id','live_stream.teacher_id')
            ->select('title','live_stream.description','live_stream.photo','date','time','views','status','users.name','users.surname')
            ->whereDate('date','>=',$ldate)
            ->orderBy('date', 'DESC')
            ->orderBy('time', 'DESC')
            ->get()->first();

            return $this->createResponse([
                'status' => 200,
                'data' => $liveStream,
            ]);
        } catch (\Exception $exception) {
            return $this->errorResponse($exception->getMessage(), 400);
        }
    }
    /**
     * @OA\Get(
     *     path="/liveStream/all",
     *     summary="Obtener todos los liveStream",
     *     tags={"LiveStream"},
     *
     *     @OA\Response(
     *         response=200,
     *         description="OK",
     *     ),
     *
     *     @OA\Response(
     *         response="400",
     *         description="Failed",
     *     ),
     *
     *
     *     deprecated=false
     * )
     */
    public function get_all()
    {
        try {
            $liveStream = LiveStreamModel::join('users','users.id','live_stream.teacher_id')
            ->select('live_stream.id','title','live_stream.photo','date','time','views','status','users.name','users.surname')
            ->orderBy('date', 'ASC')
            ->orderBy('time', 'ASC')
            ->get();

            return $this->createResponse([
                'status' => 200,
                'data' => $liveStream,
            ]);
        } catch (\Exception $exception) {
            return $this->errorResponse($exception->getMessage(), 400);
        }
    }
    /**
     * @OA\Get(
     *     path="/liveStream/{live_stream_id}",
     *     summary="Obtener todos los datos de un liveStream",
     *     tags={"LiveStream"},
     *      @OA\Parameter(
     *          name="live_stream_id",
     *          description="Identificador de liveStream",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *     @OA\Response(
     *         response=200,
     *         description="OK",
     *     ),
     *
     *     @OA\Response(
     *         response="400",
     *         description="Failed",
     *     ),
     *
     *
     *     deprecated=false
     * )
     */
    public function get($live_stream_id)
    {
        try {
            $liveStream = LiveStreamModel::join('users','users.id','live_stream.teacher_id')
            ->select('live_stream.*','users.name','users.surname')
            ->where('live_stream.id',$live_stream_id)
            ->get()->first();

            $comments = CommentsLiveStreamModel::join('users','users.id','comments_live_stream.user_id')
            ->where('live_stream_id',$live_stream_id)
            ->select('comments_live_stream.*','users.name','users.surname','users.rol')
            ->where('comments_live_stream.type',CommentsLiveStreamModel::COMMENT)
            ->orderBy('comments_live_stream.created_at', 'ASC')
            ->get();

            $liveStream['comments'] = $comments;

            return $this->createResponse([
                'status' => 200,
                'data' => $liveStream,
            ]);
        } catch (\Exception $exception) {
            return $this->errorResponse($exception->getMessage(), 400);
        }
    }

    /**
     * @OA\Get(
     *     path="/liveStream/view/{live_stream_id}",
     *     summary="Aumentar las vistas de un liveStream",
     *     tags={"LiveStream"},
     *      @OA\Parameter(
     *          name="live_stream_id",
     *          description="Identificador de liveStream",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *     @OA\Response(
     *         response=200,
     *         description="OK",
     *     ),
     *
     *     @OA\Response(
     *         response="400",
     *         description="Failed",
     *     ),
     *
     *
     *     deprecated=false
     * )
     */
    public function add_view($live_stream_id)
    {
        try {
            $liveStream = LiveStreamModel::where('live_stream_id.id',$live_stream_id)
            ->get()->first();

            if ($liveStream == null) {
                return $this->errorResponse('No se encontro liveStream', 400);
            }

            $liveStream->view += 1;
            $liveStream->update();

            return $this->createResponse([
                'status' => 200,
                'views' => $liveStream->view,
            ]);
        } catch (\Exception $exception) {
            return $this->errorResponse($exception->getMessage(), 400);
        }
    }

    /**
     * @OA\Post(
     *     path="/admi/liveStream/",
     *     summary="Registrar un liveStream",
     *     tags={"LiveStream"},
     *
     *     @OA\RequestBody(
     *         required=true,
     *         @OA\MediaType(
     *             mediaType="multipart/form-data",
     *             @OA\Schema(
     *                 @OA\Property(
     *                     property="title",
     *                     type="string",
     *                 ),
     *                 @OA\Property(
     *                     property="description",
     *                     type="string",
     *                 ),
     *                 @OA\Property(
     *                     property="photo",
     *                     type="file",
     *                 ),
     *              @OA\Property(
     *                     property="url",
     *                     type="string",
     *                 ),
     *                  @OA\Property(
     *                     property="teacher_id",
     *                     type="integer",
     *                 ),
     *                  @OA\Property(
     *                     property="date",
     *                     type="date",
     *                 ),
     *                  @OA\Property(
     *                     property="time",
     *                     type="time",
     *                 ),
     *                 example={}
     *             )
     *         )
     *     ),
     *
     *     @OA\Response(
     *         response=201,
     *         description="OK",
     *     ),
     *
     *     @OA\Response(
     *         response="400",
     *         description="Failed",
     *     ),
     *
     *     security={{"apiAuth": {} }},
     *
     *     deprecated=false
     * )
     */
    public function store(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'title' => 'required|string',
                'description' => 'required|string',
                'photo' => 'required',
                'teacher_id' => 'required',
                'url' => 'required|string',
                'date' => 'required',
                'time' => 'required'
            ]);

            if($validator->fails()){

                return $this->errorResponse($validator->errors()->first(), 400);
            }

            $teacher = User::findOrFail($request->teacher_id);

            if ($teacher!=null) {
                $register = new LiveStreamModel();
                $register->title = $request->title;
                $register->description = $request->description;
                $register->teacher_id = $request->teacher_id;
                $register->url = $request->url;
                $register->date = $request->date;
                $register->time = $request->time;

                if ($image = $request->file('photo')) {
                    $name = "image_liveStream_". $request->name . '_' . time() . "." . $image->guessExtension();
                    $rutaIMG = public_path("images/liveStreams/image/" . $name);
                    copy($image, $rutaIMG);
                }else{
                    return $this->errorResponse('Error al grabar la imagen.', 400);
                }
                $register->photo = $name;
                $register->status = LiveStreamModel::SININICIAR;
                $register->save();
            }else{
                return $this->errorResponse("Error con los identificadores.", 400);
            }

            return $this->createResponse([
                'status' => 201,
                'message' => 'LiveStream registrado.',
                'data' => $register,
            ]);
        } catch (\Exception $exception) {
            return $this->errorResponse($exception->getMessage(), 400);
        }
    }

    /**
     * @OA\Post(
     *     path="/admi/liveStream/video",
     *     summary="Registrar la grabación de un liveStream",
     *     tags={"LiveStream"},
     *
     *     @OA\RequestBody(
     *         required=true,
     *         @OA\MediaType(
     *             mediaType="multipart/form-data",
     *             @OA\Schema(
     *                 @OA\Property(
     *                     property="live_stream_id",
     *                     type="string",
     *                 ),
     *
     *                 @OA\Property(
     *                     property="video",
     *                     type="file",
     *                 ),
     *                 example={}
     *             )
     *         )
     *     ),
     *
     *     @OA\Response(
     *         response=201,
     *         description="OK",
     *     ),
     *
     *     @OA\Response(
     *         response="400",
     *         description="Failed",
     *     ),
     *
     *     security={{"apiAuth": {} }},
     *
     *     deprecated=false
     * )
     */
    public function upload_video(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'live_stream_id' => 'required|integer',
                'video' => 'required',
            ]);

            if($validator->fails()){

                return $this->errorResponse($validator->errors()->first(), 400);
            }

            $liveStream = LiveStreamModel::findOrFail($request->live_stream_id);

            if ($liveStream!=null) {

                if ($image = $request->file('video')) {
                    $name = "video_liveStream_". $request->name . '_' . time() . "." . $image->guessExtension();
                    $rutaIMG = public_path("images/liveStreams/video/" . $name);
                    copy($image, $rutaIMG);
                }else{
                    return $this->errorResponse('Error al grabar la imagen.', 400);
                }
                $liveStream->photo = $rutaIMG;
                $liveStream->update();
            }else{
                return $this->errorResponse("Error con los identificadores.", 400);
            }

            return $this->createResponse([
                'status' => 201,
                'message' => 'LiveStream actualizado.',
                'data' => $liveStream,
            ]);
        } catch (\Exception $exception) {
            return $this->errorResponse($exception->getMessage(), 400);
        }
    }
}
