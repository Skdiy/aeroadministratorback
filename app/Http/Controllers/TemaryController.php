<?php

namespace App\Http\Controllers;

use App\Models\ClassTemary;
use App\Models\ProgramClassModel;
use App\Models\ProgramTemaryModel;
use App\Models\TemaryCourse;
use App\Models\User;
use App\Traits\ApiResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class TemaryController extends Controller
{
    use ApiResponse;

    /**
     * @OA\Get(
     *     path="/course/temary/{course_id}",
     *     summary="Obtener todos los temarios de un curso.",
     *     tags={"Temario"},
     *     @OA\Parameter(
     *          name="course_id",
     *          description="Identificador de curso",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *     @OA\Response(
     *         response=200,
     *         description="OK",
     *     ),
     *
     *     @OA\Response(
     *         response="400",
     *         description="Failed",
     *     ),
     *
     *
     *     deprecated=false
     * )
     */
    public function get($course_id)
    {
        try {
            $temary = TemaryCourse::where('course_id',$course_id)
            ->get();

            foreach ($temary as $key) {
                $class = ClassTemary::where('temary_id',$key->id)->get();
                $key['classes'] = $class;
            }
        } catch (\Exception $exception) {
            return $this->errorResponse($exception->getMessage(), 400);
        }

        return $this->successResponse([
            'status' => 200,
            'data' => [
                'temary' => $temary
            ],
        ]);
    }
    /**
     * @OA\Post(
     *     path="/admi/course/temary/{course_id}",
     *     summary="Agrega un Temario",
     *     tags={"Temario"},
     *     @OA\Parameter(
     *          name="course_id",
     *          description="Identificador de curso",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *     @OA\RequestBody(
     *         required=true,
     *         @OA\MediaType(
     *             mediaType="multipart/form-data",
     *             @OA\Schema(
     *                 @OA\Property(
     *                     property="name",
     *                     type="string",
     *                 ),
     *                 example={"name": "name"}
     *             )
     *         )
     *     ),
     *
     *     @OA\Response(
     *         response=201,
     *         description="OK",
     *     ),
     *
     *     @OA\Response(
     *         response="400",
     *         description="Failed",
     *     ),
     *
     *     security={{"apiAuth": {} }},
     *
     *     deprecated=false
     * )
     */
    public function add(Request $request,$course_id)
    {
        try {
            if (auth()->user()->rol < 5 ) {
                return $this->errorResponse('No tiene acceso a esta opciones. Consulte con los administradores del sistema', 401);
            }

            $validator = Validator::make($request->all(), [
                'name' => 'required|string',
            ]);

            if($validator->fails()){

                return $this->errorResponse($validator->errors()->first(), 400);
            }

            $temary = new TemaryCourse();
            $temary->course_id = $course_id;
            $temary->name = $request->name;
            $temary->state = true;
            $temary->save();

            return $this->createResponse([
                'status' => 201,
                'message' => 'Temario registrado',
                'data' => $temary,
            ]);
        } catch (\Exception $exception) {
            return $this->errorResponse($exception->getMessage(), 400);
        }
    }
    /**
     * @OA\Post(
     *     path="/admi/course/temary/class/{temary_id}",
     *     summary="Agrega una clase",
     *     tags={"Temario"},
     *     @OA\Parameter(
     *          name="temary_id",
     *          description="Identificador de curso",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *     @OA\RequestBody(
     *         required=true,
     *         @OA\MediaType(
     *             mediaType="multipart/form-data",
     *             @OA\Schema(
     *                 @OA\Property(
     *                     property="name",
     *                     type="string",
     *                 ),
     *                  @OA\Property(
     *                     property="state",
     *                     type="boolean",
     *                 ),
     *                 @OA\Property(
     *                     property="urlRecord",
     *                     type="file",
     *                 ),
     *             )
     *         )
     *     ),
     *
     *     @OA\Response(
     *         response=201,
     *         description="OK",
     *     ),
     *
     *     @OA\Response(
     *         response="400",
     *         description="Failed",
     *     ),
     *
     *     security={{"apiAuth": {} }},
     *
     *     deprecated=false
     * )
     */
    public function addClass(Request $request,$temary_id)
    {
        try {
            if (auth()->user()->rol < 5 ) {
                return $this->errorResponse('No tiene acceso a esta opciones. Consulte con los administradores del sistema', 401);
            }

            $validator = Validator::make($request->all(), [
                'name' => 'required|string',
                'urlRecord' => 'required'
            ]);

            if($validator->fails()){

                return $this->errorResponse($validator->errors()->first(), 400);
            }

            $class = new ClassTemary();

            if ($image = $request->file('urlRecord')) {
                $name = "images/classes/video/" ."video_class_". $request->name . '_' . time() . "." . $image->guessExtension();
                $ruta = public_path( $name);
                copy($image, $ruta);
                $class->urlRecord = $name;
            }else{
                return $this->errorResponse('Error al grabar el video.', 400);
            }

            $class->temary_id = $temary_id;
            $class->name = $request->name;
            $class->state = $request->state;
            $class->save();

            return $this->createResponse([
                'status' => 201,
                'message' => 'Clase registrada',
                'data' => $class,
            ]);
        } catch (\Exception $exception) {
            return $this->errorResponse($exception->getMessage(), 400);
        }
    }
    /**
     * @OA\Patch(
     *     path="/admi/course/temary/class/{class_id}",
     *     summary="Actualizar una clase",
     *     tags={"Temario"},
     *     @OA\Parameter(
     *          name="temary_id",
     *          description="Identificador de curso",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *     @OA\RequestBody(
     *         required=true,
     *         @OA\MediaType(
     *             mediaType="multipart/form-data",
     *             @OA\Schema(
     *                 @OA\Property(
     *                     property="name",
     *                     type="string",
     *                 ),
     *                  @OA\Property(
     *                     property="state",
     *                     type="boolean",
     *                 ),
     *                 @OA\Property(
     *                     property="urlRecord",
     *                     type="file",
     *                 ),
     *             )
     *         )
     *     ),
     *
     *     @OA\Response(
     *         response=201,
     *         description="OK",
     *     ),
     *
     *     @OA\Response(
     *         response="400",
     *         description="Failed",
     *     ),
     *
     *     security={{"apiAuth": {} }},
     *
     *     deprecated=false
     * )
     */
    public function aupdateClass(Request $request,$class_id)
    {
        try {
            if (auth()->user()->rol != 5 ) {
                return $this->errorResponse('No tiene acceso a esta opciones. Consulte con los administradores del sistema', 401);
            }

            $validator = Validator::make($request->all(), [
                'name' => 'required|string',
                'state' => 'required|boolean',
                'urlRecord' => 'required'
            ]);

            if($validator->fails()){

                return $this->errorResponse($validator->errors()->first(), 400);
            }

            $class = ClassTemary::find($class_id);

            if ($image = $request->file('video')) {
                $name = "video_class_". $request->name . '_' . time() . "." . $image->guessExtension();
                $ruta = public_path("images/classes/video/" . $name);
                copy($image, $ruta);
                $class->urlRecord = $ruta;
            }

            $class->name = $request->name;
            $class->state = true;
            $class->update();

            return $this->createResponse([
                'status' => 201,
                'message' => 'Clase registrada',
                'data' => $class,
            ]);
        } catch (\Exception $exception) {
            return $this->errorResponse($exception->getMessage(), 400);
        }
    }

    /**
     * @OA\Delete(
     *     path="/admi/course/temary/",
     *     summary="Elimina Temario de un curso.",
     *     tags={"Temario"},
     *
     *     @OA\RequestBody(
     *         required=true,
     *         @OA\MediaType(
     *             mediaType="multipart/form-data",
     *             @OA\Schema(
     *                 @OA\Property(
     *                     property="temary_id",
     *                     type="string",
     *                 ),
     *                 example={}
     *             )
     *         )
     *     ),
     *
     *     @OA\Response(
     *         response=200,
     *         description="OK",
     *     ),
     *
     *     @OA\Response(
     *         response="400",
     *         description="Failed",
     *     ),
     *
     *     security={{"apiAuth": {} }},
     *
     *     deprecated=false
     * )
     */
    public function Delete(Request $request)
    {

        try {
            $Temary = TemaryCourse::find($request->temary_id);
            if ($Temary != null) {
                $Temary->delete();
            }else{
                return $this->errorResponse('No se encontro identificador.', 400);
            }


        } catch (\Exception $exception) {
            return $this->errorResponse($exception->getMessage(), 400);
        }

        return $this->successResponse([
            'status' => 200
        ]);
    }
    /**
     * @OA\Delete(
     *     path="/admi/course/temary/class",
     *     summary="Elimina una clase de un Temario.",
     *     tags={"Temario"},
     *
     *     @OA\RequestBody(
     *         required=true,
     *         @OA\MediaType(
     *             mediaType="multipart/form-data",
     *             @OA\Schema(
     *                 @OA\Property(
     *                     property="class_id",
     *                     type="string",
     *                 ),
     *                 example={}
     *             )
     *         )
     *     ),
     *
     *     @OA\Response(
     *         response=200,
     *         description="OK",
     *     ),
     *
     *     @OA\Response(
     *         response="400",
     *         description="Failed",
     *     ),
     *
     *     security={{"apiAuth": {} }},
     *
     *     deprecated=false
     * )
     */
    public function DeleteClass(Request $request)
    {

        try {
            $Class = ClassTemary::find($request->class_id);
            if ($Class != null) {
                $Class->delete();
            }else{
                return $this->errorResponse('No se encontro identificador.', 400);
            }


        } catch (\Exception $exception) {
            return $this->errorResponse($exception->getMessage(), 400);
        }

        return $this->successResponse([
            'status' => 200
        ]);
    }



    /**
     * @OA\Get(
     *     path="/program/temary/{program_id}",
     *     summary="Obtener todos los temarios de un programa.",
     *     tags={"Programs"},
     *     @OA\Parameter(
     *          name="program_id",
     *          description="Identificador de programa",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *     @OA\Response(
     *         response=200,
     *         description="OK",
     *     ),
     *
     *     @OA\Response(
     *         response="400",
     *         description="Failed",
     *     ),
     *
     *
     *     deprecated=false
     * )
     */
    public function get_temary_program($program_id)
    {
        try {
            $temary = ProgramTemaryModel::where('program_id',$program_id)
            ->get();

            foreach ($temary as $key) {
                $class = ProgramClassModel::where('program_temary_id',$key->id)->get();
                $key['classes'] = $class;
            }
        } catch (\Exception $exception) {
            return $this->errorResponse($exception->getMessage(), 400);
        }

        return $this->successResponse([
            'status' => 200,
            'data' => $temary,
        ]);
    }
    /**
     * @OA\Post(
     *     path="/admi/program/temary/{program_id}",
     *     summary="Agrega un Temario a un programa",
     *     tags={"Temario"},
     *     @OA\Parameter(
     *          name="program_id",
     *          description="Identificador de programa",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *     @OA\RequestBody(
     *         required=true,
     *         @OA\MediaType(
     *             mediaType="multipart/form-data",
     *             @OA\Schema(
     *                 @OA\Property(
     *                     property="name",
     *                     type="string",
     *                 ),
     *                 example={"name": "name"}
     *             )
     *         )
     *     ),
     *
     *     @OA\Response(
     *         response=201,
     *         description="OK",
     *     ),
     *
     *     @OA\Response(
     *         response="400",
     *         description="Failed",
     *     ),
     *
     *     security={{"apiAuth": {} }},
     *
     *     deprecated=false
     * )
     */
    public function add_temary_program(Request $request,$program_id)
    {
        try {
            if (auth()->user()->rol < 5 ) {
                return $this->errorResponse('No tiene acceso a esta opciones. Consulte con los administradores del sistema', 401);
            }

            $validator = Validator::make($request->all(), [
                'name' => 'required|string',
            ]);

            if($validator->fails()){

                return $this->errorResponse($validator->errors()->first(), 400);
            }

            $temary = new ProgramTemaryModel();
            $temary->program_id = $program_id;
            $temary->name = $request->name;
            $temary->state = true;
            $temary->save();

            return $this->createResponse([
                'status' => 201,
                'message' => 'Temario registrado',
                'data' => $temary,
            ]);
        } catch (\Exception $exception) {
            return $this->errorResponse($exception->getMessage(), 400);
        }
    }
    /**
     * @OA\Post(
     *     path="/admi/program/temary/class/{temary_id}",
     *     summary="Agrega una clase a un temario de programa",
     *     tags={"Temario"},
     *     @OA\Parameter(
     *          name="temary_id",
     *          description="Identificador de curso",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *     @OA\RequestBody(
     *         required=true,
     *         @OA\MediaType(
     *             mediaType="multipart/form-data",
     *             @OA\Schema(
     *                 @OA\Property(
     *                     property="teacher",
     *                     type="string",
     *                 ),
     *                  @OA\Property(
     *                     property="description",
     *                     type="string",
     *                 ),
     *                  @OA\Property(
     *                     property="dateIni",
     *                     type="string",
     *                 ),
     *                  @OA\Property(
     *                     property="dateEnd",
     *                     type="string",
     *                 ),
     *                 @OA\Property(
     *                     property="name",
     *                     type="string",
     *                 ),
     *                  @OA\Property(
     *                     property="state",
     *                     type="boolean",
     *                 ),
     *                 @OA\Property(
     *                     property="urlRecord",
     *                     type="file",
     *                 ),
     *             )
     *         )
     *     ),
     *
     *     @OA\Response(
     *         response=201,
     *         description="OK",
     *     ),
     *
     *     @OA\Response(
     *         response="400",
     *         description="Failed",
     *     ),
     *
     *     security={{"apiAuth": {} }},
     *
     *     deprecated=false
     * )
     */
    public function addClass_program(Request $request,$temary_id)
    {
        try {
            if (auth()->user()->rol < 5 ) {
                return $this->errorResponse('No tiene acceso a esta opciones. Consulte con los administradores del sistema', 401);
            }

            $validator = Validator::make($request->all(), [
                'name' => 'required|string',
                'teacher' => 'required|string',
                'description' => 'required|string',
                'dateIni' => 'required|string',
                'dateEnd' => 'required|string',
                'urlRecord' => 'file'
            ]);

            if($validator->fails()){

                return $this->errorResponse($validator->errors()->first(), 400);
            }

            $class = new ProgramClassModel();

            if ($image = $request->file('urlRecord')) {
                $name = "images/classes/video/" ."video_class_". $request->name . '_' . time() . "." . $image->guessExtension();
                $ruta = public_path( $name);
                copy($image, $ruta);
                $class->urlRecord = $name;
            }else{
                return $this->errorResponse('Error al grabar el video.', 400);
            }

            $class->program_temary_id = $temary_id;
            $class->name = $request->name;
            $class->teacher = $request->teacher;
            $class->description = $request->description;
            $class->dateIni = $request->dateIni;
            $class->dateEnd = $request->dateEnd;
            $class->state = $request->state;
            $class->save();

            return $this->createResponse([
                'status' => 201,
                'message' => 'Clase registrada',
                'data' => $class,
            ]);
        } catch (\Exception $exception) {
            return $this->errorResponse($exception->getMessage(), 400);
        }
    }
    /**
     * @OA\Patch(
     *     path="/admi/program/temary/class/{class_id}",
     *     summary="Actualizar una clase",
     *     tags={"Temario"},
     *     @OA\Parameter(
     *          name="class_id",
     *          description="Identificador de curso",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *     @OA\RequestBody(
     *         required=true,
     *         @OA\MediaType(
     *             mediaType="multipart/form-data",
     *             @OA\Schema(
     *                 @OA\Property(
     *                     property="name",
     *                     type="string",
     *                 ),
     *                  @OA\Property(
     *                     property="state",
     *                     type="boolean",
     *                 ),
     *                 @OA\Property(
     *                     property="urlRecord",
     *                     type="file",
     *                 ),
     *             )
     *         )
     *     ),
     *
     *     @OA\Response(
     *         response=201,
     *         description="OK",
     *     ),
     *
     *     @OA\Response(
     *         response="400",
     *         description="Failed",
     *     ),
     *
     *     security={{"apiAuth": {} }},
     *
     *     deprecated=false
     * )
     */
    public function updateClass_program(Request $request,$class_id)
    {
        try {
            if (auth()->user()->rol != 5 ) {
                return $this->errorResponse('No tiene acceso a esta opciones. Consulte con los administradores del sistema', 401);
            }

            $validator = Validator::make($request->all(), [
                'name' => 'required|string',
                'state' => 'required|boolean',
                'urlRecord' => 'required'
            ]);

            if($validator->fails()){

                return $this->errorResponse($validator->errors()->first(), 400);
            }

            $class = ProgramClassModel::find($class_id);

            if ($image = $request->file('video')) {
                $name = "video_class_". $request->name . '_' . time() . "." . $image->guessExtension();
                $ruta = public_path("images/classes/video/" . $name);
                copy($image, $ruta);
                $class->urlRecord = $ruta;
            }

            $class->name = $request->name;
            $class->state = true;
            $class->update();

            return $this->createResponse([
                'status' => 201,
                'message' => 'Clase registrada',
                'data' => $class,
            ]);
        } catch (\Exception $exception) {
            return $this->errorResponse($exception->getMessage(), 400);
        }
    }

    /**
     * @OA\Delete(
     *     path="/admi/program/temary/",
     *     summary="Elimina Temario de un programa.",
     *     tags={"Temario"},
     *
     *     @OA\RequestBody(
     *         required=true,
     *         @OA\MediaType(
     *             mediaType="multipart/form-data",
     *             @OA\Schema(
     *                 @OA\Property(
     *                     property="temary_id",
     *                     type="string",
     *                 ),
     *                 example={}
     *             )
     *         )
     *     ),
     *
     *     @OA\Response(
     *         response=200,
     *         description="OK",
     *     ),
     *
     *     @OA\Response(
     *         response="400",
     *         description="Failed",
     *     ),
     *
     *     security={{"apiAuth": {} }},
     *
     *     deprecated=false
     * )
     */
    public function Delete_program(Request $request)
    {

        try {
            $Temary = ProgramTemaryModel::find($request->temary_id);
            if ($Temary != null) {
                $Temary->delete();
            }else{
                return $this->errorResponse('No se encontro identificador.', 400);
            }


        } catch (\Exception $exception) {
            return $this->errorResponse($exception->getMessage(), 400);
        }

        return $this->successResponse([
            'status' => 200
        ]);
    }
    /**
     * @OA\Delete(
     *     path="/admi/program/temary/class",
     *     summary="Elimina una clase de un Temario.",
     *     tags={"Temario"},
     *
     *     @OA\RequestBody(
     *         required=true,
     *         @OA\MediaType(
     *             mediaType="multipart/form-data",
     *             @OA\Schema(
     *                 @OA\Property(
     *                     property="class_id",
     *                     type="string",
     *                 ),
     *                 example={}
     *             )
     *         )
     *     ),
     *
     *     @OA\Response(
     *         response=200,
     *         description="OK",
     *     ),
     *
     *     @OA\Response(
     *         response="400",
     *         description="Failed",
     *     ),
     *
     *     security={{"apiAuth": {} }},
     *
     *     deprecated=false
     * )
     */
    public function DeleteClass_program(Request $request)
    {

        try {
            $Class = ProgramClassModel::find($request->class_id);
            if ($Class != null) {
                $Class->delete();
            }else{
                return $this->errorResponse('No se encontro identificador.', 400);
            }


        } catch (\Exception $exception) {
            return $this->errorResponse($exception->getMessage(), 400);
        }

        return $this->successResponse([
            'status' => 200
        ]);
    }
}
