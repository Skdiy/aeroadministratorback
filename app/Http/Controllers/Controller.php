<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    /**
     * @OA\Info(
     *     title="API Aeronautica",
     *     version="1.0.0"
     * )
     *
     * @OA\SecurityScheme(
     *     type="http",
     *     description="login",
     *     name="token",
     *     in="header",
     *     scheme="bearer",
     *     bearerFormat="JWT",
     *     securityScheme="apiAuth"
     * )
     *
     * @OA\Server(
     *      url=L5_SWAGGER_CONST_HOST,
     *      description="Demo API Server",
     * @OA\ServerVariable(
     *      serverVariable="schema",
     *      enum={"https", "http"},
     *      default="http"
     *  )
     * )

        *
        * @OA\Tag(
        *     name="Aeronautica",
        *     description="API Endpoints of Projects"
        * )
    */
}
