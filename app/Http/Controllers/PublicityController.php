<?php

namespace App\Http\Controllers;

use App\Models\Carousel;
use App\Models\UserOpinionModel;
use App\Traits\ApiResponse;
use Illuminate\Http\Request;

class PublicityController extends Controller
{
    use ApiResponse;

    /**
     * @OA\Get(
     *     path="/publicity/carousel",
     *     summary="Devuelve las imagenes para el carousel",
     *     tags={"Publicity"},
     *
     *     @OA\Response(
     *         response=200,
     *         description="OK",
     *     ),
     *
     *     @OA\Response(
     *         response="400",
     *         description="Failed",
     *     ),
     *
     *
     *     deprecated=false
     * )
     */
    public function getCarousel()
    {
        try {
            $carousel = Carousel::all();
        } catch (\Exception $exception) {
            return $this->errorResponse($exception->getMessage(), 400);
        }

        return $this->successResponse([
            'status' => 200,
            'data' => [
                'carousel' => $carousel
            ],
        ]);
    }
    /**
     * @OA\Get(
     *     path="/publicity/opinions",
     *     summary="Devuelve las opiniones de los usuarios",
     *     tags={"Publicity"},
     *
     *     @OA\Response(
     *         response=200,
     *         description="OK",
     *     ),
     *
     *     @OA\Response(
     *         response="400",
     *         description="Failed",
     *     ),
     *
     *
     *     deprecated=false
     * )
     */
    public function getUserOpinion()
    {
        try {
            $opinions = UserOpinionModel::all();
        } catch (\Exception $exception) {
            return $this->errorResponse($exception->getMessage(), 400);
        }

        return $this->successResponse(
           $opinions);
    }

    /**
     * @OA\Post(
     *     path="/admi/publicity",
     *     summary="Grabar una nueva imagen para carrousel.",
     *     tags={"Publicity"},
     *
     *     @OA\RequestBody(
     *         required=true,
     *         @OA\MediaType(
     *             mediaType="multipart/form-data",
     *             @OA\Schema(
     *                 @OA\Property(
     *                     property="image",
     *                     type="file[]",
     *                 ),
     *
     *                 example={}
     *             )
     *         )
     *     ),
     *
     *     @OA\Response(
     *         response=201,
     *         description="OK",
     *     ),
     *
     *     @OA\Response(
     *         response="400",
     *         description="Failed",
     *     ),
     *
     *     security={{"apiAuth": {} }},
     *
     *     deprecated=false
     * )
     */
    public function store(Request $request)
    {

        if ($request->has('image')) {
            $files = $request->file('image');
            foreach($files as $n_file){
                $data = new Carousel();
                $name = '';
                $name = "carrousel_" . time() . "." . $n_file->guessExtension();
                $ruta = public_path("images/carousel/" . $name);
                copy($n_file, $ruta);
                $data->image = "images/carousel/" . $name;
                $data->order = Carousel::all()->count() + 1;
                $data->save();
            }
            return $this->createResponse([
                'status' => 201,
                'data' => 'Se guardo correctamente las imagenes.'
            ]);
        } else {
            return $this->errorResponse("No se recibieron imagenes", 400);
        }
    }

    /**
     * @OA\Post(
     *     path="/admi/publicity/opinion",
     *     summary="Guardar una opnion de un alumno.",
     *     tags={"Publicity"},
     *
     *     @OA\RequestBody(
     *         required=true,
     *         @OA\MediaType(
     *             mediaType="multipart/form-data",
     *             @OA\Schema(
     *                 @OA\Property(
     *                     property="comment",
     *                     type="string",
     *                 ),
     *                  @OA\Property(
     *                     property="name",
     *                     type="string",
     *                 ),
     *                  @OA\Property(
     *                     property="profesion",
     *                     type="string",
     *                 ),
     *                  @OA\Property(
     *                     property="photo",
     *                     type="file",
     *                 ),
     *                 example={}
     *             )
     *         )
     *     ),
     *
     *     @OA\Response(
     *         response=201,
     *         description="OK",
     *     ),
     *
     *     @OA\Response(
     *         response="400",
     *         description="Failed",
     *     ),
     *
     *     security={{"apiAuth": {} }},
     *
     *     deprecated=false
     * )
     */
    public function storeOpinion(Request $request)
    {

        try {
            $opinion = new UserOpinionModel();
            $opinion->comment = $request->comment;
            $opinion->name = $request->name;
            $opinion->profesion = $request->profesion;

            $file = $request->file('photo');
            $name = "opinion_" . time() . "." . $file->guessExtension();
            $ruta = public_path("images/opinion/" . $name);
            copy($file, $ruta);
            $opinion->photo = "images/opinion/" . $name;
            $opinion->save();

        } catch (\Exception $exception) {
            return $this->errorResponse($exception->getMessage(), 400);
        }

        return $this->successResponse([
            'status' => 200,
            'data' =>  $opinion,
        ]);
    }
     /**
     * @OA\Delete(
     *     path="/admi/publicity/opinion",
     *     summary="Elimina una opnion de un alumno.",
     *     tags={"Publicity"},
     *
     *     @OA\RequestBody(
     *         required=true,
     *         @OA\MediaType(
     *             mediaType="multipart/form-data",
     *             @OA\Schema(
     *                 @OA\Property(
     *                     property="opinion_id",
     *                     type="string",
     *                 ),
     *                 example={}
     *             )
     *         )
     *     ),
     *
     *     @OA\Response(
     *         response=201,
     *         description="OK",
     *     ),
     *
     *     @OA\Response(
     *         response="400",
     *         description="Failed",
     *     ),
     *
     *     security={{"apiAuth": {} }},
     *
     *     deprecated=false
     * )
     */
    public function DeleteOpinion(Request $request)
    {

        try {
            $opinion = UserOpinionModel::find($request->opinion_id);
            if ($opinion != null) {
                $opinion->delete();
            }else{
                return $this->errorResponse('No se encontro identificador.', 400);
            }


        } catch (\Exception $exception) {
            return $this->errorResponse($exception->getMessage(), 400);
        }

        return $this->successResponse([
            'status' => 200
        ]);
    }
    /**
     * @OA\Delete(
     *     path="/admi/publicity",
     *     summary="Elimina una imagen del carousel.",
     *     tags={"Publicity"},
     *
     *     @OA\RequestBody(
     *         required=true,
     *         @OA\MediaType(
     *             mediaType="multipart/form-data",
     *             @OA\Schema(
     *                 @OA\Property(
     *                     property="carousel_id",
     *                     type="string",
     *                 ),
     *                 example={}
     *             )
     *         )
     *     ),
     *
     *     @OA\Response(
     *         response=201,
     *         description="OK",
     *     ),
     *
     *     @OA\Response(
     *         response="400",
     *         description="Failed",
     *     ),
     *
     *     security={{"apiAuth": {} }},
     *
     *     deprecated=false
     * )
     */
    public function Delete(Request $request)
    {

        try {
            $carousel = Carousel::find($request->carousel_id);
            if ($carousel != null) {
                $carousel->delete();
            }else{
                return $this->errorResponse('No se encontro identificador.', 400);
            }


        } catch (\Exception $exception) {
            return $this->errorResponse($exception->getMessage(), 400);
        }

        return $this->successResponse([
            'status' => 200
        ]);
    }
}
