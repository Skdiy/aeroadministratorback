<?php

namespace App\Http\Controllers;

use App\Models\TeacherContractModel;
use App\Models\TeacherPaymentsModel;
use App\Traits\ApiResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ContractController extends Controller
{
    use ApiResponse;

    /**
     * @OA\Post(
     *     path="/admi/teacher/contract",
     *     summary="Crear un contrato para un profesor.",
     *     tags={"Teacher Contract"},
     *
     *     @OA\RequestBody(
     *         required=true,
     *         @OA\MediaType(
     *             mediaType="multipart/form-data",
     *             @OA\Schema(
     *                 @OA\Property(
     *                     property="teacher_id",
     *                     type="integer",
     *                 ),
     *                 @OA\Property(
     *                     property="course_code",
     *                     type="string",
     *                 ),
     *                  @OA\Property(
     *                     property="total",
     *                     type="integer",
     *                 ),
     *                  @OA\Property(
     *                     property="emision",
     *                     type="date",
     *                 ),
     *                  @OA\Property(
     *                     property="course_type",
     *                     type="integer",
     *                 ),
     *                 example={}
     *             )
     *         )
     *     ),
     *
     *     @OA\Response(
     *         response=201,
     *         description="OK",
     *     ),
     *
     *     @OA\Response(
     *         response="400",
     *         description="Failed",
     *     ),
     *
     *     security={{"apiAuth": {} }},
     *
     *     deprecated=false
     * )
     */
    public function create_contract(Request $request)
    {
        try {
            if (auth()->user()->rol != 10 ) {
                return $this->errorResponse('No tiene acceso a esta opciones. Consulte con los administradores del sistema', 401);
            }

            $validator = Validator::make($request->all(), [
                'teacher_id' => 'required',
                'course_code' => 'required',
                'total' => 'required',
                'emision' => 'required',
                'course_type' => 'required',
            ]);
            if($validator->fails()){

                return $this->errorResponse($validator->errors()->first(), 400);
            }

            $contract = new TeacherContractModel();
            $contract->teacher_id = $request->teacher_id ;
            $contract->course_code = $request->course_code ;
            $contract->total = $request->total ;
            $contract->emision = $request->emision ;
            $contract->course_type = $request->course_type ;
            $contract->save();


            return $this->createResponse([
                'status' => 201,
                'message' => 'Contrato registrado.',
                'data' => $contract,
            ]);
        } catch (\Exception $exception) {
            return $this->errorResponse($exception->getMessage(), 400);
        }
    }

    /**
     * @OA\Post(
     *     path="/admi/teacher/contract/pay",
     *     summary="Crear un pago del contrato.",
     *     tags={"Teacher Contract"},
     *
     *     @OA\RequestBody(
     *         required=true,
     *         @OA\MediaType(
     *             mediaType="multipart/form-data",
     *             @OA\Schema(
     *                 @OA\Property(
     *                     property="contract_id",
     *                     type="integer",
     *                 ),
     *                 @OA\Property(
     *                     property="name",
     *                     type="string",
     *                 ),
     *                  @OA\Property(
     *                     property="deposit",
     *                     type="integer",
     *                 ),
     *                  @OA\Property(
     *                     property="pay",
     *                     type="date",
     *                 ),
     *
     *                 example={}
     *             )
     *         )
     *     ),
     *
     *     @OA\Response(
     *         response=201,
     *         description="OK",
     *     ),
     *
     *     @OA\Response(
     *         response="400",
     *         description="Failed",
     *     ),
     *
     *     security={{"apiAuth": {} }},
     *
     *     deprecated=false
     * )
     */
    public function create_contract_pay(Request $request)
    {
        try {
            if (auth()->user()->rol != 10 ) {
                return $this->errorResponse('No tiene acceso a esta opciones. Consulte con los administradores del sistema', 401);
            }

            $validator = Validator::make($request->all(), [
                'contract_id' => 'required',
                'name' => 'required',
                'deposit' => 'required',
                'pay' => 'required',
            ]);

            if($validator->fails()){

                return $this->errorResponse($validator->errors()->first(), 400);
            }

            $contract = TeacherContractModel::findOrFail($request->contract_id);

            if ($contract == null) {
                return $this->errorResponse('Contrato no encontrado.', 400);
            }

            $payments = TeacherPaymentsModel::where('contract_id',$request->contract_id)->get();
            $totalsum = 0;
            foreach ($payments as $key ) {
                $totalsum += $key->pay;
            }

            if ($totalsum + $request->pay > $contract->total  ) {
                return $this->errorResponse('El monto maximo fue superado.', 400);
            }

            $pay = new TeacherPaymentsModel();
            $pay->contract_id = $request->contract_id ;
            $pay->name = $request->name ;
            $pay->deposit = $request->deposit ;
            $pay->pay = $request->pay ;
            $pay->state = 1 ;
            $pay->save();


            return $this->createResponse([
                'status' => 201,
                'message' => 'Pago registrado.',
                'data' => $pay,
            ]);
        } catch (\Exception $exception) {
            return $this->errorResponse($exception->getMessage(), 400);
        }
    }
    /**
     * @OA\Get(
     *     path="/teacher/contract",
     *     summary="Obetener los contratos de un profesor.",
     *     tags={"Teacher Contract"},
     *
     *     @OA\Response(
     *         response=200,
     *         description="OK",
     *     ),
     *
     *     @OA\Response(
     *         response="400",
     *         description="Failed",
     *     ),
     *
     *
     *     deprecated=false
     * )
     */
    public function get_Contract()
    {
        try {

            $contracts = TeacherContractModel::where('teacher_id',auth()->user()->id)->get();
            foreach ($contracts as $key) {
                $pays = TeacherPaymentsModel::where('contract_id',$key->id)->get();
                $contracts['payments'] = $pays;
            }

        } catch (\Exception $exception) {
            return $this->errorResponse($exception->getMessage(), 400);
        }

        return $this->successResponse([
            'status' => 200,
            'data' =>  $contracts,
        ]);
    }

    /**
     * @OA\Get(
     *     path="/teacher/contract/pay",
     *     summary="Obetener los pagos de un profesor.",
     *     tags={"Teacher Contract"},
     *
     *     @OA\Response(
     *         response=200,
     *         description="OK",
     *     ),
     *
     *     @OA\Response(
     *         response="400",
     *         description="Failed",
     *     ),
     *
     *
     *     deprecated=false
     * )
     */
    public function get_payments()
    {
        try {

            $contracts = TeacherContractModel::where('teacher_id',auth()->user()->id)->get();
            $payments = [];
            foreach ($contracts as $key) {
                $pays = TeacherPaymentsModel::where('contract_id',$key->id)->get();
                array_push($payments,$pays);
            }

        } catch (\Exception $exception) {
            return $this->errorResponse($exception->getMessage(), 400);
        }

        return $this->successResponse([
            'status' => 200,
            'data' =>  $payments,
        ]);
    }

}
