<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\UserCourse;
use App\Traits\ApiResponse;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;

class AuthController extends Controller
{
    use ApiResponse;
    /**
     * @OA\Post(
     *     path="/auth/login",
     *     summary="Autentificación de usuarios",
     *     tags={"Autentificación"},
     *
     *     @OA\RequestBody(
     *         required=true,
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 @OA\Property(
     *                     property="email",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="password",
     *                     type="string"
     *                 ),
     *
     *                 example={"email": "email@gmail.com", "password": "987654321"}
     *             )
     *         )
     *     ),
     *
     *     @OA\Response(
     *         response=200,
     *         description="OK",
     *     ),
     *
     *     @OA\Response(
     *         response="400",
     *         description="Failed",
     *     ),
     *
     *     deprecated=false
     * )
     */
    public function login(Request $request){
    	$validator = Validator::make($request->all(), [
            'email' => 'required|email',
            'password' => 'required|string|min:6',
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }

        if (!$token = auth()->attempt($validator->validated())) {
            return response()->json(['error' => 'Unauthorized'], 401);
        }

        if (!auth()->user()->status) {
            auth()->logout(true);
            return $this->errorResponse("Usuario inhabilitado.", 400);
        }
        return $this->createNewToken($token);
    }

    /**
     * @OA\Post(
     *     path="/auth/register",
     *     summary="Registro de usuario",
     *     tags={"Autentificación"},
     *
     *     @OA\RequestBody(
     *         required=true,
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 @OA\Property(
     *                     property="email",
     *                     type="string"
     *                 ),
     *                  @OA\Property(
     *                     property="name",
     *                     type="string"
     *                 ),
     *                  @OA\Property(
     *                     property="surname",
     *                     type="string"
     *                 ),
     *                  @OA\Property(
     *                     property="num_doc",
     *                     type="integer"
     *                 ),
     *                  @OA\Property(
     *                     property="phone",
     *                     type="integer"
     *                 ),
     *                  @OA\Property(
     *                     property="ocupation",
     *                     type="string"
     *                 ),
     *                  @OA\Property(
     *                     property="genre",
     *                     type="string"
     *                 ),
     *                  @OA\Property(
     *                     property="bithday",
     *                     type="date"
     *                 ),
     *                  @OA\Property(
     *                     property="country",
     *                     type="string"
     *                 ),
     *                  @OA\Property(
     *                     property="city",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="password",
     *                     type="string"
     *                 ),
     *
     *                 @OA\Property(
     *                     property="rol",
     *                     type="integer"
     *                 ),
     *                 example={"email": "email@gmail.com","name": "Pepelucho","surname": "Quispe","num_doc": 123456789,"phone": 123456789,"ocupation": "Obrero","genre": "Masculino","country": "Peru","city": "Arequipa","bithday": "2021-01-01", "password": "123456789","rol" : 1}
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *         response=201,
     *         description="OK",
     *     ),
     *
     *     @OA\Response(
     *         response="400",
     *         description="Failed",
     *     ),
     *
     *     deprecated=false
     * )
     */
    public function register(Request $request) {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|between:2,100',
            'surname' => 'required|string|between:2,100',
            'phone' => 'integer|between:100000000,1000000000',
            'num_doc' => 'integer|between:10000000,100000000000',
            'ocupation' => 'string|between:2,100',
            'genre' => 'string|between:2,100',
            'country' => 'string|between:2,100',
            'city' => 'string|between:2,100',
            'email' => 'required|string|email|max:100|unique:users',
            'password' => 'required|string|min:6',
            'rol' => 'required|integer',
        ]);

        if($validator->fails()){
            return response()->json($validator->errors()->toJson(), 400);
        }

        $user = new User();

        $user->password = bcrypt($request->password);

        if ($request->rol > 1 && auth()->user()->rol !== 10) {
            return $this->errorResponse("No tienes permiso para esta solicitud.", 400);
        }

        $user->rol = $request->rol;
        $user->name = $request->name;
        $user->email = $request->email;
        $user->surname = $request->surname;
        $user->phone = $request->phone;
        $user->num_doc = $request->num_doc;
        $user->ocupation = $request->ocupation;
        $user->country = $request->country;
        $user->city = $request->city;
        $user->genre = $request->genre;
        $user->bithday = $request->bithday;
        $user->save();


        return response()->json([
            'message' => 'User successfully registered',
            'user' => $user
        ], 201);
    }

    /**
     * @OA\Post(
     *     path="/auth/register/teacher",
     *     summary="Registro de profesor",
     *     tags={"Autentificación"},
     *
     *     @OA\RequestBody(
     *         required=true,
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 @OA\Property(
     *                     property="email",
     *                     type="string"
     *                 ),
     *                  @OA\Property(
     *                     property="name",
     *                     type="string"
     *                 ),
     *                  @OA\Property(
     *                     property="surname",
     *                     type="string"
     *                 ),
     *
     *                  @OA\Property(
     *                     property="nationality",
     *                     type="string"
     *                 ),
     *                  @OA\Property(
     *                     property="personalEmail",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="password",
     *                     type="string"
     *                 ),
     *
     *
     *                 example={"email": "email@sector-aeronautico.com","name": "Pepelucho","surname": "Quispe","nationality": "Peruano","personalEmail": "test@gmail.com","password": "123456789"}
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *         response=201,
     *         description="OK",
     *     ),
     *
     *     @OA\Response(
     *         response="400",
     *         description="Failed",
     *     ),
     *
     *     deprecated=false
     * )
     */
    public function register_teacher(Request $request) {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|between:2,100',
            'surname' => 'required|string|between:2,100',
            'nationality' => 'string',
            'personalEmail' => 'string|between:2,100',
            'email' => 'required|string|email|max:100|unique:users',
            'password' => 'required|string|min:6'
        ]);

        if($validator->fails()){
            return response()->json($validator->errors()->toJson(), 400);
        }

        $user = new User();

        $user->password = bcrypt($request->password);

        if (auth()->user()->rol !== 10) {
            return $this->errorResponse("No tienes permiso para esta solicitud.", 400);
        }

        $user->rol = User::ROL_PROFESOR;
        $user->name = $request->name;
        $user->email = $request->email;
        $user->surname = $request->surname;
        $user->nationality = $request->nationality;
        $user->personalEmail = $request->personalEmail;
        $user->save();


        return response()->json([
            'message' => 'User successfully registered',
            'user' => $user
        ], 201);
    }


    /**
     * @OA\Post(
     *     path="/auth/logout",
     *     summary="Finaliza la sessión del usuario",
     *     tags={"Autentificación"},
     *
     *     @OA\Response(
     *         response=200,
     *         description="OK",
     *     ),
     *
     *     @OA\Response(
     *         response="400",
     *         description="Failed",
     *     ),
     *
     *     security={{"apiAuth": {} }},
     *
     *     deprecated=false
     * )
     */
    public function logout() {
        $users = User::where('iduser', auth()->user()->id)->get()->first();
        auth()->logout(true);
        return response()->json(['message' => 'User successfully signed out']);
    }

    /**
     * @OA\Get(
     *     path="/auth/refresh",
     *     summary="Refrescar token de usuario.",
     *     tags={"Autentificación"},
     *
     *     @OA\Response(
     *         response=200,
     *         description="OK",
     *     ),
     *
     *     @OA\Response(
     *         response="400",
     *         description="Failed",
     *     ),
     *
     *     security={{"apiAuth": {} }},
     *
     *     deprecated=false
     * )
     */
    public function refresh() {
        return $this->createNewToken(auth()->refresh());
    }
    /**
     * @OA\Get(
     *     path="/auth/user-profile",
     *     summary="Información del usuario logeado",
     *     tags={"Autentificación"},
     *
     *     @OA\Response(
     *         response=200,
     *         description="OK",
     *     ),
     *
     *     @OA\Response(
     *         response="400",
     *         description="Failed",
     *     ),
     *
     *     security={{"apiAuth": {} }},
     *
     *     deprecated=false
     * )
     */
    public function userProfile() {

        $user = User::findOrFail(auth()->user()->id);
        $user['cantCourse'] = UserCourse::where('user_id',auth()->user()->id)->get()->count();
        return response()->json($user);
    }

    /**
     * @OA\Get(
     *     path="/auth/createNewToken",
     *     summary="Obtener nuevo token para usuario",
     *     tags={"Autentificación"},
     *
     *     @OA\Response(
     *         response=200,
     *         description="OK",
     *     ),
     *
     *     @OA\Response(
     *         response="400",
     *         description="Failed",
     *     ),
     *
     *     security={{"apiAuth": {} }},
     *
     *     deprecated=false
     * )
     */
    protected function createNewToken($token){
        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => auth()->factory()->getTTL() * 60,
            'user' => auth()->user()
        ]);
    }

    /**
     * @OA\Patch(
     *     path="/auth/update",
     *     summary="Actualizar usuario",
     *     tags={"Autentificación"},
     *
     *     @OA\RequestBody(
     *         required=true,
     *         @OA\MediaType(
     *             mediaType="multipart/form-data",
     *             @OA\Schema(
     *                  @OA\Property(
     *                     property="type",
     *                     type="integer"
     *                 ),
     *                 @OA\Property(
     *                     property="email",
     *                     type="string"
     *                 ),
     *                  @OA\Property(
     *                     property="name",
     *                     type="string"
     *                 ),
     *                  @OA\Property(
     *                     property="surname",
     *                     type="string"
     *                 ),
     *                  @OA\Property(
     *                     property="num_doc",
     *                     type="integer"
     *                 ),
     *                  @OA\Property(
     *                     property="phone",
     *                     type="integer"
     *                 ),
     *                  @OA\Property(
     *                     property="ocupation",
     *                     type="string"
     *                 ),
     *                  @OA\Property(
     *                     property="country",
     *                     type="string"
     *                 ),
     *                  @OA\Property(
     *                     property="city",
     *                     type="string"
     *                 ),
     *                  @OA\Property(
     *                     property="photo",
     *                     type="file"
     *                 ),
     *                  @OA\Property(
     *                     property="bg_image",
     *                     type="file"
     *                 ),
     *                  @OA\Property(
     *                     property="description",
     *                     type="string"
     *                 ),
     *                  @OA\Property(
     *                     property="facebook",
     *                     type="string"
     *                 ),
     *                  @OA\Property(
     *                     property="instagram",
     *                     type="string"
     *                 ),
     *
     *                 @OA\Property(
     *                     property="Actpassword",
     *                     type="string"
     *                 ),
     *
     *                 @OA\Property(
     *                     property="password",
     *                     type="string"
     *                 ),
     *
     *                 @OA\Property(
     *                     property="rol",
     *                     type="integer"
     *                 ),
     *                 example={"email": "email@gmail.com","name": "Pepelucho","surname": "Quispe","num_doc": 123456789,"phone": 123456789,"ocupation": "Obrero","genre": "Masculino","country": "Peru","city": "Arequipa","bithday": "2021-01-01", "password": "123456789","rol" : 1}
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *         response=201,
     *         description="OK",
     *     ),
     *
     *     @OA\Response(
     *         response="400",
     *         description="Failed",
     *     ),
     *
     *     deprecated=false
     * )
     */
    public function update(Request $request) {
        /* $validator = Validator::make($request->all(), [
            'name' => 'required|string|between:2,100',
            'surname' => 'required|string|between:2,100',
            'phone' => 'required|integer|between:100000000,1000000000',
            'num_doc' => 'required|integer|between:10000000,100000000000',
            'ocupation' => 'required|string|between:2,100',
            'genre' => 'required|string|between:2,100',
            'bithday' => 'required',
            'country' => 'required|string|between:2,100',
            'city' => 'required|string|between:2,100',
            'email' => 'required|string|email|max:100|unique:users',
            'password' => 'required|string|min:6',
            'rol' => 'required|integer',
        ]);

        if($validator->fails()){
            return response()->json($validator->errors()->toJson(), 400);
        } */

        $user = User::find(auth()->user()->id);

        switch ($request->type) {
            case 1:
                $user->name = $request->name;
                $user->surname = $request->surname;
                $user->email = $request->email;
                $user->ocupation = $request->ocupation;
                $user->country = $request->country;
                $user->city = $request->city;
                $user->update();
                break;
            case 2:
                if ($user->password == bcrypt($request->Actpassword)) {
                    $user->password = bcrypt($request->password);
                }
                $user->update();
                break;
            case 3:
                if ($image = $request->file('photo')) {
                    $name = "photo_users_". $request->name . '_' . time() . "." . $image->guessExtension();
                    $ruta = public_path("images/users/image/" . $name);
                    copy($image, $ruta);
                    $user->photo = $request->photo;
                }
                if ($image = $request->file('bg_image')) {
                    $name = "bg_image_users_". $request->name . '_' . time() . "." . $image->guessExtension();
                    $rutaIMG = public_path("images/users/image/" . $name);
                    copy($image, $rutaIMG);
                    $user->bg_image = $request->bg_image;
                }
                $user->update();
                break;
            case 4:
                $user->description = $request->description;
                $user->update();
                break;
            case 5:
                $user->facebook = $request->facebook;
                $user->instagram = $request->instagram;
                $user->update();
                break;
            default:
                return response()->json(['error' => 'No selecciono un tipo.'], 401);

        }


        return response()->json([
            'message' => 'User successfully updated',
            'user' => $user
        ], 201);
    }


    /**
     * @OA\Get(
     *     path="/admi/auth/status/{user_id}",
     *     summary="Cambiar estado de habilitado a deshabilitado.",
     *     tags={"Autentificación"},
     *
     *     @OA\Response(
     *         response=200,
     *         description="OK",
     *     ),
     *
     *     @OA\Response(
     *         response="400",
     *         description="Failed",
     *     ),
     *
     *     security={{"apiAuth": {} }},
     *
     *     deprecated=false
     * )
     */
    public function change_status($user_id) {

        $user = User::findOrFail($user_id);
        if ($user == null) {
            return $this->errorResponse("Usuario no encontrado.", 400);
        }
        $user->status = !$user->status;
        $user->update();

        return $this->createResponse([
            'status' => 200,
            'data' => $user,
        ]);
    }

    /**
     * @OA\Get(
     *     path="/admi/list/users",
     *     summary="Listar alumnos del sistema.",
     *     tags={"Autentificación"},
     *
     *     @OA\Response(
     *         response=200,
     *         description="OK",
     *     ),
     *
     *     @OA\Response(
     *         response="400",
     *         description="Failed",
     *     ),
     *
     *     security={{"apiAuth": {} }},
     *
     *     deprecated=false
     * )
     */
    public function list_users() {
        try{
            $user = User::where('rol',User::ROL_ALUMNO)->get();

            return $this->createResponse([
                'status' => 200,
                'data' => $user,
            ]);
        }catch (\Exception $exception) {
            return $this->errorResponse($exception->getMessage(), 400);
        }
    }

    /**
     * @OA\Get(
     *     path="/admi/list/teachers",
     *     summary="Listar alumnos del sistema.",
     *     tags={"Autentificación"},
     *
     *     @OA\Response(
     *         response=200,
     *         description="OK",
     *     ),
     *
     *     @OA\Response(
     *         response="400",
     *         description="Failed",
     *     ),
     *
     *     security={{"apiAuth": {} }},
     *
     *     deprecated=false
     * )
     */
    public function list_teachers() {
        try{
            $user = User::where('rol',User::ROL_PROFESOR)->get();

            return $this->createResponse([
                'status' => 200,
                'data' => $user,
            ]);
        }catch (\Exception $exception) {
            return $this->errorResponse($exception->getMessage(), 400);
        }
    }
    /**
     * @OA\Get(
     *     path="/admi/list/business",
     *     summary="Listar alumnos del sistema.",
     *     tags={"Autentificación"},
     *
     *     @OA\Response(
     *         response=200,
     *         description="OK",
     *     ),
     *
     *     @OA\Response(
     *         response="400",
     *         description="Failed",
     *     ),
     *
     *     security={{"apiAuth": {} }},
     *
     *     deprecated=false
     * )
     */
    public function list_business() {
        try{
            $user = User::where('rol',User::ROL_EMPRESA)->get();

            return $this->createResponse([
                'status' => 200,
                'data' => $user,
            ]);

        }catch (\Exception $exception) {
            return $this->errorResponse($exception->getMessage(), 400);
        }

    }
}
