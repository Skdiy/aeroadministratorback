<?php

namespace App\Http\Controllers;

use App\Models\ExamModel;
use App\Models\NotesModel;
use App\Models\ProyectsCourse;
use App\Models\User;
use App\Traits\ApiResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class NotesController extends Controller
{
    use ApiResponse;

    /**
     * @OA\Get(
     *     path="/admin/notes/{course_id}",
     *     summary="Obetener todas las notas de los alumnos.",
     *     tags={"Notes"},
     *
     *     @OA\Response(
     *         response=200,
     *         description="OK",
     *     ),
     *
     *     @OA\Response(
     *         response="400",
     *         description="Failed",
     *     ),
     *
     *
     *     deprecated=false
     * )
     */
    public function getAllusers($course_id)
    {
        try {
            $notes = NotesModel::where('course_id',$course_id)
            ->join('users','notes.user_id','=','users.id')
            ->select('notes.*','user.name','user.surname')
            ->get();
        } catch (\Exception $exception) {
            return $this->errorResponse($exception->getMessage(), 400);
        }

        return $this->successResponse([
            'status' => 200,
            'data' => $notes ,
        ]);
    }
    /**
     * @OA\Get(
     *     path="/notes/{course_id}",
     *     summary="Obetener todas las notas de un curso.",
     *     tags={"Notes"},
     *
     *     @OA\Response(
     *         response=200,
     *         description="OK",
     *     ),
     *
     *     @OA\Response(
     *         response="400",
     *         description="Failed",
     *     ),
     *
     *
     *     deprecated=false
     * )
     */
    public function getMynotes($course_id)
    {
        try {
            $notes = NotesModel::where('course_id',$course_id)
            ->where('users.id',auth()->user()->id)
            ->get();
        } catch (\Exception $exception) {
            return $this->errorResponse($exception->getMessage(), 400);
        }

        return $this->successResponse([
            'status' => 200,
            'data' => $notes ,
        ]);
    }
    /**
     * @OA\Get(
     *     path="/notes/user/{course_id}/{user_id}",
     *     summary="Obetener todas mis notas de un curso.",
     *     tags={"Notes"},
     *
     *     @OA\Response(
     *         response=200,
     *         description="OK",
     *     ),
     *
     *     @OA\Response(
     *         response="400",
     *         description="Failed",
     *     ),
     *
     *
     *     deprecated=false
     * )
     */
    public function getUsernotes($course_id,$user_id)
    {
        try {
            $notes = NotesModel::where('course_id',$course_id)
            ->where('users.id',$user_id)
            ->get();
        } catch (\Exception $exception) {
            return $this->errorResponse($exception->getMessage(), 400);
        }

        return $this->successResponse([
            'status' => 200,
            'data' => $notes ,
        ]);
    }
    /**
     * @OA\Post(
     *     path="/admin/notes/user/",
     *     summary="Agrega una nota a un alumno",
     *     tags={"Notes"},
     *
     *     @OA\RequestBody(
     *         required=true,
     *         @OA\MediaType(
     *             mediaType="multipart/form-data",
     *             @OA\Schema(
     *                 @OA\Property(
     *                     property="note",
     *                     type="integer",
     *                 ),
     *                 @OA\Property(
     *                     property="percent",
     *                     type="decimal"
     *                 ),
     *                 @OA\Property(
     *                     property="user_id",
     *                     type="integer"
     *                 ),
     *                 @OA\Property(
     *                     property="course_id",
     *                     type="integer"
     *                 ),
     *                  @OA\Property(
     *                     property="exam_id",
     *                     type="integer"
     *                 ),
     *                  @OA\Property(
     *                     property="proyect_id",
     *                     type="integer"
     *                 ),
     *              @OA\Property(
     *                     property="type",
     *                     type="integer"
     *                 ),
     *                 example={"note": 70, "percent": 0.5, "user_id": 1, "course_id": 1, "type":1,"proyect_id":1,"exam_id":1}
     *             )
     *         )
     *     ),
     *
     *     @OA\Response(
     *         response=201,
     *         description="OK",
     *     ),
     *
     *     @OA\Response(
     *         response="400",
     *         description="Failed",
     *     ),
     *
     *     security={{"apiAuth": {} }},
     *
     *     deprecated=false
     * )
     */
    public function addNote(Request $request)
    {
        try {
            if (auth()->user()->rol != 5 ) {
                return $this->errorResponse('No tiene acceso a esta opciones. Consulte con los administradores del sistema', 401);
            }

            $validator = Validator::make($request->all(), [
                'note' => 'required',
                'percent' => 'required',
                'user_id' => 'required',
                'course_id' => 'required',
                'type' => 'required',
            ]);

            if($validator->fails()){

                return $this->errorResponse($validator->errors()->first(), 400);
            }

            if ($request->type == NotesModel::EXAM) {
                if ($request->exam_id != null  ) {
                    $exam = ExamModel::findOrFail($request->exam_id);
                    if ($exam ==null) {
                        return $this->errorResponse('Examen no encontrado.', 400);
                    }
                }
                else{
                    return $this->errorResponse('Debe enviar el id del examen.', 400);
                }
            }elseif ($request->type == NotesModel::PROYECT) {
                if ($request->proyect_id != null  ) {
                    $proyect = ProyectsCourse::findOrFail($request->proyect_id);
                    if ($proyect ==null) {
                        return $this->errorResponse('Proyecto no encontrado.', 400);
                    }
                }
                else{
                    return $this->errorResponse('Debe enviar el id del proyecto.', 400);
                }
            }else{
                return $this->errorResponse('Tipo de nota invalido.', 400);
            }

            $teacher = User::findOrFail(auth()->user()->id);

            if ($teacher == null) {
                return $this->errorResponse('No se encontro al profesor.', 400);
            }else {
                if ($teacher->rol == User::ROL_PROFESOR) {
                    $user = User::findOrFail($request->user_id);
                    if ($user == null) {
                        $note = NotesModel::where('user_id',$request->user_id)
                        ->where('course_id',$request->course_id)
                        ->get();
                        if (empty($note) && $request->percent <= 1) {
                            $Newnote = new NotesModel();
                            $Newnote->user_id = $request->user_id;
                            $Newnote->course_id = $request->course_id;
                            $Newnote->note = $request->note;
                            $Newnote->percent = $request->percent;
                            $Newnote->type = $request->type;
                            switch ($request->type) {
                                case NotesModel::EXAM:
                                    $Newnote->exam_id = $request->exam_id;
                                    break;
                                case NotesModel::PROYECT:
                                    $Newnote->proyect_id = $request->proyect_id;
                                    break;
                            };
                            $Newnote->save();
                        }else {
                            $percent = 0;
                            foreach ($note as $key) {
                                $percent += $key->percent;
                            }
                            if ($percent + $request->percent < 1) {
                                $Newnote = new NotesModel();
                                $Newnote->user_id = $request->user_id;
                                $Newnote->course_id = $request->course_id;
                                $Newnote->note = $request->note;
                                $Newnote->percent = $request->percent;
                                $Newnote->type = $request->type;
                            switch ($request->type) {
                                case NotesModel::EXAM:
                                    $Newnote->exam_id = $request->exam_id;
                                    break;
                                case NotesModel::PROYECT:
                                    $Newnote->proyect_id = $request->proyect_id;
                                    break;
                            };
                                $Newnote->save();
                            }elseif ($percent + $request->percent == 1) {
                                $Newnote = new NotesModel();
                                $Newnote->user_id = $request->user_id;
                                $Newnote->course_id = $request->course_id;
                                $Newnote->note = $request->note;
                                $Newnote->percent = $request->percent;
                                $Newnote->type = $request->type;
                                switch ($request->type) {
                                    case NotesModel::EXAM:
                                        $Newnote->exam_id = $request->exam_id;
                                        break;
                                    case NotesModel::PROYECT:
                                        $Newnote->proyect_id = $request->proyect_id;
                                        break;
                                };
                                $Newnote->save();
                            }else{
                                return $this->errorResponse('Porcentaje no valido.', 400);
                            }
                        }
                    }else {
                        return $this->errorResponse('No se encontro al alumno.', 400);
                    }

                }else{
                    return $this->errorResponse('No tiene permisos para esta acción.', 400);
                }
            }

            return $this->createResponse([
                'status' => 201,
                'message' => 'Usuario registrado',
                'data' => $note,
            ]);
        } catch (\Exception $exception) {
            return $this->errorResponse($exception->getMessage(), 400);
        }
    }
}
