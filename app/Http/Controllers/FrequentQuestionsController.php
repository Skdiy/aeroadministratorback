<?php

namespace App\Http\Controllers;

use App\Models\Course;
use App\Models\ProgramModel;
use App\Models\QuestionsCourse;
use App\Models\QuestionsProgramModel;
use App\Traits\ApiResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class FrequentQuestionsController extends Controller
{
    use ApiResponse;
    /**
     * @OA\Get(
     *     path="/course/questions/{course_id}",
     *     summary="Obtener preguntas frecuentes",
     *     tags={"Courses"},
     *     @OA\Parameter(
     *          name="course_id",
     *          description="Identificador de curso",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *     @OA\Response(
     *         response=200,
     *         description="OK",
     *     ),
     *
     *     @OA\Response(
     *         response="400",
     *         description="Failed",
     *     ),
     *
     *
     *     deprecated=false
     * )
     */
    public function get($course_id)
    {
        try {
            $questions = QuestionsCourse::where('course_id',$course_id)->get();
            if ($questions == null) {
                return $this->errorResponse("Sin preguntas registradas.", 400);
            }
        } catch (\Exception $exception) {
            return $this->errorResponse($exception->getMessage(), 400);
        }

        return $this->successResponse([
            'status' => 200,
            'data' =>
                $questions
            ,
        ]);
    }

    /**
     * @OA\Post(
     *     path="/admi/course/questions",
     *     summary="Registrar una nueva pregunta",
     *     tags={"Courses"},
     *
     *     @OA\RequestBody(
     *         required=true,
     *         @OA\MediaType(
     *             mediaType="multipart/form-data",
     *             @OA\Schema(
     *                 @OA\Property(
     *                     property="course_id",
     *                     type="integer",
     *                 ),
     *                 @OA\Property(
     *                     property="question",
     *                     type="integer",
     *                 ),
     *                 @OA\Property(
     *                     property="answer",
     *                     type="integer",
     *                 ),
     *                 example={}
     *             )
     *         )
     *     ),
     *
     *     @OA\Response(
     *         response=201,
     *         description="OK",
     *     ),
     *
     *     @OA\Response(
     *         response="400",
     *         description="Failed",
     *     ),
     *
     *     security={{"apiAuth": {} }},
     *
     *     deprecated=false
     * )
     */
    public function store(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'course_id' => 'required|integer',
                'question' => 'required|string',
                'answer' => 'required|string'
            ]);

            if($validator->fails()){

                return $this->errorResponse($validator->errors()->first(), 400);
            }

            $course = Course::findOrFail($request->course_id);

            if ($course!=null) {
                $register = new QuestionsCourse();
                $register->course_id = $request->course_id;
                $register->question = $request->question;
                $register->answer = $request->answer;
                $register->save();
            }else{
                return $this->errorResponse("Error con los identificadores.", 400);
            }

            return $this->createResponse([
                'status' => 201,
                'message' => 'Pregunta registrada en el curso',
                'data' => $register,
            ]);
        } catch (\Exception $exception) {
            return $this->errorResponse($exception->getMessage(), 400);
        }
    }
    /**
     * @OA\Patch(
     *     path="/admi/course/questions",
     *     summary="Actualizar una pregunta",
     *     tags={"Courses"},
     *
     *     @OA\RequestBody(
     *         required=true,
     *         @OA\MediaType(
     *             mediaType="multipart/form-data",
     *             @OA\Schema(
     *                 @OA\Property(
     *                     property="question_id",
     *                     type="integer",
     *                 ),
     *                 @OA\Property(
     *                     property="question",
     *                     type="integer",
     *                 ),
     *                 @OA\Property(
     *                     property="answer",
     *                     type="integer",
     *                 ),
     *                 example={}
     *             )
     *         )
     *     ),
     *
     *     @OA\Response(
     *         response=201,
     *         description="OK",
     *     ),
     *
     *     @OA\Response(
     *         response="400",
     *         description="Failed",
     *     ),
     *
     *     security={{"apiAuth": {} }},
     *
     *     deprecated=false
     * )
     */
    public function update(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'question_id' => 'required|integer',
                'question' => 'required|string',
                'answer' => 'required|string'
            ]);

            if($validator->fails()){

                return $this->errorResponse($validator->errors()->first(), 400);
            }

            $course = QuestionsCourse::findOrFail($request->course_id);

            if ($course!=null) {
                $course->question = $request->question;
                $course->answer = $request->answer;
                $course->update();
            }else{
                return $this->errorResponse("Error con los identificadores.", 400);
            }

            return $this->createResponse([
                'status' => 201,
                'message' => 'Pregunta registrada en el curso',
                'data' => $course,
            ]);
        } catch (\Exception $exception) {
            return $this->errorResponse($exception->getMessage(), 400);
        }
    }
    /**
     * @OA\Delete(
     *     path="/admi/course/questions/{question_id}",
     *     summary="Eliminar una pregunta.",
     *     tags={"Courses"},
     *      @OA\Parameter(
     *          name="question_id",
     *          description="Identificador de pregunta",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *     @OA\Response(
     *         response=200,
     *         description="OK",
     *     ),
     *
     *     @OA\Response(
     *         response="400",
     *         description="Failed",
     *     ),
     *
     *
     *     deprecated=false
     * )
     */
    public function delete($question_id)
    {
        try {
            $question = QuestionsCourse::findOrFail($question_id);
            if ($question!=null) {
                $question->delete();
            }else{
                return $this->errorResponse('No se encontro curso.', 400);
            }
        } catch (\Exception $exception) {
            return $this->errorResponse($exception->getMessage(), 400);
        }

        return $this->successResponse([
            'status' => 200,
            'data' => $question,
        ]);
    }

     /**
     * @OA\Get(
     *     path="/program/questions/{program_id}",
     *     summary="Obtener preguntas frecuentes",
     *     tags={"Courses"},
     *     @OA\Parameter(
     *          name="program_id",
     *          description="Identificador de curso",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *     @OA\Response(
     *         response=200,
     *         description="OK",
     *     ),
     *
     *     @OA\Response(
     *         response="400",
     *         description="Failed",
     *     ),
     *
     *
     *     deprecated=false
     * )
     */
    public function get_program($program_id)
    {
        try {
            $questions = QuestionsProgramModel::where('program_id',$program_id)->get();
            if ($questions == null) {
                return $this->errorResponse("Sin preguntas registradas.", 400);
            }
        } catch (\Exception $exception) {
            return $this->errorResponse($exception->getMessage(), 400);
        }

        return $this->successResponse([
            'status' => 200,
            'data' =>
                $questions
            ,
        ]);
    }

    /**
     * @OA\Post(
     *     path="/admi/program/questions",
     *     summary="Registrar una nueva pregunta",
     *     tags={"Courses"},
     *
     *     @OA\RequestBody(
     *         required=true,
     *         @OA\MediaType(
     *             mediaType="multipart/form-data",
     *             @OA\Schema(
     *                 @OA\Property(
     *                     property="program_id",
     *                     type="integer",
     *                 ),
     *                 @OA\Property(
     *                     property="question",
     *                     type="integer",
     *                 ),
     *                 @OA\Property(
     *                     property="answer",
     *                     type="integer",
     *                 ),
     *                 example={}
     *             )
     *         )
     *     ),
     *
     *     @OA\Response(
     *         response=201,
     *         description="OK",
     *     ),
     *
     *     @OA\Response(
     *         response="400",
     *         description="Failed",
     *     ),
     *
     *     security={{"apiAuth": {} }},
     *
     *     deprecated=false
     * )
     */
    public function store_program(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'program_id' => 'required|integer',
                'question' => 'required|string',
                'answer' => 'required|string'
            ]);

            if($validator->fails()){

                return $this->errorResponse($validator->errors()->first(), 400);
            }

            $course = ProgramModel::findOrFail($request->program_id);

            if ($course!=null) {
                $register = new QuestionsProgramModel();
                $register->program_id = $request->program_id;
                $register->question = $request->question;
                $register->answer = $request->answer;
                $register->save();
            }else{
                return $this->errorResponse("Error con los identificadores.", 400);
            }

            return $this->createResponse([
                'status' => 201,
                'message' => 'Pregunta registrada en el curso',
                'data' => $register,
            ]);
        } catch (\Exception $exception) {
            return $this->errorResponse($exception->getMessage(), 400);
        }
    }
    /**
     * @OA\Patch(
     *     path="/admi/program/questions",
     *     summary="Actualizar una pregunta",
     *     tags={"Courses"},
     *
     *     @OA\RequestBody(
     *         required=true,
     *         @OA\MediaType(
     *             mediaType="multipart/form-data",
     *             @OA\Schema(
     *                 @OA\Property(
     *                     property="question_id",
     *                     type="integer",
     *                 ),
     *                 @OA\Property(
     *                     property="question",
     *                     type="integer",
     *                 ),
     *                 @OA\Property(
     *                     property="answer",
     *                     type="integer",
     *                 ),
     *                 example={}
     *             )
     *         )
     *     ),
     *
     *     @OA\Response(
     *         response=201,
     *         description="OK",
     *     ),
     *
     *     @OA\Response(
     *         response="400",
     *         description="Failed",
     *     ),
     *
     *     security={{"apiAuth": {} }},
     *
     *     deprecated=false
     * )
     */
    public function update_program(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'question_id' => 'required|integer',
                'question' => 'required|string',
                'answer' => 'required|string'
            ]);

            if($validator->fails()){

                return $this->errorResponse($validator->errors()->first(), 400);
            }

            $question = QuestionsCourse::findOrFail($request->question_id);

            if ($question!=null) {
                $question->question = $request->question;
                $question->answer = $request->answer;
                $question->update();
            }else{
                return $this->errorResponse("Error con los identificadores.", 400);
            }

            return $this->createResponse([
                'status' => 201,
                'message' => 'Pregunta registrada en el curso',
                'data' => $question,
            ]);
        } catch (\Exception $exception) {
            return $this->errorResponse($exception->getMessage(), 400);
        }
    }
    /**
     * @OA\Delete(
     *     path="/admi/program/questions/{question_id}",
     *     summary="Eliminar una pregunta.",
     *     tags={"Courses"},
     *      @OA\Parameter(
     *          name="question_id",
     *          description="Identificador de pregunta",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *     @OA\Response(
     *         response=200,
     *         description="OK",
     *     ),
     *
     *     @OA\Response(
     *         response="400",
     *         description="Failed",
     *     ),
     *
     *
     *     deprecated=false
     * )
     */
    public function delete_program($question_id)
    {
        try {
            $question = QuestionsProgramModel::findOrFail($question_id);
            if ($question!=null) {
                $question->delete();
            }else{
                return $this->errorResponse('No se encontro curso.', 400);
            }
        } catch (\Exception $exception) {
            return $this->errorResponse($exception->getMessage(), 400);
        }

        return $this->successResponse([
            'status' => 200,
            'data' => $question,
        ]);
    }
}
