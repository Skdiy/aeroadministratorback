<?php

namespace App\Http\Controllers;

use App\Models\PaymentModel;
use App\Traits\ApiResponse;
use Faker\Provider\Uuid;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
class PaymentController extends Controller
{
    use ApiResponse;

    /**
     * @OA\Get(
     *     path="/auth/payment",
     *     summary="Obtener pagos de un usuario",
     *     tags={"Payments"},
     *     @OA\Response(
     *         response=200,
     *         description="OK",
     *     ),
     *
     *     @OA\Response(
     *         response="400",
     *         description="Failed",
     *     ),
     *
     *     security={{"apiAuth": {} }},
     *     deprecated=false
     * )
     */
    public function getPayment()
    {
        try {
            $payments = PaymentModel::where('user_id',auth()->user()->id)->get();

            return $this->createResponse([
                'status' => 200,
                'data' => $payments,
            ]);
        } catch (\Exception $exception) {
            return $this->errorResponse($exception->getMessage(), 400);
        }
    }

    /**
     * @OA\Post(
     *     path="/auth/payment",
     *     summary="Agrega un pago",
     *     tags={"Payments"},
     *
     *     @OA\RequestBody(
     *         required=true,
     *         @OA\MediaType(
     *             mediaType="multipart/form-data",
     *             @OA\Schema(
     *
     *                 @OA\Property(
     *                     property="price",
     *                     type="integer"
     *                 ),
     *
     *                  @OA\Property(
     *                     property="comment",
     *                     type="string"
     *                 ),
     *                 example={}
     *             )
     *         )
     *     ),
     *
     *     @OA\Response(
     *         response=201,
     *         description="OK",
     *     ),
     *
     *     @OA\Response(
     *         response="400",
     *         description="Failed",
     *     ),
     *
     *     security={{"apiAuth": {} }},
     *
     *     deprecated=false
     * )
     */
    public function add(Request $request)
    {
        try {
            $pay = new PaymentModel();
            $pay->user_id = auth()->user()->id;
            $pay->comment = $request->comment;
            $pay->price = $request->price;
            $pay->codPay = Str::uuid();
            $pay->save();

            return $this->successResponse([
                'status' => 201,
            ]);
        } catch (\Exception $exception) {
            return $this->errorResponse($exception->getMessage(), 400);
        }
    }
}
