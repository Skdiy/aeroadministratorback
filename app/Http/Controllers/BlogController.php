<?php

namespace App\Http\Controllers;

use App\Models\BlogCategoryModel;
use App\Models\BlogModel;
use App\Traits\ApiResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class BlogController extends Controller
{
    use ApiResponse;

    /**
     * @OA\Get(
     *     path="/blog",
     *     summary="Obtener los blog por categorias",
     *     tags={"Blogs"},
     *
     *     @OA\Response(
     *         response=200,
     *         description="OK",
     *     ),
     *
     *     @OA\Response(
     *         response="400",
     *         description="Failed",
     *     ),
     *
     *
     *     deprecated=false
     * )
     */
    public function get_home()
    {
        try {

            $categories = BlogCategoryModel::select('id','name')->get();
            foreach ($categories as $key) {
                $blogs = BlogModel::where('category_id',$key->id)->select('id','title','photo','created_at')->get();
                $key['blogs'] = $blogs;
            }
            $lastblog = BlogModel::orderby('created_at', 'DESC')->get()->first();
            return $this->successResponse([
                'status' => 200,
                'data' =>
                    $categories
                ,
                'blog' => $lastblog
            ]);

        } catch (\Throwable $th) {
            //throw $th;
        }
    }
    /**
     * @OA\Get(
     *     path="/blog/{blog_id}",
     *     summary="Obtener el blog completo",
     *     tags={"Blogs"},
     *     @OA\Parameter(
     *          name="blog_id",
     *          description="Identificador de blog",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *     @OA\Response(
     *         response=200,
     *         description="OK",
     *     ),
     *
     *     @OA\Response(
     *         response="400",
     *         description="Failed",
     *     ),
     *
     *
     *     deprecated=false
     * )
     */
    public function get_blog($blog_id)
    {
        try {
            $blog = BlogModel::find($blog_id);
            if ($blog != null) {
                return $this->successResponse([
                    'status' => 200,
                    'data' =>
                        $blog
                    ,
                ]);
            }else {
                return $this->errorResponse('Blog no encontrado.', 400);
            }


        } catch (\Exception $exception) {
            return $this->errorResponse($exception->getMessage(), 400);
        }
    }
    /**
     * @OA\Post(
     *     path="/admi/blog",
     *     summary="Agrega un blog",
     *     tags={"Blogs"},
     *
     *     @OA\RequestBody(
     *         required=true,
     *         @OA\MediaType(
     *             mediaType="multipart/form-data",
     *             @OA\Schema(
     *                 @OA\Property(
     *                     property="title",
     *                     type="string",
     *                 ),
     *                 @OA\Property(
     *                     property="photo",
     *                     type="file"
     *                 ),
     *                 @OA\Property(
     *                     property="banner",
     *                     type="file"
     *                 ),
     *                 @OA\Property(
     *                     property="content",
     *                     type="text"
     *                 ),
     *                 @OA\Property(
     *                     property="category_id",
     *                     type="string"
     *                 ),
     *
     *                 example={"title": "Prueba de blog", "photo": "", "banner": "", "content": "Intermedio", "category_id": 1}
     *             )
     *         )
     *     ),
     *
     *     @OA\Response(
     *         response=201,
     *         description="OK",
     *     ),
     *
     *     @OA\Response(
     *         response="400",
     *         description="Failed",
     *     ),
     *
     *     security={{"apiAuth": {} }},
     *
     *     deprecated=false
     * )
     */
    public function create_blog(Request $request)
    {
        try {


            if (auth()->user()->rol != 10 ) {
                return $this->errorResponse('No tiene acceso a esta opciones. Consulte con los administradores del sistema', 401);
            }

            $validator = Validator::make($request->all(), [
                'title' => 'required',
                'photo' => 'required|file|mimes:jpg,jpeg,png',
                'banner' => 'required|file|mimes:jpg,jpeg,png',
                'content' => 'required',
                'category_id' => 'required',
            ]);
            if($validator->fails()){

                return $this->errorResponse($validator->errors()->first(), 400);
            }

            $category = BlogCategoryModel::findOrFail($request->category_id);

            if ($category == null) {
                return $this->errorResponse('Error no se encontro la categoria.', 400);
            }

            if ($image = $request->file('photo')) {
                $nameIMG = "images/blog/images/image_blog_". time() . "." . $image->guessExtension();
                $rutaIMG = public_path($nameIMG);
                copy($image, $rutaIMG);
            }else{
                return $this->errorResponse('Error al grabar la imagen.', 400);
            }
            if ($banner = $request->file('banner')) {
                $nameBanner = "images/blog/banner/image_blog_banner_". time() . "." . $banner->guessExtension();
                $rutaIMGbanner = public_path( $nameBanner);
                copy($banner, $rutaIMGbanner);
            }else{
                return $this->errorResponse('Error al grabar la imagen.', 400);
            }

            $blog = new BlogModel();
            $blog->title = $request->title ;
            $blog->content = $request->content ;
            $blog->category_id = $request->category_id ;
            $blog->photo = $nameIMG ;
            $blog->banner = $nameBanner ;
            $blog->save();

            return $this->createResponse([
                'status' => 201,
                'message' => 'Blog registrado',
                'data' => $blog,
            ]);
        } catch (\Exception $exception) {
            return $this->errorResponse($exception->getMessage(), 400);
        }
    }
}
