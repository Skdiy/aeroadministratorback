<?php

namespace App\Http\Controllers;

use App\Models\Certificate;
use App\Models\Files;
use App\Models\User;
use App\Traits\ApiResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class CertificateController extends Controller
{
    use ApiResponse;
    /**
     * @OA\Get(
     *     path="/certificate/{course_id}",
     *     summary="Valida tu certificado",
     *     tags={"Certificate"},
     *     @OA\Parameter(
     *          name="course_id",
     *          description="Codigo de certificado",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *     @OA\Response(
     *         response=200,
     *         description="OK",
     *     ),
     *
     *     @OA\Response(
     *         response="400",
     *         description="Failed",
     *     ),
     *
     *
     *     deprecated=false
     * )
     */
    public function validate($course_id)
    {
        try {
            $certificate = Certificate::join('files','files.id','certificates.file_id')->where('codCertificate',$course_id)->get()->first();
            if ($certificate == null) {
                return $this->errorResponse("Certificado no encontrado.", 400);
            }
        } catch (\Exception $exception) {
            return $this->errorResponse($exception->getMessage(), 400);
        }

        return $this->successResponse([
            'status' => 200,
            'data' => [
                $certificate
            ],
        ]);
    }
    /**
     * @OA\Post(
     *     path="/admi/certificate",
     *     summary="Agrega un certificado",
     *     tags={"Certificate"},
     *
     *     @OA\RequestBody(
     *         required=true,
     *         @OA\MediaType(
     *             mediaType="multipart/form-data",
     *             @OA\Schema(
     *                 @OA\Property(
     *                     property="user_id",
     *                     type="string",
     *                 ),
     *                 @OA\Property(
     *                     property="codCertificate",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="certificate",
     *                     type="file"
     *                 ),
     *
     *                 example={"user_id": 1, "codCertificate": "COD-001", "certificate": ""}
     *             )
     *         )
     *     ),
     *
     *     @OA\Response(
     *         response=201,
     *         description="OK",
     *     ),
     *
     *     @OA\Response(
     *         response="400",
     *         description="Failed",
     *     ),
     *
     *     security={{"apiAuth": {} }},
     *
     *     deprecated=false
     * )
     */
    public function create_certificate(Request $request)
    {
        try {
            if (auth()->user()->rol != 10 ) {
                return $this->errorResponse('No tiene acceso a esta opciones. Consulte con los administradores del sistema', 401);
            }
            $validator = Validator::make($request->all(), [
                'user_id' => 'required',
                'certificate' => 'required',
                'codCertificate' => 'required',

            ]);
            if($validator->fails()){

                return $this->errorResponse($validator->errors()->first(), 400);
            }
            $user = User::find($request->user_id);
            if ($user == null) {
                return $this->errorResponse('No se encontro al estudiante.', 400);
            }
            if ($image = $request->file('certificate')) {
                $name = "certificate_". $request->name . '_' . time() . "." . $image->guessExtension();
                $rutaIMG = public_path("images/certificate/" . $name);
                copy($image, $rutaIMG);
            }else{
                return $this->errorResponse('Error al grabar el certificado.', 400);
            }
            $file = new Files();
            $file->url = $rutaIMG;
            $file->save();

            $certificate = new Certificate();
            $certificate->user_id = $request->user_id;
            $certificate->codCertificate = $request->codCertificate;
            $certificate->file_id = $file->id;
            $certificate->save();

            return $this->createResponse([
                'status' => 201,
                'message' => 'Blog registrado',
                'data' => $certificate,
            ]);

        } catch (\Exception $exception) {
            return $this->errorResponse($exception->getMessage(), 400);
        }
    }
}
