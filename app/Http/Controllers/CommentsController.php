<?php

namespace App\Http\Controllers;

use App\Models\CommentClassModel;
use App\Models\CommentsLiveStreamModel;
use App\Models\LiveStreamModel;
use App\Traits\ApiResponse;
use Illuminate\Http\Request;

class CommentsController extends Controller
{
    use ApiResponse;


    /**
     * @OA\Post(
     *     path="/liveStream/comment/{liveStream_id}",
     *     summary="Registrar un comentario",
     *     tags={"LiveStream"},
     *
     *      @OA\Parameter(
     *          name="liveStream_id",
     *          description="Identificador de comentario",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *     @OA\RequestBody(
     *         required=true,
     *         @OA\MediaType(
     *             mediaType="multipart/form-data",
     *             @OA\Schema(
     *                 @OA\Property(
     *                     property="comment",
     *                     type="string",
     *                 ),
     *
     *                 example={}
     *             )
     *         )
     *     ),
     *
     *     @OA\Response(
     *         response=201,
     *         description="OK",
     *     ),
     *
     *     @OA\Response(
     *         response="400",
     *         description="Failed",
     *     ),
     *
     *     security={{"apiAuth": {} }},
     *
     *     deprecated=false
     * )
     */
    public function store(Request $request, $liveStream_id)
    {
        try {

            $auction = LiveStreamModel::findOrFail($liveStream_id);
            if ($auction == null) {
                return $this->errorResponse('LiveStream no encontrado.', 400);
            }

            $comment = new CommentsLiveStreamModel();
            $comment->user_id = auth()->user()->id;
            $comment->comment = $request->comment;
            $comment->live_stream_id = $liveStream_id;
            $comment->type = CommentsLiveStreamModel::COMMENT;
            $comment->save();

            return $this->successResponse([
                'status' => 201,
                'data' =>
                    $comment
                ,
            ]);


        } catch (\Exception $exception) {
            return $this->errorResponse($exception->getMessage(), 400);
        }
    }
}
