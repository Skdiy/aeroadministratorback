<?php

namespace App\Http\Controllers;


use App\Models\Course;
use App\Models\AssessmentsCourse;
use App\Models\ClassRealTime;
use App\Models\ClassResource;
use App\Models\ClassTemary;
use App\Models\CourseDefinitionsModel;
use App\Models\CourseOffert;
use App\Models\Files;
use App\Models\NotesModel;
use App\Models\PaymentModel;
use App\Models\User;
use App\Models\UserCourse;
use App\Traits\ApiResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class CourseController extends Controller
{
    use ApiResponse;
    /**
     * @OA\Get(
     *     path="/course",
     *     summary="Obetener todos los cursos.",
     *     tags={"Courses"},
     *
     *     @OA\Response(
     *         response=200,
     *         description="OK",
     *     ),
     *
     *     @OA\Response(
     *         response="400",
     *         description="Failed",
     *     ),
     *
     *
     *     deprecated=false
     * )
     */
    public function get()
    {
        try {
            $courses = Course::all();

            foreach ($courses as $key) {
                $key['offert'] = CourseOffert::where('course_id',$key->id)->get()->first();
            }
        } catch (\Exception $exception) {
            return $this->errorResponse($exception->getMessage(), 400);
        }

        return $this->successResponse([
            'status' => 200,
            'data' => [
                'courses' => $courses
            ],
        ]);
    }
    /**
     * @OA\Get(
     *     path="/course/class/{class_id}",
     *     summary="Obetener la clase.",
     *     tags={"Courses"},
     *     @OA\Parameter(
     *          name="class_id",
     *          description="Identificador de clase",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *     @OA\Response(
     *         response=200,
     *         description="OK",
     *     ),
     *
     *     @OA\Response(
     *         response="400",
     *         description="Failed",
     *     ),
     *
     *
     *     deprecated=false
     * )
     */
    public function getClass($class_id)
    {
        try {

            $class = ClassTemary::findOrFail($class_id);
            $class['resources'] = ClassResource::join('files','files.id','class_resources.file_id')
            ->where('class_id',$class_id)
            ->select('class_resources.*','files.url')
            ->get();

        } catch (\Exception $exception) {
            return $this->errorResponse($exception->getMessage(), 400);
        }

        return $this->successResponse([
            'status' => 200,
            'data' =>  $class,
        ]);
    }
    /**
     * @OA\Post(
     *     path="/admi/course/class",
     *     summary="Agrega un recurso al curso",
     *     tags={"Courses"},
     *
     *     @OA\RequestBody(
     *         required=true,
     *         @OA\MediaType(
     *             mediaType="multipart/form-data",
     *             @OA\Schema(
     *                 @OA\Property(
     *                     property="class_id",
     *                     type="string",
     *                 ),
     *                 @OA\Property(
     *                     property="file",
     *                     type="file"
     *                 ),
     *
     *                 example={}
     *             )
     *         )
     *     ),
     *
     *     @OA\Response(
     *         response=201,
     *         description="OK",
     *     ),
     *
     *     @OA\Response(
     *         response="400",
     *         description="Failed",
     *     ),
     *
     *     security={{"apiAuth": {} }},
     *
     *     deprecated=false
     * )
     */
    public function addResource(Request $request)
    {
        try {
            if (auth()->user()->rol < 5 ) {
                return $this->errorResponse('No tiene acceso a esta opciones. Consulte con los administradores del sistema', 401);
            }

            $validator = Validator::make($request->all(), [
                'class_id' => 'required',
                'file' => 'required',
            ]);

            if($validator->fails()){

                return $this->errorResponse($validator->errors()->first(), 400);
            }


            if ($file = $request->file('file')) {
                $nameFile = "images/courses/resources/" . "video_course_". time() . "." . $file->guessExtension();
                $ruta = public_path($nameFile);
                copy($file, $ruta);
                $fileM = new Files();
                $fileM->url = $nameFile;
                $fileM->save();
            }else{
                return $this->errorResponse('Error al guardar archivo.', 400);
            }

            $resource = new ClassResource();
            $resource->class_id = $request->class_id;
            $resource->file_id = $fileM->id;
            $resource->save();

            return $this->createResponse([
                'status' => 201,
                'message' => 'Recurso registrado',
                'data' => $resource,
            ]);
        } catch (\Exception $exception) {
            return $this->errorResponse($exception->getMessage(), 400);
        }
    }
    /**
     * @OA\Get(
     *     path="/course/home",
     *     summary="Obetener los cursos para el home.",
     *     tags={"Courses"},
     *
     *     @OA\Response(
     *         response=200,
     *         description="OK",
     *     ),
     *
     *     @OA\Response(
     *         response="400",
     *         description="Failed",
     *     ),
     *
     *
     *     deprecated=false
     * )
     */
    public function getHome()
    {
        try {
            $courses = Course::join('users','users.id','courses.teacher_id')
            ->select('courses.id','courses.type','courses.date_ini','courses.spots','courses.name','courses.level','courses.timeClass','courses.image','courses.abstract','users.name AS teacher','users.surname AS teacherSurname')
            ->inRandomOrder()->limit(9)->get();
        } catch (\Exception $exception) {
            return $this->errorResponse($exception->getMessage(), 400);
        }

        return $this->successResponse($courses

        );
    }
    /**
     * @OA\Get(
     *     path="/course/{course_id}",
     *     summary="Obetener todos los datos de un curso.",
     *     tags={"Courses"},
     *      @OA\Parameter(
     *          name="course_id",
     *          description="Identificador de curso",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *     @OA\Response(
     *         response=200,
     *         description="OK",
     *     ),
     *
     *     @OA\Response(
     *         response="400",
     *         description="Failed",
     *     ),
     *
     *
     *     deprecated=false
     * )
     */
    public function getCourse($course_id)
    {
        try {
            $course = Course::findOrFail($course_id);
            $definitions = CourseDefinitionsModel::where('course_id',$course_id)->get();
            $teacher = User::where('id',$course->teacher_id)->select('id','name','surname','ocupation')->get()->first();
            $teacher['num_course'] = Course::where('teacher_id',$teacher->id)->get()->count();
            $course['teacher'] = $teacher;
            $course['offert'] = CourseOffert::where('course_id',$course_id)->get()->first();
            $course['definitions'] = $definitions;
            if ($course->type == Course::REALTIME) {
                $course['realtime'] = ClassRealTime::where('course_id',$course->id)->get()->first();
            }
        } catch (\Exception $exception) {
            return $this->errorResponse($exception->getMessage(), 400);
        }

        return $this->successResponse($course);
    }

    /**
     * @OA\Get(
     *     path="/auth/course",
     *     summary="Obetener mis cursos.",
     *     tags={"Courses"},
     *     @OA\Response(
     *         response=200,
     *         description="OK",
     *     ),
     *
     *     @OA\Response(
     *         response="400",
     *         description="Failed",
     *     ),
     *
     *     security={{"apiAuth": {} }},
     *     deprecated=false
     * )
     */
    public function getUserCourse()
    {
        try {
            $course = Course::join('user_courses','user_courses.course_id','=','courses.id')
            ->where('user_courses.user_id',auth()->user()->id)
            ->select('courses.*')
            ->get();

        } catch (\Exception $exception) {
            return $this->errorResponse($exception->getMessage(), 400);
        }

        return $this->successResponse($course);
    }

    /**
     * @OA\Get(
     *     path="/auth/course/progress/{id_course}",
     *     summary="Obetener mi progreso.",
     *     tags={"Courses"},
     *     @OA\Response(
     *         response=200,
     *         description="OK",
     *     ),
     *
     *     @OA\Response(
     *         response="400",
     *         description="Failed",
     *     ),
     *
     *     security={{"apiAuth": {} }},
     *     deprecated=false
     * )
     */
    public function getMyProgress()
    {
        try {
            $course = Course::join('user_courses','user_courses.course_id','=','courses.id')
            ->where('user_courses.user_id',auth()->user()->id)
            ->select('courses.*')
            ->get();

        } catch (\Exception $exception) {
            return $this->errorResponse($exception->getMessage(), 400);
        }

        return $this->successResponse($course);
    }

    /**
     * @OA\Post(
     *     path="/admi/course",
     *     summary="Agrega un curso",
     *     tags={"Courses"},
     *
     *     @OA\RequestBody(
     *         required=true,
     *         @OA\MediaType(
     *             mediaType="multipart/form-data",
     *             @OA\Schema(
     *                 @OA\Property(
     *                     property="name",
     *                     type="string",
     *                 ),
     *                 @OA\Property(
     *                     property="timeClass",
     *                     type="Time"
     *                 ),
     *                  @OA\Property(
     *                     property="date_ini",
     *                     type="date"
     *                 ),
     *                 @OA\Property(
     *                     property="spots",
     *                     type="integer"
     *                 ),
     *                 @OA\Property(
     *                     property="level",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="description",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="abstract",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="price",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="knowledge",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="language",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="video",
     *                     type="file"
     *                 ),
     *                  @OA\Property(
     *                     property="image",
     *                     type="file"
     *                 ),
     *                 @OA\Property(
     *                     property="type",
     *                     type="string"
     *                 ),
     *                  @OA\Property(
     *                     property="minAVG",
     *                     type="integer"
     *                 ),
     *                  @OA\Property(
     *                     property="teacher_id",
     *                     type="integer"
     *                 ),
     *                 example={"name": "name", "timeClass": "01:20:00", "spots": 20, "level": "Intermedio", "description": "Un curso de ...", "abstract": "un curso", "knowledge": "Conocimiento o experiencia previa en ventas.", "language": "Español", "video": "", "image": "", "type": "1", "teacher_id": 1,"minAVG":80}
     *             )
     *         )
     *     ),
     *
     *     @OA\Response(
     *         response=201,
     *         description="OK",
     *     ),
     *
     *     @OA\Response(
     *         response="400",
     *         description="Failed",
     *     ),
     *
     *     security={{"apiAuth": {} }},
     *
     *     deprecated=false
     * )
     */
    public function add(Request $request)
    {
        try {
            if (auth()->user()->rol < 5 ) {
                return $this->errorResponse('No tiene acceso a esta opciones. Consulte con los administradores del sistema', 401);
            }

            $validator = Validator::make($request->all(), [
                'name' => 'required',
                'timeClass' => 'required',
                'level' => 'required',
                'description' => 'required',
                'abstract' => 'required',
                'price' => 'required',
                'knowledge' => 'required',
                'language' => 'required',
                'type' => 'required',
                'teacher_id' => 'required',
                'minAVG' => 'required',
                'video' => 'required|file|mimes:mp4',
                'image' => 'required|file|mimes:jpg,jpeg,png',
            ]);

            if($validator->fails()){

                return $this->errorResponse($validator->errors()->first(), 400);
            }

            $teacher = User::findOrFail($request->teacher_id);

            if ($teacher == null) {
                return $this->errorResponse('No se encontro al profesor.', 400);
            }

            if ($image = $request->file('video')) {
                $nameVideo = "images/courses/video/" . "video_course_". time() . "." . $image->guessExtension();
                $ruta = public_path($nameVideo);
                copy($image, $ruta);
            }else{
                return $this->errorResponse('Error al grabar el video.', 400);
            }
            if ($image = $request->file('image')) {
                $nameIMG = "images/courses/image/" ."image_course_". time() . "." . $image->guessExtension();
                $rutaIMG = public_path( $nameIMG);
                copy($image, $rutaIMG);
            }else{
                return $this->errorResponse('Error al grabar la imagen.', 400);
            }

            $course = new Course();
            $course->name = $request->name;
            $course->timeClass = $request->timeClass;
            $course->date_ini = $request->date_ini;
            $course->spots = $request->spots;
            $course->level = $request->level;
            $course->description = $request->description;
            $course->abstract = $request->abstract;
            $course->price = $request->price;
            $course->knowledge = $request->knowledge;
            $course->language = $request->language;
            $course->type = $request->type;
            $course->video = $nameVideo;
            $course->image = $nameIMG;
            $course->minAVG = $request->minAVG;
            $course->teacher_id = $request->teacher_id;
            $course->save();

            return $this->createResponse([
                'status' => 201,
                'message' => 'Usuario registrado',
                'data' => $course,
            ]);
        } catch (\Exception $exception) {
            return $this->errorResponse($exception->getMessage(), 400);
        }
    }
    /**
     * @OA\Post(
     *     path="/admi/course/definitions",
     *     summary="Agrega definiciones del curso",
     *     tags={"Courses"},
     *
     *     @OA\RequestBody(
     *         required=true,
     *         @OA\MediaType(
     *             mediaType="multipart/form-data",
     *             @OA\Schema(
     *              @OA\Property(
     *                     property="course_id",
     *                     type="integer",
     *                 ),
     *                 @OA\Property(
     *                     property="title1",
     *                     type="string",
     *                 ),
     *                 @OA\Property(
     *                     property="definition1",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="title2",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="definition2",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="title3",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="definition3",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="title4",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="definition4",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="title5",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="definition5",
     *                     type="string"
     *                 ),
     *                  @OA\Property(
     *                     property="title6",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="definition6",
     *                     type="string"
     *                 ),
     *                  @OA\Property(
     *                     property="title7",
     *                     type="string"
     *                 ),
     *                  @OA\Property(
     *                     property="definition7",
     *                     type="string"
     *                 ),
     *                 example={}
     *             )
     *         )
     *     ),
     *
     *     @OA\Response(
     *         response=201,
     *         description="OK",
     *     ),
     *
     *     @OA\Response(
     *         response="400",
     *         description="Failed",
     *     ),
     *
     *     security={{"apiAuth": {} }},
     *
     *     deprecated=false
     * )
     */
    public function addDefinition(Request $request)
    {
        try {
            if (auth()->user()->rol < 5 ) {
                return $this->errorResponse('No tiene acceso a esta opciones. Consulte con los administradores del sistema', 401);
            }

            $validator = Validator::make($request->all(), [
                'course_id' => 'required|integer',
                'title1' => 'required|string',
                'definition1' => 'required|string',
                'title2' => 'required|string',
                'definition2' => 'required|string',
                'title3' => 'required|string',
                'definition3' => 'required|string',
                'title4' => 'required|string',
                'definition4' => 'required|string',
                'title5' => 'required|string',
                'definition5' => 'required|string',
                'title6' => 'required|string',
                'definition6' => 'required|string',
                'title7' => 'required|string',
                'definition7' => 'required|string',
            ]);

            if($validator->fails()){

                return $this->errorResponse($validator->errors()->first(), 400);
            }

            $course = Course::findOrFail($request->course_id);

            if ($course!=null) {
                $definition1 = new CourseDefinitionsModel(['course_id'=> $request->course_id, 'title' => $request->title1,'definition' => $request->definition1]);
                $definition1->save();
                $definition2 = new CourseDefinitionsModel(['course_id'=> $request->course_id, 'title' => $request->title2,'definition' => $request->definition2]);
                $definition2->save();
                $definition3 = new CourseDefinitionsModel(['course_id'=> $request->course_id, 'title' => $request->title3,'definition' => $request->definition3]);
                $definition3->save();
                $definition4 = new CourseDefinitionsModel(['course_id'=> $request->course_id, 'title' => $request->title4,'definition' => $request->definition4]);
                $definition4->save();
                $definition5 = new CourseDefinitionsModel(['course_id'=> $request->course_id, 'title' => $request->title5,'definition' => $request->definition5]);
                $definition5->save();
                $definition6 = new CourseDefinitionsModel(['course_id'=> $request->course_id, 'title' => $request->title6,'definition' => $request->definition6]);
                $definition6->save();
                $definition7 = new CourseDefinitionsModel(['course_id'=> $request->course_id, 'title' => $request->title7,'definition' => $request->definition7]);
                $definition7->save();
            }




            return $this->createResponse([
                'status' => 201,
                'message' => 'Usuario registrado',
                'data' => $course,
            ]);
        } catch (\Exception $exception) {
            return $this->errorResponse($exception->getMessage(), 400);
        }
    }
    /**
     * @OA\Patch(
     *     path="/admi/course",
     *     summary="Actualizar un curso",
     *     tags={"Courses"},
     *
     *     @OA\RequestBody(
     *         required=true,
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                  @OA\Property(
     *                     property="course_id",
     *                     type="integer"
     *                 ),
     *                 @OA\Property(
     *                     property="name",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="timeClass",
     *                     type="Time"
     *                 ),
     *                 @OA\Property(
     *                     property="spots",
     *                     type="integer"
     *                 ),
     *                 @OA\Property(
     *                     property="level",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="description",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="abstract",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="price",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="knowledge",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="language",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="video",
     *                     type="file"
     *                 ),
     *                  @OA\Property(
     *                     property="image",
     *                     type="file"
     *                 ),
     *                 @OA\Property(
     *                     property="type",
     *                     type="string"
     *                 ),
     *                  @OA\Property(
     *                     property="teacher_id",
     *                     type="integer"
     *                 ),
     *                 example={"course_id": 1,"name": "name", "timeClass": "01:20:00", "spots": 20, "level": "Intermedio", "description": "Un curso de ...", "abstract": "un curso", "knowledge": "Conocimiento o experiencia previa en ventas.", "language": "Español", "video": "", "image": "", "type": "1", "teacher_id": 1}
     *             )
     *         )
     *     ),
     *
     *     @OA\Response(
     *         response=201,
     *         description="OK",
     *     ),
     *
     *     @OA\Response(
     *         response="400",
     *         description="Failed",
     *     ),
     *
     *     security={{"apiAuth": {} }},
     *
     *     deprecated=false
     * )
     */
    public function update(Request $request)
    {
        try {
            if (auth()->user()->rol != 5 || auth()->user()->rol != 10) {
                return $this->errorResponse('No tiene acceso a esta opciones. Consulte con los administradores del sistema', 401);
            }

            $validator = Validator::make($request->all(), [
                'course_id' => 'required',
                'name' => 'required',
                'timeClass' => 'required',
                'spots' => 'required',
                'level' => 'required',
                'description' => 'required',
                'abstract' => 'required',
                'price' => 'required',
                'knowledge' => 'required',
                'language' => 'required',
                'type' => 'required',
                'teacher_id' => 'required',
                'video' => 'required|file|mimes:mp4',
                'image' => 'required|file|mimes:jpg,jpeg,png',
            ]);

            if($validator->fails()){

                return $this->errorResponse($validator->errors()->first(), 400);
            }


            $course = Course::findOrFail($request->course_id);
            if ($course==null) {
                return $this->errorResponse('No se encontro el curso', 401);
            }


            $teacher = User::findOrFail($request->teacher_id);

            if ($teacher == null) {
                return $this->errorResponse('No se encontro al profesor.', 401);
            }

            if ($image = $request->file('video')) {
                $name = "video_course_". $request->name . '_' . time() . "." . $image->guessExtension();
                $ruta = public_path("images/courses/video/" . $name);
                copy($image, $ruta);
                $course->video = $ruta;
            }else{
                return $this->errorResponse('Error al grabar el video.', 400);
            }
            if ($image = $request->file('video')) {
                $name = "video_course_". $request->name . '_' . time() . "." . $image->guessExtension();
                $rutaIMG = public_path("images/courses/image/" . $name);
                copy($image, $ruta);
                $course->image = $rutaIMG;
            }else{
                return $this->errorResponse('Error al grabar el video.', 400);
            }

            $course->name = $request->name;
            $course->timeClass = $request->timeClass;
            $course->spots = $request->spots;
            $course->level = $request->level;
            $course->description = $request->description;
            $course->abstract = $request->abstract;
            $course->price = $request->price;
            $course->knowledge = $request->knowledge;
            $course->language = $request->language;
            $course->type = $request->type;
            $course->teacher_id = $request->teacher_id;
            $course->update();

            return $this->createResponse([
                'status' => 201,
                'message' => 'Usuario registrado',
                'data' => $course,
            ]);
        } catch (\Exception $exception) {
            return $this->errorResponse($exception->getMessage(), 400);
        }
    }
    /**
     * @OA\Delete(
     *     path="/admi/course/{course_id}",
     *     summary="Eliminar un curso.",
     *     tags={"Courses"},
     *      @OA\Parameter(
     *          name="course_id",
     *          description="Identificador de curso",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *     @OA\Response(
     *         response=200,
     *         description="OK",
     *     ),
     *
     *     @OA\Response(
     *         response="400",
     *         description="Failed",
     *     ),
     *
     *
     *     deprecated=false
     * )
     */
    public function delete($course_id)
    {
        try {
            $course = Course::findOrFail($course_id);
            if ($course!=null) {
                $course->delete();
            }else{
                return $this->errorResponse('No se encontro curso.', 400);
            }
        } catch (\Exception $exception) {
            return $this->errorResponse($exception->getMessage(), 400);
        }

        return $this->successResponse([
            'status' => 200,
            'data' => $course,
        ]);
    }

    /**
     * @OA\Post(
     *     path="/course/registerUser",
     *     summary="Registrar un usuario en un curso",
     *     tags={"Courses"},
     *
     *     @OA\RequestBody(
     *         required=true,
     *         @OA\MediaType(
     *             mediaType="multipart/form-data",
     *             @OA\Schema(
     *                 @OA\Property(
     *                     property="user_id",
     *                     type="integer",
     *                 ),
     *                 @OA\Property(
     *                     property="course_id",
     *                     type="integer",
     *                 ),
     *                 @OA\Property(
     *                     property="codPay",
     *                     type="integer",
     *                 ),
     *                 example={}
     *             )
     *         )
     *     ),
     *
     *     @OA\Response(
     *         response=201,
     *         description="OK",
     *     ),
     *
     *     @OA\Response(
     *         response="400",
     *         description="Failed",
     *     ),
     *
     *     security={{"apiAuth": {} }},
     *
     *     deprecated=false
     * )
     */
    public function RegisterUser(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'user_id' => 'required|integer',
                'course_id' => 'required|string',
                'codPay' => 'required|string'
            ]);

            if($validator->fails()){

                return $this->errorResponse($validator->errors()->first(), 400);
            }

            $course = Course::findOrFail($request->course_id);
            $user = User::findOrFail($request->user_id);
            if ($course!=null && $user!=null) {

                $pay = new PaymentModel();
                $pay->user_id =  $request->user_id;
                $pay->codPay =  $request->codPay;
                $pay->price =  50;
                $pay->comment =  '';
                $pay->save();

                $register = new UserCourse();
                $register->user_id = $request->user_id;
                $register->course_id = $request->course_id;
                $register->payment_id = $pay->id;
                $register->save();
            }else{
                return $this->errorResponse("Error con los identificadores.", 400);
            }

            return $this->createResponse([
                'status' => 201,
                'message' => 'Usuario registrado en el curso',
                'data' => $course,
            ]);
        } catch (\Exception $exception) {
            return $this->errorResponse($exception->getMessage(), 400);
        }
    }
    /**
     * @OA\Post(
     *     path="/course/AssesmentCourse",
     *     summary="Registrar una valoracion de un usuario para un curso",
     *     tags={"Courses"},
     *
     *     @OA\RequestBody(
     *         required=true,
     *         @OA\MediaType(
     *             mediaType="multipart/form-data",
     *             @OA\Schema(
     *                 @OA\Property(
     *                     property="user_id",
     *                     type="integer",
     *                 ),
     *                 @OA\Property(
     *                     property="course_id",
     *                     type="integer",
     *                 ),
     *                 @OA\Property(
     *                     property="assessment",
     *                     type="integer",
     *                 ),
     *                  @OA\Property(
     *                     property="comment",
     *                     type="string",
     *                 ),
     *                 example={}
     *             )
     *         )
     *     ),
     *
     *     @OA\Response(
     *         response=201,
     *         description="OK",
     *     ),
     *
     *     @OA\Response(
     *         response="400",
     *         description="Failed",
     *     ),
     *
     *     security={{"apiAuth": {} }},
     *
     *     deprecated=false
     * )
     */
    public function AssesmentCourse(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'user_id' => 'required|integer',
                'course_id' => 'required|integer',
                'assessment' => 'required',
                'comment' => 'string'
            ]);

            if($validator->fails()){

                return $this->errorResponse($validator->errors()->first(), 400);
            }

            $course = Course::findOrFail($request->course_id);


            $user = User::findOrFail($request->user_id);

            if ($course!=null && $user!=null) {
                $register = new AssessmentsCourse();
                $register->user_id = $user->id;
                $register->course_id = $course->id;
                $register->assessment = $request->assessment;
                $register->comment = $request->comment;
                $register->save();

                return $this->createResponse([
                    'status' => 201,
                    'message' => 'La valoración se registro.',
                ]);

            }else{
                return $this->errorResponse("Error con los identificadores.", 400);
            }


        } catch (\Exception $exception) {
            return $this->errorResponse($exception->getMessage(), 400);
        }
    }

    /**
     * @OA\Get(
     *     path="/course/AssesmentCourse/{course_id}",
     *     summary="Obetener valoraciones de un curso.",
     *     tags={"Courses"},
     *     @OA\Parameter(
     *          name="course_id",
     *          description="Identificador de curso",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *     @OA\Response(
     *         response=200,
     *         description="OK",
     *     ),
     *
     *     @OA\Response(
     *         response="400",
     *         description="Failed",
     *     ),
     *
     *
     *     deprecated=false
     * )
     */
    public function GetAssesmentCourse($course_id)
    {
        try {

            $avg = 0;

            $result = DB::select(
                'Select assessments_courses.*, users.name, users.surname, users.photo from assessments_courses inner join users on users.id = assessments_courses.user_id Where course_id = '.$course_id
            );

            /* $result = AssessmentsCourse::where('course_id','=',$course_id)->get(); */

            foreach ($result as $key) {

                $avg = $avg + floatval($key->assessment);
            }


            if (count($result)>0) {
                $avg = $avg/count($result);
            }

            return $this->createResponse([
                'status' => 200,
                'data' => $result,
                'AVGassesstament' =>$avg
            ]);

        } catch (\Exception $exception) {
            return $this->errorResponse($exception->getMessage(), 400);
        }
    }
    /**
     * @OA\Delete(
     *     path="/course/AssesmentCourse/{assessment_id}",
     *     summary="Eliminar una valoracióm.",
     *     tags={"Courses"},
     *      @OA\Parameter(
     *          name="assessment_id",
     *          description="Identificador de assessment",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *     @OA\Response(
     *         response=200,
     *         description="OK",
     *     ),
     *
     *     @OA\Response(
     *         response="400",
     *         description="Failed",
     *     ),
     *
     *     security={{"apiAuth": {} }},
     *     deprecated=false
     * )
     */
    public function DeleteAssesmentCourse($assessment_id)
    {
        try {

            $assesstament = AssessmentsCourse::findOrFail($assessment_id);
            if ($assesstament != null) {
                if ($assesstament->user_id == auth()->user()->id || auth()->user()->rol == User::ROL_ADMIN) {
                    $assesstament->delete();
                }else {
                    return $this->errorResponse("No tiene permisos para realizar esta acción.", 400);
                }
            }else {
                return $this->errorResponse("No se encontro valoración.", 400);
            }
            return $this->createResponse([
                'status' => 200,
                'data' => $assesstament
            ]);
        } catch (\Exception $exception) {
            return $this->errorResponse($exception->getMessage(), 400);
        }
    }

    /**
     * @OA\Get(
     *     path="/course/search/{search}",
     *     summary="Get all courses with this word.",
     *     tags={"Courses"},
     *     @OA\Parameter(
     *          name="search",
     *          description="Palabra a buscar",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *     @OA\Response(
     *         response=200,
     *         description="OK",
     *     ),
     *
     *     @OA\Response(
     *         response="400",
     *         description="Failed",
     *     ),
     *
     *
     *     deprecated=false
     * )
     */
    public function Search($search)
    {
        try {

            $courses = Course::select('name','abstract','level','type')
            ->where('name','LIKE','%'.$search.'%')
            ->orwhere('abstract','LIKE','%'.$search.'%')
            ->orwhere('description','LIKE','%'.$search.'%')
            ->limit(5)
            ->get();

            foreach ($courses as $key) {
                $key['offert'] = CourseOffert::where('course_id',$key->id)->get()->first();
            }

            return $this->createResponse([
                'status' => 200,
                'data' => $courses
            ]);
        } catch (\Exception $exception) {
            return $this->errorResponse($exception->getMessage(), 400);
        }
    }

    /**
     * @OA\Get(
     *     path="/course/searchAll/{search}",
     *     summary="Get all courses with this word.",
     *     tags={"Courses"},
     *     @OA\Parameter(
     *          name="search",
     *          description="Palabra a buscar",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *     @OA\Response(
     *         response=200,
     *         description="OK",
     *     ),
     *
     *     @OA\Response(
     *         response="400",
     *         description="Failed",
     *     ),
     *
     *
     *     deprecated=false
     * )
     */
    public function SearchAll($search)
    {
        try {

            $courses = Course::select('name','abstract','level','type')
            ->where('name','LIKE','%'.$search.'%')
            ->orwhere('abstract','LIKE','%'.$search.'%')
            ->orwhere('description','LIKE','%'.$search.'%')
            ->orderby('id','desc')
            ->paginate(10);

            foreach ($courses as $key) {
                $key['offert'] = CourseOffert::where('course_id',$key->id)->get()->first();
            }

            return $this->createResponse([
                'status' => 200,
                'data' => $courses
            ]);
        } catch (\Exception $exception) {
            return $this->errorResponse($exception->getMessage(), 400);
        }
    }
}


