<?php

namespace App\Http\Controllers;

use App\Models\ProgramModel;
use App\Models\ProgramTemaryModel;
use App\Models\TemaryCourse;
use App\Models\User;
use App\Models\UserProgramAssistanceModel;
use App\Models\UserProgramModel;
use App\Traits\ApiResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class ProgramController extends Controller
{
    use ApiResponse;

    /**
     * @OA\Get(
     *     path="/program",
     *     summary="Obetener todos los programas.",
     *     tags={"Programs"},
     *
     *     @OA\Response(
     *         response=200,
     *         description="OK",
     *     ),
     *
     *     @OA\Response(
     *         response="400",
     *         description="Failed",
     *     ),
     *
     *
     *     deprecated=false
     * )
     */
    public function get()
    {
        try {
            $programs = DB::select('select * from program ', []);
        } catch (\Exception $exception) {
            return $this->errorResponse($exception->getMessage(), 400);
        }

        return $this->successResponse([
            'status' => 200,
            'data' => $programs
            ,
        ]);
    }
    /**
     * @OA\Get(
     *     path="/program/home",
     *     summary="Obetener los programas para el home.",
     *     tags={"Programs"},
     *
     *     @OA\Response(
     *         response=200,
     *         description="OK",
     *     ),
     *
     *     @OA\Response(
     *         response="400",
     *         description="Failed",
     *     ),
     *
     *
     *     deprecated=false
     * )
     */
    public function getHome()
    {
        try {
            $programs = ProgramModel::join('users','users.id','program.teacher_id')
            ->select('program.id','program.title','program.abstract','program.price','users.name AS teacher','users.surname AS teacherSurname')
            ->inRandomOrder()->limit(9)->get();
        } catch (\Exception $exception) {
            return $this->errorResponse($exception->getMessage(), 400);
        }

        return $this->successResponse($programs);
    }
    /**
     * @OA\Get(
     *     path="/program/{program_id}",
     *     summary="Obetener todos los datos de un programa.",
     *     tags={"Programs"},
     *      @OA\Parameter(
     *          name="program_id",
     *          description="Identificador de programa",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *     @OA\Response(
     *         response=200,
     *         description="OK",
     *     ),
     *
     *     @OA\Response(
     *         response="400",
     *         description="Failed",
     *     ),
     *
     *
     *     deprecated=false
     * )
     */
    public function getProgram($program_id)
    {
        try {
            $program = ProgramModel::join('users','users.id','program.teacher_id')
            ->select('program.*', 'users.name AS teacher', 'users.surname AS teacherSurname')
            ->where('program.id',$program_id)
            ->get()->first();


        } catch (\Exception $exception) {
            return $this->errorResponse($exception->getMessage(), 400);
        }

        return $this->successResponse([
            'status' => 200,
            'data' => $program,
        ]);
    }

    /**
     * @OA\Get(
     *     path="/program/my/{program_id}",
     *     summary="Obetener programa de usuario registrado.",
     *     tags={"Programs"},
     *      @OA\Parameter(
     *          name="program_id",
     *          description="Identificador de programa",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *     @OA\Response(
     *         response=200,
     *         description="OK",
     *     ),
     *
     *     @OA\Response(
     *         response="400",
     *         description="Failed",
     *     ),
     *
     *     security={{"apiAuth": {} }},
     *     deprecated=false
     * )
     */
    public function getMyProgram($program_id)
    {
        try {

            $user = UserProgramModel::where('program_id',$program_id)
            ->where('user_id',auth()->user()->id)
            ->get()
            ->first();

            if ($user == null) {
                return $this->errorResponse('Usuario no registrado', 400);
            }

            $program = ProgramModel::join('users','users.id','program.teacher_id')
            ->select('program.*', 'users.name AS teacher', 'users.surname AS teacherSurname')
            ->where('program.id',$program_id)
            ->get()->first();


            $InactiveCourses = ProgramTemaryModel::where('program_id',$program_id)->where('state',0)->get()->count();
            $ActiveCourses = ProgramTemaryModel::where('program_id',$program_id)->where('state',1)->get()->count();

            $program['InactiveCourses'] = $InactiveCourses;
            $program['ActiveCourses'] = $ActiveCourses;

            $program['AccumulateHours'] = UserProgramAssistanceModel::where('user_program_id',$user->id)
            ->select('hours')
            ->sum('hours');

            return $this->successResponse([
                'status' => 200,
                'data' => $program,
            ]);

        } catch (\Exception $exception) {
            return $this->errorResponse($exception->getMessage(), 400);
        }
    }

    /**
     * @OA\Get(
     *     path="/auth/program",
     *     summary="Obetener mis programas.",
     *     tags={"Programs"},
     *     @OA\Response(
     *         response=200,
     *         description="OK",
     *     ),
     *
     *     @OA\Response(
     *         response="400",
     *         description="Failed",
     *     ),
     *
     *     security={{"apiAuth": {} }},
     *     deprecated=false
     * )
     */
    public function getUserCourse()
    {
        try {
            $course = ProgramModel::join('user_program_models','user_program_models.program_id','=','program.id')
            ->where('user_program_models.user_id',auth()->user()->id)
            ->select('program.*')
            ->get();

        } catch (\Exception $exception) {
            return $this->errorResponse($exception->getMessage(), 400);
        }

        return $this->successResponse([
            'status' => 200,
            'data' => $course,
        ]);
    }

    /**
     * @OA\Post(
     *     path="/admi/program",
     *     summary="Agrega un programa",
     *     tags={"Programs"},
     *
     *     @OA\RequestBody(
     *         required=true,
     *         @OA\MediaType(
     *             mediaType="multipart/form-data",
     *             @OA\Schema(
     *                 @OA\Property(
     *                     property="certificate",
     *                     type="string",
     *                 ),
     *                 @OA\Property(
     *                     property="duration",
     *                     type="Time"
     *                 ),
     *                 @OA\Property(
     *                     property="dateIni",
     *                     type="date"
     *                 ),
     *                 @OA\Property(
     *                     property="title",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="videoUrl",
     *                     type="file"
     *                 ),
     *                 @OA\Property(
     *                     property="description",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="price",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="language",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="abstract",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="requeriments",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="spots",
     *                     type="integer"
     *                 ),
     *
     *                  @OA\Property(
     *                     property="teacher_id",
     *                     type="integer"
     *                 ),
     *                 example={}
     *             )
     *         )
     *     ),
     *
     *     @OA\Response(
     *         response=201,
     *         description="OK",
     *     ),
     *
     *     @OA\Response(
     *         response="400",
     *         description="Failed",
     *     ),
     *
     *     security={{"apiAuth": {} }},
     *
     *     deprecated=false
     * )
     */
    public function add(Request $request)
    {
        try {
            if (auth()->user()->rol < 5 ) {
                return $this->errorResponse('No tiene acceso a esta opciones. Consulte con los administradores del sistema', 401);
            }

            $validator = Validator::make($request->all(), [
                'certificate' => 'required',
                'duration' => 'required',
                'dateIni' => 'required',
                'title' => 'required',
                'videoUrl' => 'required|file|mimes:mp4',
                'description' => 'required',
                'language' => 'required',
                'abstract' => 'required',
                'requeriments' => 'required',
                'teacher_id' => 'required',
                'price' => 'required',
                'spots' => 'required'
            ]);

            if($validator->fails()){

                return $this->errorResponse($validator->errors()->first(), 400);
            }

            $teacher = User::findOrFail($request->teacher_id);

            if ($teacher == null) {
                return $this->errorResponse('No se encontro al profesor.', 400);
            }

            if ($image = $request->file('videoUrl')) {
                $name = "video_program_". $request->name . '_' . time() . "." . $image->guessExtension();
                $ruta = public_path("images/programs/video/" . $name);
                copy($image, $ruta);
            }else{
                return $this->errorResponse('Error al grabar el video.', 400);
            }


            $program = new ProgramModel();
            $program->certificate = $request->certificate;
            $program->duration = $request->duration;
            $program->dateIni = $request->dateIni;
            $program->title = $request->title;
            $program->description = $request->description;
            $program->language = $request->language;
            $program->abstract = $request->abstract;
            $program->spots = $request->spots;
            $program->requeriments = $request->requeriments;
            $program->videoUrl = $ruta;
            $program->teacher_id = $request->teacher_id;
            $program->price = $request->price;

            $program->save();

            return $this->createResponse([
                'status' => 201,
                'message' => 'Programa registrado',
            ]);
        } catch (\Exception $exception) {
            return $this->errorResponse($exception->getMessage(), 400);
        }
    }

    /**
     * @OA\Patch(
     *     path="/admi/program",
     *     summary="Actualizar un programa",
     *     tags={"Programs"},
     *
     *     @OA\RequestBody(
     *         required=true,
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                  @OA\Property(
     *                     property="program_id",
     *                     type="integer"
     *                 ),
     *                 @OA\Property(
     *                     property="certificate",
     *                     type="string",
     *                 ),
     *                 @OA\Property(
     *                     property="duration",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="dateIni",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="title",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="videoUrl",
     *                     type="file"
     *                 ),
     *                 @OA\Property(
     *                     property="description",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="price",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="language",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="abstract",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="requeriments",
     *                     type="string"
     *                 ),
     *
     *                 @OA\Property(
     *                     property="type",
     *                     type="string"
     *                 ),
     *
     *                  @OA\Property(
     *                     property="teacher_id",
     *                     type="integer"
     *                 ),
     *                 example={}
     *             )
     *         )
     *     ),
     *
     *     @OA\Response(
     *         response=201,
     *         description="OK",
     *     ),
     *
     *     @OA\Response(
     *         response="400",
     *         description="Failed",
     *     ),
     *
     *     security={{"apiAuth": {} }},
     *
     *     deprecated=false
     * )
     */
    public function update(Request $request)
    {
        try {
            if (auth()->user()->rol != 5 || auth()->user()->rol != 10) {
                return $this->errorResponse('No tiene acceso a esta opciones. Consulte con los administradores del sistema', 401);
            }

            $validator = Validator::make($request->all(), [
                'program_id' => 'required',
                'certificate' => 'required',
                'duration' => 'required',
                'dateIni' => 'required',
                'title' => 'required',
                'videoUrl' => 'required|file|mimes:mp4',
                'description' => 'required',
                'language' => 'required',
                'abstract' => 'required',
                'requeriments' => 'required',
                'type' => 'required',
                'teacher_id' => 'required',
            ]);

            if($validator->fails()){

                return $this->errorResponse($validator->errors()->first(), 400);
            }


            $program = ProgramModel::findOrFail($request->program_id);
            if ($program==null) {
                return $this->errorResponse('No se encontro el programa', 401);
            }


            $teacher = User::findOrFail($request->teacher_id);

            if ($teacher == null) {
                return $this->errorResponse('No se encontro al profesor.', 401);
            }

            if ($image = $request->file('videoUrl')) {
                $name = "video_program_". $request->name . '_' . time() . "." . $image->guessExtension();
                $ruta = public_path("images/programs/video/" . $name);
                copy($image, $ruta);
                $program->videoUrl = $ruta;
            }else{
                return $this->errorResponse('Error al grabar el video.', 400);
            }


            $program->name = $request->name;
            $program->timeClass = $request->timeClass;
            $program->spots = $request->spots;
            $program->level = $request->level;
            $program->description = $request->description;
            $program->abstract = $request->abstract;
            $program->price = $request->price;
            $program->knowledge = $request->knowledge;
            $program->language = $request->language;
            $program->type = $request->type;
            $program->teacher_id = $request->teacher_id;
            $program->update();

            return $this->createResponse([
                'status' => 201,
                'message' => 'Usuario registrado',
                'data' => $program,
            ]);
        } catch (\Exception $exception) {
            return $this->errorResponse($exception->getMessage(), 400);
        }
    }
    /**
     * @OA\Delete(
     *     path="/admi/program/{program_id}",
     *     summary="Eliminar un programa.",
     *     tags={"Programs"},
     *      @OA\Parameter(
     *          name="program_id",
     *          description="Identificador de programa",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *     @OA\Response(
     *         response=200,
     *         description="OK",
     *     ),
     *
     *     @OA\Response(
     *         response="400",
     *         description="Failed",
     *     ),
     *
     *
     *     deprecated=false
     * )
     */
    public function delete($program_id)
    {
        try {
            $program = ProgramModel::findOrFail($program_id);
            if ($program!=null) {
                $program->delete();
            }else{
                return $this->errorResponse('No se encontro curso.', 400);
            }
        } catch (\Exception $exception) {
            return $this->errorResponse($exception->getMessage(), 400);
        }

        return $this->successResponse([
            'status' => 200,
            'data' => $program,
        ]);
    }

    /**
     * @OA\Post(
     *     path="/program/registerUser",
     *     summary="Registrar un usuario en un curso",
     *     tags={"Programs"},
     *
     *     @OA\RequestBody(
     *         required=true,
     *         @OA\MediaType(
     *             mediaType="multipart/form-data",
     *             @OA\Schema(
     *                 @OA\Property(
     *                     property="user_id",
     *                     type="integer",
     *                 ),
     *                 @OA\Property(
     *                     property="program_id",
     *                     type="integer",
     *                 ),
     *                 @OA\Property(
     *                     property="payment_id",
     *                     type="integer",
     *                 ),
     *                 example={}
     *             )
     *         )
     *     ),
     *
     *     @OA\Response(
     *         response=201,
     *         description="OK",
     *     ),
     *
     *     @OA\Response(
     *         response="400",
     *         description="Failed",
     *     ),
     *
     *     security={{"apiAuth": {} }},
     *
     *     deprecated=false
     * )
     */
    public function RegisterUser(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'user_id' => 'required|integer',
                'program_id' => 'required|string',
                'payment_id' => 'required|string'
            ]);

            if($validator->fails()){

                return $this->errorResponse($validator->errors()->first(), 400);
            }

            $program = ProgramModel::findOrFail($request->program_id);
            $user = User::findOrFail($request->user_id);
            if ($program!=null && $user!=null) {
                $register = new UserProgramModel();
                $register->user_id = $request->user_id;
                $register->program_id = $request->program_id;
                $register->payment_id = $request->payment_id;
                $register->save();
            }else{
                return $this->errorResponse("Error con los identificadores.", 400);
            }

            return $this->createResponse([
                'status' => 201,
                'message' => 'Usuario registrado en el curso',
                'data' => $register,
            ]);
        } catch (\Exception $exception) {
            return $this->errorResponse($exception->getMessage(), 400);
        }
    }

    /**
     * @OA\Get(
     *     path="/program/search/{search}",
     *     summary="Get all programs with this word.",
     *     tags={"Programs"},
     *     @OA\Parameter(
     *          name="search",
     *          description="Palabra a buscar",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *     @OA\Response(
     *         response=200,
     *         description="OK",
     *     ),
     *
     *     @OA\Response(
     *         response="400",
     *         description="Failed",
     *     ),
     *
     *
     *     deprecated=false
     * )
     */
    public function Search($search)
    {
        try {

            $courses = ProgramModel::join('users','users.id','program.teacher_id')
            ->select('program.id','program.name','program.abstract','users.name AS teacher','users.surname AS teacherSurname')
            ->where('program.title','LIKE','%'.$search.'%')
            ->orwhere('program.abstract','LIKE','%'.$search.'%')
            ->orwhere('program.description','LIKE','%'.$search.'%')
            ->limit(5)
            ->get();

            return $this->createResponse([
                'status' => 200,
                'data' => $courses
            ]);
        } catch (\Exception $exception) {
            return $this->errorResponse($exception->getMessage(), 400);
        }
    }

    /**
     * @OA\Get(
     *     path="/program/searchAll/{search}",
     *     summary="Get all courses with this word.",
     *     tags={"Programs"},
     *     @OA\Parameter(
     *          name="search",
     *          description="Palabra a buscar",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *     @OA\Response(
     *         response=200,
     *         description="OK",
     *     ),
     *
     *     @OA\Response(
     *         response="400",
     *         description="Failed",
     *     ),
     *
     *
     *     deprecated=false
     * )
     */
    public function SearchAll($search)
    {
        try {

            $programs = ProgramModel::join('users','users.id','program.teacher_id')
            ->select('program.id','program.name','program.abstract','users.name AS teacher','users.surname AS teacherSurname')
            ->where('program.name','LIKE','%'.$search.'%')
            ->orwhere('program.abstract','LIKE','%'.$search.'%')
            ->orwhere('program.description','LIKE','%'.$search.'%')
            ->orderby('program.id','desc')
            ->paginate(10);

            return $this->createResponse([
                'status' => 200,
                'data' => $programs
            ]);
        } catch (\Exception $exception) {
            return $this->errorResponse($exception->getMessage(), 400);
        }
    }

    /**
     * @OA\Post(
     *     path="/program/assistance",
     *     summary="Registrar un usuario en un curso",
     *     tags={"Programs"},
     *
     *     @OA\RequestBody(
     *         required=true,
     *         @OA\MediaType(
     *             mediaType="multipart/form-data",
     *             @OA\Schema(
     *                  @OA\Property(
     *                     property="program_id",
     *                     type="integer",
     *                 ),
     *                 @OA\Property(
     *                     property="program_class_id",
     *                     type="integer",
     *                 ),
     *                 @OA\Property(
     *                     property="assistance",
     *                     type="integer",
     *                 ),
     *                  @OA\Property(
     *                     property="hours",
     *                     type="integer",
     *                 ),
     *                 example={}
     *             )
     *         )
     *     ),
     *
     *     @OA\Response(
     *         response=201,
     *         description="OK",
     *     ),
     *
     *     @OA\Response(
     *         response="400",
     *         description="Failed",
     *     ),
     *
     *     security={{"apiAuth": {} }},
     *
     *     deprecated=false
     * )
     */
    public function SetAssistance(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'program_id' => 'required|integer',
                'program_class_id' => 'required|integer',
                'assistance' => 'required|integer',
                'hours' => 'required|integer',
            ]);

            if($validator->fails()){

                return $this->errorResponse($validator->errors()->first(), 400);
            }

            $user = UserProgramModel::where('user_id', auth()->user()->id)
            ->where('program_id',$request->program_id)
            ->get()
            ->first();

            if ($user == null) {
                return $this->errorResponse('Usuario no registrado', 400);
            }

            $assistance = UserProgramAssistanceModel::where('user_program_id',$user->id)
            ->where('program_class_id', $request->program_class_id)
            ->get()
            ->first();

            if ($assistance == null) {
                $assistance = new UserProgramAssistanceModel();
                $assistance->user_program_id = $user->id;
                $assistance->program_class_id = $request->program_class_id;
                $assistance->assistance = $request->assistance;
                $assistance->hours = $request->hours;
                $assistance->save();
            }else {
                $assistance->hours += $request->hours;
                $assistance->update();
            }

            return $this->createResponse([
                'status' => 201,
                'message' => 'Asistencia registrada/actualizada',
                'data' => $assistance,
            ]);
        } catch (\Exception $exception) {
            return $this->errorResponse($exception->getMessage(), 400);
        }
    }
}
