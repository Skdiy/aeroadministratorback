<?php

namespace App\Http\Controllers;

use App\Models\EventBusinessCatalogueModel;
use App\Models\EventBusinessFormModel;
use App\Models\EventBusinessInfoModel;
use App\Models\EventContactInfoModel;
use App\Models\EventContactPartsModel;
use App\Models\EventDelegationConferenceModel;
use App\Models\EventDelegationFormModel;
use App\Models\EventDelegationInfoModel;
use App\Models\EventNoticesModel;
use App\Models\EventQuestionsModel;
use App\Models\EventSponsorModel;
use App\Models\EventSubscriptionModel;
use App\Traits\ApiResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class FeriaController extends Controller
{
    use ApiResponse;


    /**
     * @OA\Post(
     *     path="/admi/event/bussines/form",
     *     summary="Agrega un formulario.",
     *     tags={"Event"},
     *
     *     @OA\RequestBody(
     *         required=true,
     *         @OA\MediaType(
     *             mediaType="multipart/form-data",
     *             @OA\Schema(
     *                 @OA\Property(
     *                     property="country",
     *                     type="string",
     *                 ),
     *                 @OA\Property(
     *                     property="name",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="email",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="phone",
     *                     type="string"
     *                 ),
     *                  @OA\Property(
     *                     property="message",
     *                     type="string"
     *                 ),
     *
     *                 example={"country": "Peru", "name": "Primer", "email": "email@gmail.com", "phone": "987654321", "message": "ayuda"}
     *             )
     *         )
     *     ),
     *
     *     @OA\Response(
     *         response=201,
     *         description="OK",
     *     ),
     *
     *     @OA\Response(
     *         response="400",
     *         description="Failed",
     *     ),
     *
     *     security={{"apiAuth": {} }},
     *
     *     deprecated=false
     * )
     */
    public function create_form(Request $request)
    {
        try {
            if (auth()->user()->rol != 10 ) {
                return $this->errorResponse('No tiene acceso a esta opciones. Consulte con los administradores del sistema', 401);
            }

            $validator = Validator::make($request->all(), [
                'country' => 'required',
                'name' => 'required',
                'email' => 'required',
                'phone' => 'required',
                'message' => 'required',
            ]);
            if($validator->fails()){

                return $this->errorResponse($validator->errors()->first(), 400);
            }

            $blog = new EventBusinessFormModel();
            $blog->country = $request->country ;
            $blog->name = $request->name ;
            $blog->email = $request->email ;
            $blog->phone = $request->phone ;
            $blog->message = $request->message ;
            $blog->save();

            return $this->createResponse([
                'status' => 201,
                'message' => 'Formulario registrado.',
                'data' => $blog,
            ]);
        } catch (\Exception $exception) {
            return $this->errorResponse($exception->getMessage(), 400);
        }
    }

    /**
     * @OA\Post(
     *     path="/admi/event/bussines/catalogue",
     *     summary="Agrega un catalogo",
     *     tags={"Event"},
     *
     *     @OA\RequestBody(
     *         required=true,
     *         @OA\MediaType(
     *             mediaType="multipart/form-data",
     *             @OA\Schema(
     *                 @OA\Property(
     *                     property="title",
     *                     type="string",
     *                 ),
     *                 @OA\Property(
     *                     property="photo",
     *                     type="file"
     *                 ),
     *                 @OA\Property(
     *                     property="description",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="state",
     *                     type="boolean"
     *                 ),
     *
     *                 example={"title": "Prueba de catalogo", "photo": "", "description": "Intermedio", "state": 1}
     *             )
     *         )
     *     ),
     *
     *     @OA\Response(
     *         response=201,
     *         description="OK",
     *     ),
     *
     *     @OA\Response(
     *         response="400",
     *         description="Failed",
     *     ),
     *
     *     security={{"apiAuth": {} }},
     *
     *     deprecated=false
     * )
     */
    public function create_catalogue(Request $request)
    {
        try {
            if (auth()->user()->rol != 10 ) {
                return $this->errorResponse('No tiene acceso a esta opciones. Consulte con los administradores del sistema', 401);
            }

            $validator = Validator::make($request->all(), [
                'title' => 'required',
                'photo' => 'required|file|mimes:jpg,jpeg,png',
                'description' => 'required',
                'state' => 'required',
            ]);
            if($validator->fails()){

                return $this->errorResponse($validator->errors()->first(), 400);
            }

            if ($image = $request->file('photo')) {
                $nameIMG = "images/event/bussines/image_bussines_". time() . "." . $image->guessExtension();
                $rutaIMG = public_path($nameIMG);
                copy($image, $rutaIMG);
            }else{
                return $this->errorResponse('Error al grabar la imagen.', 400);
            }

            $blog = new EventBusinessCatalogueModel();
            $blog->title = $request->title ;
            $blog->description = $request->description ;
            $blog->state = $request->state ;
            $blog->photo = $nameIMG ;
            $blog->save();

            return $this->createResponse([
                'status' => 201,
                'message' => 'Catalogo registrado',
                'data' => $blog,
            ]);
        } catch (\Exception $exception) {
            return $this->errorResponse($exception->getMessage(), 400);
        }
    }

    /**
     * @OA\Post(
     *     path="/admi/event/bussines/info",
     *     summary="Actualizar info de empresas",
     *     tags={"Event"},
     *
     *     @OA\RequestBody(
     *         required=true,
     *         @OA\MediaType(
     *             mediaType="multipart/form-data",
     *             @OA\Schema(
     *                 @OA\Property(
     *                     property="professional_visitors",
     *                     type="string",
     *                 ),
     *                 @OA\Property(
     *                     property="official_delegations",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="countries",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="file",
     *                     type="file"
     *                 ),
     *
     *                 example={"professional_visitors": "10", "official_delegations": "20", "countries": "20", "file": ""}
     *             )
     *         )
     *     ),
     *
     *     @OA\Response(
     *         response=201,
     *         description="OK",
     *     ),
     *
     *     @OA\Response(
     *         response="400",
     *         description="Failed",
     *     ),
     *
     *     security={{"apiAuth": {} }},
     *
     *     deprecated=false
     * )
     */
    public function bussiness_info(Request $request)
    {
        try {
            if (auth()->user()->rol != 10 ) {
                return $this->errorResponse('No tiene acceso a esta opciones. Consulte con los administradores del sistema', 401);
            }

            $validator = Validator::make($request->all(), [
                'professional_visitors' => 'required',
                'official_delegations' => 'required',
                'countries' => 'required',
                'file' => 'required|file',
            ]);
            if($validator->fails()){

                return $this->errorResponse($validator->errors()->first(), 400);
            }

            if ($image = $request->file('file')) {
                $nameIMG = "images/event/bussines/file/bussines_". time() . "." . $image->guessExtension();
                $rutaIMG = public_path($nameIMG);
                copy($image, $rutaIMG);
            }else{
                return $this->errorResponse('Error al grabar la imagen.', 400);
            }

            $info = EventBusinessInfoModel::findOrFail(1);

            if ($info == null) {
                $info = new EventBusinessInfoModel();
                $info->professional_visitors = $request->professional_visitors ;
                $info->official_delegations = $request->official_delegations ;
                $info->countries = $request->countries ;
                $info->file = $nameIMG ;
                $info->save();
            }else {

                $info->professional_visitors = $request->professional_visitors ;
                $info->official_delegations = $request->official_delegations ;
                $info->countries = $request->countries ;
                $info->file = $nameIMG ;
                $info->update();
            }

            return $this->createResponse([
                'status' => 201,
                'message' => 'Informacion actualizada.',
                'data' => $info,
            ]);

        } catch (\Exception $exception) {
            return $this->errorResponse($exception->getMessage(), 400);
        }
    }



    /**
     * @OA\Get(
     *     path="event/bussines/form",
     *     summary="Obetener todos los formularios.",
     *     tags={"Event"},
     *
     *     @OA\Response(
     *         response=200,
     *         description="OK",
     *     ),
     *
     *     @OA\Response(
     *         response="400",
     *         description="Failed",
     *     ),
     *
     *
     *     deprecated=false
     * )
     */
    public function get_form()
    {
        try {
            $forms = EventBusinessFormModel::all();


        } catch (\Exception $exception) {
            return $this->errorResponse($exception->getMessage(), 400);
        }

        return $this->successResponse([
            'status' => 200,
            'data' => $forms
        ]);
    }


    /**
     * @OA\Get(
     *     path="event/bussines/catalogue",
     *     summary="Obetener todos los catalogos.",
     *     tags={"Event"},
     *
     *     @OA\Response(
     *         response=200,
     *         description="OK",
     *     ),
     *
     *     @OA\Response(
     *         response="400",
     *         description="Failed",
     *     ),
     *
     *
     *     deprecated=false
     * )
     */
    public function get_catalogue()
    {
        try {
            $catalogue = EventBusinessCatalogueModel::all();


        } catch (\Exception $exception) {
            return $this->errorResponse($exception->getMessage(), 400);
        }

        return $this->successResponse([
            'status' => 200,
            'data' => $catalogue
        ]);
    }

    /**
     * @OA\Get(
     *     path="event/bussines/info",
     *     summary="Obetener todos los catalogos.",
     *     tags={"Event"},
     *
     *     @OA\Response(
     *         response=200,
     *         description="OK",
     *     ),
     *
     *     @OA\Response(
     *         response="400",
     *         description="Failed",
     *     ),
     *
     *
     *     deprecated=false
     * )
     */
    public function get_info()
    {
        try {
            $info = EventBusinessCatalogueModel::findOrFail(1);


        } catch (\Exception $exception) {
            return $this->errorResponse($exception->getMessage(), 400);
        }

        return $this->successResponse([
            'status' => 200,
            'data' => $info
        ]);
    }

    /**
     * @OA\Post(
     *     path="/admi/event/contact/part",
     *     summary="Agrega un part de contacto.",
     *     tags={"Event"},
     *
     *     @OA\RequestBody(
     *         required=true,
     *         @OA\MediaType(
     *             mediaType="multipart/form-data",
     *             @OA\Schema(
     *                 @OA\Property(
     *                     property="title",
     *                     type="string",
     *                 ),
     *                 @OA\Property(
     *                     property="image",
     *                     type="file"
     *                 ),
     *                 @OA\Property(
     *                     property="abstract",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="description",
     *                     type="string"
     *                 ),
     *                  @OA\Property(
     *                     property="languages",
     *                     type="string"
     *                 ),
     *                  @OA\Property(
     *                     property="type",
     *                     type="integer"
     *                 ),
     *
     *                 example={}
     *             )
     *         )
     *     ),
     *
     *     @OA\Response(
     *         response=201,
     *         description="OK",
     *     ),
     *
     *     @OA\Response(
     *         response="400",
     *         description="Failed",
     *     ),
     *
     *     security={{"apiAuth": {} }},
     *
     *     deprecated=false
     * )
     */
    public function create_contact_part(Request $request)
    {
        try {
            if (auth()->user()->rol != 10 ) {
                return $this->errorResponse('No tiene acceso a esta opciones. Consulte con los administradores del sistema', 401);
            }

            $validator = Validator::make($request->all(), [
                'title' => 'required',
                'image' => 'required|file|mimes:jpg,jpeg,png',
                'abstract' => 'required',
                'description' => 'required',
                'languages' => 'required',
                'type' => 'required',
            ]);
            if($validator->fails()){

                return $this->errorResponse($validator->errors()->first(), 400);
            }

            if ($image = $request->file('image')) {
                $nameIMG = "images/event/contact/image_bussines_". time() . "." . $image->guessExtension();
                $rutaIMG = public_path($nameIMG);
                copy($image, $rutaIMG);
            }else{
                return $this->errorResponse('Error al grabar la imagen.', 400);
            }

            $contact = new EventContactPartsModel();
            $contact->title = $request->title ;
            $contact->abstract = $request->abstract ;
            $contact->description = $request->description ;
            $contact->languages = $request->languages ;
            $contact->type = $request->type ;
            $contact->urlimg = $nameIMG ;
            $contact->save();

            return $this->createResponse([
                'status' => 201,
                'message' => 'Part registrado.',
                'data' => $contact,
            ]);
        } catch (\Exception $exception) {
            return $this->errorResponse($exception->getMessage(), 400);
        }
    }

    /**
     * @OA\Post(
     *     path="/admi/event/contact/info",
     *     summary="Actualizar info de contacto.",
     *     tags={"Event"},
     *
     *     @OA\RequestBody(
     *         required=true,
     *         @OA\MediaType(
     *             mediaType="multipart/form-data",
     *             @OA\Schema(
     *                 @OA\Property(
     *                     property="title",
     *                     type="string",
     *                 ),
     *                 @OA\Property(
     *                     property="description",
     *                     type="file"
     *                 ),
     *
     *
     *                 example={}
     *             )
     *         )
     *     ),
     *
     *     @OA\Response(
     *         response=201,
     *         description="OK",
     *     ),
     *
     *     @OA\Response(
     *         response="400",
     *         description="Failed",
     *     ),
     *
     *     security={{"apiAuth": {} }},
     *
     *     deprecated=false
     * )
     */
    public function update_contact_info(Request $request)
    {
        try {
            if (auth()->user()->rol != 10 ) {
                return $this->errorResponse('No tiene acceso a esta opciones. Consulte con los administradores del sistema', 401);
            }

            $validator = Validator::make($request->all(), [
                'title' => 'required',
                'description' => 'required',
            ]);
            if($validator->fails()){

                return $this->errorResponse($validator->errors()->first(), 400);
            }

            $contact = EventContactInfoModel::findOrFail(1);

            if ($contact == null) {
                $contact = new EventContactInfoModel();
                $contact->title = $request->title ;
                $contact->description = $request->description ;
                $contact->save();
            }else {
                $contact->title = $request->title ;
                $contact->description = $request->description ;
                $contact->update();
            }

            return $this->createResponse([
                'status' => 201,
                'message' => 'Contacto actualizado.',
                'data' => $contact,
            ]);
        } catch (\Exception $exception) {
            return $this->errorResponse($exception->getMessage(), 400);
        }
    }

    /**
     * @OA\Get(
     *     path="event/contact/part",
     *     summary="Obetener todos las partes de contacto.",
     *     tags={"Event"},
     *
     *     @OA\Response(
     *         response=200,
     *         description="OK",
     *     ),
     *
     *     @OA\Response(
     *         response="400",
     *         description="Failed",
     *     ),
     *
     *
     *     deprecated=false
     * )
     */
    public function get_contact_part()
    {
        try {
            $catalogue = EventContactPartsModel::all();


        } catch (\Exception $exception) {
            return $this->errorResponse($exception->getMessage(), 400);
        }

        return $this->successResponse([
            'status' => 200,
            'data' => $catalogue
        ]);
    }

    /**
     * @OA\Get(
     *     path="event/contact/info",
     *     summary="Obetener info de contact.",
     *     tags={"Event"},
     *
     *     @OA\Response(
     *         response=200,
     *         description="OK",
     *     ),
     *
     *     @OA\Response(
     *         response="400",
     *         description="Failed",
     *     ),
     *
     *
     *     deprecated=false
     * )
     */
    public function get_contact_info()
    {
        try {
            $info = EventContactInfoModel::findOrFail(1);
        } catch (\Exception $exception) {
            return $this->errorResponse($exception->getMessage(), 400);
        }

        return $this->successResponse([
            'status' => 200,
            'data' => $info
        ]);
    }

    /**
     * @OA\Post(
     *     path="/admi/event/delegation/conference",
     *     summary="Agrega una conferencia de una delegación.",
     *     tags={"Event"},
     *
     *     @OA\RequestBody(
     *         required=true,
     *         @OA\MediaType(
     *             mediaType="multipart/form-data",
     *             @OA\Schema(
     *                 @OA\Property(
     *                     property="name",
     *                     type="string",
     *                 ),
     *                 @OA\Property(
     *                     property="corporation",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="date",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="image",
     *                     type="file"
     *                 ),
     *                  @OA\Property(
     *                     property="state",
     *                     type="integer"
     *                 ),
     *
     *                 example={}
     *             )
     *         )
     *     ),
     *
     *     @OA\Response(
     *         response=201,
     *         description="OK",
     *     ),
     *
     *     @OA\Response(
     *         response="400",
     *         description="Failed",
     *     ),
     *
     *     security={{"apiAuth": {} }},
     *
     *     deprecated=false
     * )
     */
    public function create_delegation_conference(Request $request)
    {
        try {
            if (auth()->user()->rol != 10 ) {
                return $this->errorResponse('No tiene acceso a esta opciones. Consulte con los administradores del sistema', 401);
            }

            $validator = Validator::make($request->all(), [
                'name' => 'required',
                'corporation' => 'required',
                'date' => 'required',
                'state' => 'required',
                'image' => 'required|file|mimes:jpg,jpeg,png',
            ]);
            if($validator->fails()){

                return $this->errorResponse($validator->errors()->first(), 400);
            }

            if ($image = $request->file('image')) {
                $nameIMG = "images/event/delegation/image_bussines_". time() . "." . $image->guessExtension();
                $rutaIMG = public_path($nameIMG);
                copy($image, $rutaIMG);
            }else{
                return $this->errorResponse('Error al grabar la imagen.', 400);
            }

            $conference = new EventDelegationConferenceModel();
            $conference->name = $request->name ;
            $conference->corporation = $request->corporation ;
            $conference->date = $request->date ;
            $conference->state = $request->state ;
            $conference->imgUrl = $nameIMG ;
            $conference->save();

            return $this->createResponse([
                'status' => 201,
                'message' => 'Conferencia registrada.',
                'data' => $conference,
            ]);
        } catch (\Exception $exception) {
            return $this->errorResponse($exception->getMessage(), 400);
        }
    }
    /**
     * @OA\Post(
     *     path="/admi/event/delegation/form",
     *     summary="Agrega un formulario de una delegación.",
     *     tags={"Event"},
     *
     *     @OA\RequestBody(
     *         required=true,
     *         @OA\MediaType(
     *             mediaType="multipart/form-data",
     *             @OA\Schema(
     *                 @OA\Property(
     *                     property="name",
     *                     type="string",
     *                 ),
     *                 @OA\Property(
     *                     property="email",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="phone",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="message",
     *                     type="string"
     *                 ),
     *
     *                 example={}
     *             )
     *         )
     *     ),
     *
     *     @OA\Response(
     *         response=201,
     *         description="OK",
     *     ),
     *
     *     @OA\Response(
     *         response="400",
     *         description="Failed",
     *     ),
     *
     *     security={{"apiAuth": {} }},
     *
     *     deprecated=false
     * )
     */
    public function create_delegation_form(Request $request)
    {
        try {
            if (auth()->user()->rol != 10 ) {
                return $this->errorResponse('No tiene acceso a esta opciones. Consulte con los administradores del sistema', 401);
            }

            $validator = Validator::make($request->all(), [
                'name' => 'required',
                'email' => 'required',
                'phone' => 'required',
                'message' => 'required',
            ]);
            if($validator->fails()){

                return $this->errorResponse($validator->errors()->first(), 400);
            }

            $form = new EventDelegationFormModel();
            $form->name = $request->name ;
            $form->email = $request->email ;
            $form->phone = $request->phone ;
            $form->message = $request->message ;
            $form->save();

            return $this->createResponse([
                'status' => 201,
                'message' => 'Formulario registrado.',
                'data' => $form,
            ]);
        } catch (\Exception $exception) {
            return $this->errorResponse($exception->getMessage(), 400);
        }
    }

    /**
     * @OA\Post(
     *     path="/admi/event/delegation/info",
     *     summary="Actualizar info de delegacion.",
     *     tags={"Event"},
     *
     *     @OA\RequestBody(
     *         required=true,
     *         @OA\MediaType(
     *             mediaType="multipart/form-data",
     *             @OA\Schema(
     *                 @OA\Property(
     *                     property="file",
     *                     type="file",
     *                 ),
     *                 example={}
     *             )
     *         )
     *     ),
     *
     *     @OA\Response(
     *         response=201,
     *         description="OK",
     *     ),
     *
     *     @OA\Response(
     *         response="400",
     *         description="Failed",
     *     ),
     *
     *     security={{"apiAuth": {} }},
     *
     *     deprecated=false
     * )
     */
    public function update_delegation_info(Request $request)
    {
        try {
            if (auth()->user()->rol != 10 ) {
                return $this->errorResponse('No tiene acceso a esta opciones. Consulte con los administradores del sistema', 401);
            }

            $validator = Validator::make($request->all(), [
                'file' => 'required|file',
            ]);
            if($validator->fails()){

                return $this->errorResponse($validator->errors()->first(), 400);
            }

            if ($image = $request->file('file')) {
                $nameIMG = "images/event/delegation/file_bussines_". time() . "." . $image->guessExtension();
                $rutaIMG = public_path($nameIMG);
                copy($image, $rutaIMG);
            }else{
                return $this->errorResponse('Error al grabar la imagen.', 400);
            }

            $delegation = EventDelegationInfoModel::findOrFail(1);

            if ($delegation == null) {
                $delegation = new EventDelegationInfoModel();
                $delegation->file = $nameIMG ;
                $delegation->save();
            }else {
                $delegation->file = $nameIMG ;
                $delegation->update();
            }

            return $this->createResponse([
                'status' => 201,
                'message' => 'Contacto actualizado.',
                'data' => $delegation,
            ]);
        } catch (\Exception $exception) {
            return $this->errorResponse($exception->getMessage(), 400);
        }
    }

    /**
     * @OA\Get(
     *     path="event/delegation/conference",
     *     summary="Obetener todos las conferencias de la delegación.",
     *     tags={"Event"},
     *
     *     @OA\Response(
     *         response=200,
     *         description="OK",
     *     ),
     *
     *     @OA\Response(
     *         response="400",
     *         description="Failed",
     *     ),
     *
     *
     *     deprecated=false
     * )
     */
    public function get_delegation_conference()
    {
        try {
            $catalogue = EventDelegationConferenceModel::all();


        } catch (\Exception $exception) {
            return $this->errorResponse($exception->getMessage(), 400);
        }

        return $this->successResponse([
            'status' => 200,
            'data' => $catalogue
        ]);
    }

    /**
     * @OA\Get(
     *     path="event/delegation/form",
     *     summary="Obetener formulario de las delegaciones.",
     *     tags={"Event"},
     *
     *     @OA\Response(
     *         response=200,
     *         description="OK",
     *     ),
     *
     *     @OA\Response(
     *         response="400",
     *         description="Failed",
     *     ),
     *
     *
     *     deprecated=false
     * )
     */
    public function get_delegation_form()
    {
        try {
            $info = EventDelegationFormModel::all();
        } catch (\Exception $exception) {
            return $this->errorResponse($exception->getMessage(), 400);
        }

        return $this->successResponse([
            'status' => 200,
            'data' => $info
        ]);
    }

    /**
     * @OA\Get(
     *     path="event/delegation/info",
     *     summary="Obetener info de la delegación.",
     *     tags={"Event"},
     *
     *     @OA\Response(
     *         response=200,
     *         description="OK",
     *     ),
     *
     *     @OA\Response(
     *         response="400",
     *         description="Failed",
     *     ),
     *
     *
     *     deprecated=false
     * )
     */
    public function get_delegation_info()
    {
        try {
            $info = EventDelegationInfoModel::findOrFail(1);
        } catch (\Exception $exception) {
            return $this->errorResponse($exception->getMessage(), 400);
        }

        return $this->successResponse([
            'status' => 200,
            'data' => $info
        ]);
    }

    /**
     * @OA\Post(
     *     path="/admi/event/question",
     *     summary="Crear una pregunta.",
     *     tags={"Event"},
     *
     *     @OA\RequestBody(
     *         required=true,
     *         @OA\MediaType(
     *             mediaType="multipart/form-data",
     *             @OA\Schema(
     *                 @OA\Property(
     *                     property="question",
     *                     type="string",
     *                 ),
     *                 @OA\Property(
     *                     property="answer",
     *                     type="string",
     *                 ),
     *                  @OA\Property(
     *                     property="state",
     *                     type="integer",
     *                 ),
     *                 example={}
     *             )
     *         )
     *     ),
     *
     *     @OA\Response(
     *         response=201,
     *         description="OK",
     *     ),
     *
     *     @OA\Response(
     *         response="400",
     *         description="Failed",
     *     ),
     *
     *     security={{"apiAuth": {} }},
     *
     *     deprecated=false
     * )
     */
    public function create_question(Request $request)
    {
        try {
            if (auth()->user()->rol != 10 ) {
                return $this->errorResponse('No tiene acceso a esta opciones. Consulte con los administradores del sistema', 401);
            }

            $validator = Validator::make($request->all(), [
                'question' => 'required',
                'answer' => 'required',
                'state' => 'required',
            ]);
            if($validator->fails()){

                return $this->errorResponse($validator->errors()->first(), 400);
            }

            $delegation = new EventQuestionsModel();
            $delegation->question = $request->question ;
            $delegation->answer = $request->answer ;
            $delegation->state = $request->state ;
            $delegation->save();


            return $this->createResponse([
                'status' => 201,
                'message' => 'Pregunta registrada.',
                'data' => $delegation,
            ]);
        } catch (\Exception $exception) {
            return $this->errorResponse($exception->getMessage(), 400);
        }
    }

    /**
     * @OA\Get(
     *     path="event/question",
     *     summary="Obtener las preguntas frecuentas.",
     *     tags={"Event"},
     *
     *     @OA\Response(
     *         response=200,
     *         description="OK",
     *     ),
     *
     *     @OA\Response(
     *         response="400",
     *         description="Failed",
     *     ),
     *
     *
     *     deprecated=false
     * )
     */
    public function get_questions()
    {
        try {
            $questions = EventQuestionsModel::all();


        } catch (\Exception $exception) {
            return $this->errorResponse($exception->getMessage(), 400);
        }

        return $this->successResponse([
            'status' => 200,
            'data' => $questions
        ]);
    }

    /**
     * @OA\Post(
     *     path="/admi/event/sponsor",
     *     summary="Crear un sponsor.",
     *     tags={"Event"},
     *
     *     @OA\RequestBody(
     *         required=true,
     *         @OA\MediaType(
     *             mediaType="multipart/form-data",
     *             @OA\Schema(
     *                 @OA\Property(
     *                     property="name",
     *                     type="string",
     *                 ),
     *                 @OA\Property(
     *                     property="image",
     *                     type="file",
     *                 ),
     *                  @OA\Property(
     *                     property="state",
     *                     type="integer",
     *                 ),
     *                 example={}
     *             )
     *         )
     *     ),
     *
     *     @OA\Response(
     *         response=201,
     *         description="OK",
     *     ),
     *
     *     @OA\Response(
     *         response="400",
     *         description="Failed",
     *     ),
     *
     *     security={{"apiAuth": {} }},
     *
     *     deprecated=false
     * )
     */
    public function create_sponsor(Request $request)
    {
        try {
            if (auth()->user()->rol != 10 ) {
                return $this->errorResponse('No tiene acceso a esta opciones. Consulte con los administradores del sistema', 401);
            }

            $validator = Validator::make($request->all(), [
                'name' => 'required',
                'image' => 'required',
                'state' => 'required',
            ]);
            if($validator->fails()){

                return $this->errorResponse($validator->errors()->first(), 400);
            }

            if ($image = $request->file('image')) {
                $nameIMG = "images/event/sponsor/image_sponsor_". time() . "." . $image->guessExtension();
                $rutaIMG = public_path($nameIMG);
                copy($image, $rutaIMG);
            }else{
                return $this->errorResponse('Error al grabar la imagen.', 400);
            }

            $sponsor = new EventSponsorModel();
            $sponsor->name = $request->name ;
            $sponsor->urlimg = $nameIMG ;
            $sponsor->state = $request->state ;
            $sponsor->save();


            return $this->createResponse([
                'status' => 201,
                'message' => 'Sponsor registrada.',
                'data' => $sponsor,
            ]);
        } catch (\Exception $exception) {
            return $this->errorResponse($exception->getMessage(), 400);
        }
    }

    /**
     * @OA\Get(
     *     path="event/sponsor",
     *     summary="Obtener los sponsor frecuentas.",
     *     tags={"Event"},
     *
     *     @OA\Response(
     *         response=200,
     *         description="OK",
     *     ),
     *
     *     @OA\Response(
     *         response="400",
     *         description="Failed",
     *     ),
     *
     *
     *     deprecated=false
     * )
     */
    public function get_sponsor()
    {
        try {
            $sponsor = EventSponsorModel::all();


        } catch (\Exception $exception) {
            return $this->errorResponse($exception->getMessage(), 400);
        }

        return $this->successResponse([
            'status' => 200,
            'data' => $sponsor
        ]);
    }

    /**
     * @OA\Post(
     *     path="/admi/event/subscription",
     *     summary="Crear una subscription.",
     *     tags={"Event"},
     *
     *     @OA\RequestBody(
     *         required=true,
     *         @OA\MediaType(
     *             mediaType="multipart/form-data",
     *             @OA\Schema(
     *                 @OA\Property(
     *                     property="bussines",
     *                     type="string",
     *                 ),
     *                 @OA\Property(
     *                     property="country",
     *                     type="string",
     *                 ),
     *                  @OA\Property(
     *                     property="city",
     *                     type="string",
     *                 ),
     *                  @OA\Property(
     *                     property="phone",
     *                     type="string",
     *                 ),
     *                   @OA\Property(
     *                     property="web",
     *                     type="string",
     *                 ),
     *                   @OA\Property(
     *                     property="rubro",
     *                     type="string",
     *                 ),
     *                   @OA\Property(
     *                     property="code",
     *                     type="string",
     *                 ),
     *                   @OA\Property(
     *                     property="representante",
     *                     type="string",
     *                 ),
     *                    @OA\Property(
     *                     property="cargo",
     *                     type="string",
     *                 ),
     *                    @OA\Property(
     *                     property="contact_number",
     *                     type="string",
     *                 ),
     *                    @OA\Property(
     *                     property="email",
     *                     type="string",
     *                 ),
     *                    @OA\Property(
     *                     property="others",
     *                     type="string",
     *                 ),
     *                     @OA\Property(
     *                     property="integrants",
     *                     type="integer",
     *                 ),
     *                 example={}
     *             )
     *         )
     *     ),
     *
     *     @OA\Response(
     *         response=201,
     *         description="OK",
     *     ),
     *
     *     @OA\Response(
     *         response="400",
     *         description="Failed",
     *     ),
     *
     *     security={{"apiAuth": {} }},
     *
     *     deprecated=false
     * )
     */
    public function create_subscription(Request $request)
    {
        try {
            if (auth()->user()->rol != 10 ) {
                return $this->errorResponse('No tiene acceso a esta opciones. Consulte con los administradores del sistema', 401);
            }

            $validator = Validator::make($request->all(), [
                'bussines' => 'required',
                'country' => 'required',
                'phone' => 'required',
                'city' => 'required',
                'web' => 'required',
                'rubro' => 'required',
                'code' => 'required',
                'representante' => 'required',
                'cargo' => 'required',
                'contact_number' => 'required',
                'email' => 'required',
                'integrants' => 'required',
            ]);
            if($validator->fails()){

                return $this->errorResponse($validator->errors()->first(), 400);
            }

            $subscription = new EventSubscriptionModel();
            $subscription->bussines = $request->bussines ;
            $subscription->country = $request->country ;
            $subscription->phone = $request->phone ;
            $subscription->city = $request->city ;
            $subscription->web = $request->web ;
            $subscription->rubro = $request->rubro ;
            $subscription->code = $request->code ;
            $subscription->representante = $request->representante ;
            $subscription->cargo = $request->cargo ;
            $subscription->contact_number = $request->contact_number ;
            $subscription->email = $request->email ;
            $subscription->integrants = $request->integrants ;
            $subscription->others = $request->others ;
            $subscription->save();


            return $this->createResponse([
                'status' => 201,
                'message' => 'Subscription registrada.',
                'data' => $subscription,
            ]);
        } catch (\Exception $exception) {
            return $this->errorResponse($exception->getMessage(), 400);
        }
    }

    /**
     * @OA\Get(
     *     path="event/subscription",
     *     summary="Obtener los sponsor frecuentas.",
     *     tags={"Event"},
     *
     *     @OA\Response(
     *         response=200,
     *         description="OK",
     *     ),
     *
     *     @OA\Response(
     *         response="400",
     *         description="Failed",
     *     ),
     *
     *
     *     deprecated=false
     * )
     */
    public function get_subscription()
    {
        try {
            $sponsor = EventSubscriptionModel::all();


        } catch (\Exception $exception) {
            return $this->errorResponse($exception->getMessage(), 400);
        }

        return $this->successResponse([
            'status' => 200,
            'data' => $sponsor
        ]);
    }

    /**
     * @OA\Post(
     *     path="/admi/event/notice",
     *     summary="Crear una noticia.",
     *     tags={"Event"},
     *
     *     @OA\RequestBody(
     *         required=true,
     *         @OA\MediaType(
     *             mediaType="multipart/form-data",
     *             @OA\Schema(
     *                 @OA\Property(
     *                     property="name",
     *                     type="string",
     *                 ),
     *                 @OA\Property(
     *                     property="urlSocial",
     *                     type="string",
     *                 ),
     *                  @OA\Property(
     *                     property="type_social",
     *                     type="integer",
     *                 ),
     *
     *
     *                 example={}
     *             )
     *         )
     *     ),
     *
     *     @OA\Response(
     *         response=201,
     *         description="OK",
     *     ),
     *
     *     @OA\Response(
     *         response="400",
     *         description="Failed",
     *     ),
     *
     *     security={{"apiAuth": {} }},
     *
     *     deprecated=false
     * )
     */
    public function create_notice(Request $request)
    {
        try {
            if (auth()->user()->rol != 10 ) {
                return $this->errorResponse('No tiene acceso a esta opciones. Consulte con los administradores del sistema', 401);
            }

            $validator = Validator::make($request->all(), [
                'name' => 'required',
                'urlSocial' => 'required',
                'type_social' => 'required',
            ]);
            if($validator->fails()){

                return $this->errorResponse($validator->errors()->first(), 400);
            }

            $notice = new EventNoticesModel();
            $notice->name = $request->name ;
            $notice->urlSocial = $request->urlSocial ;
            $notice->type_social = $request->type_social ;
            $notice->save();


            return $this->createResponse([
                'status' => 201,
                'message' => 'Noticia registrada.',
                'data' => $notice,
            ]);
        } catch (\Exception $exception) {
            return $this->errorResponse($exception->getMessage(), 400);
        }
    }

    /**
     * @OA\Get(
     *     path="event/notice",
     *     summary="Obtener las noticias.",
     *     tags={"Event"},
     *
     *     @OA\Response(
     *         response=200,
     *         description="OK",
     *     ),
     *
     *     @OA\Response(
     *         response="400",
     *         description="Failed",
     *     ),
     *
     *
     *     deprecated=false
     * )
     */
    public function get_notice()
    {
        try {
            $sponsor = EventNoticesModel::all();


        } catch (\Exception $exception) {
            return $this->errorResponse($exception->getMessage(), 400);
        }

        return $this->successResponse([
            'status' => 200,
            'data' => $sponsor
        ]);
    }
}
