<?php

namespace App\Http\Controllers;

use App\Models\ClassRealTime;
use App\Models\CommentClassModel;
use App\Models\Course;
use App\Models\ZoomMeeting;
use App\Traits\ApiResponse;
use App\Traits\ZoomMeetingTrait;
use Illuminate\Http\Request;

class RealTimeController extends Controller
{
    use ApiResponse;
    use ZoomMeetingTrait;
    /**
     * @OA\Get(
     *     path="/realtime/{course_id}",
     *     summary="Devuelve la proxima clase.",
     *     tags={"Realtime Class"},
     *      @OA\Parameter(
     *          name="course_id",
     *          description="Identificador de cursos",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *     @OA\Response(
     *         response=200,
     *         description="OK",
     *     ),
     *
     *     @OA\Response(
     *         response="400",
     *         description="Failed",
     *     ),
     *
     *
     *     deprecated=false
     * )
     */
    public function getCarousel($course_id)
    {
        try {
            $course = Course::findOrFail($course_id);
            if ($course == null) {
                return $this->errorResponse('Curso no existe.', 400);
            }elseif ( $course->type == Course::REALTIME ) {
                $realtime = ClassRealTime::where('course_id',$course_id)->get()->first();
            }else {
                return $this->errorResponse('Curso no es realtime.', 401);
            }
        } catch (\Exception $exception) {
            return $this->errorResponse($exception->getMessage(), 402);
        }

        return $this->successResponse([
            'status' => 200,
            'data' =>  $realtime,
        ]);
    }

    /**
     * @OA\Post(
     *     path="/admi/realtime/{course_id}",
     *     summary="Crear link de proxima clase en realtime.",
     *     tags={"Realtime Class"},
     *     @OA\Parameter(
     *          name="course_id",
     *          description="Identificador de cursos",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *     @OA\RequestBody(
     *         required=true,
     *         @OA\MediaType(
     *             mediaType="multipart/form-data",
     *             @OA\Schema(
     *                 @OA\Property(
     *                     property="start_time",
     *                     type="datatime",
     *                 ),
     *
     *                 example={}
     *             )
     *         )
     *     ),
     *
     *     @OA\Response(
     *         response=201,
     *         description="OK",
     *     ),
     *
     *     @OA\Response(
     *         response="400",
     *         description="Failed",
     *     ),
     *
     *     security={{"apiAuth": {} }},
     *
     *     deprecated=false
     * )
     */
    public function store(Request $request,$course_id)
    {
        $course = Course::findOrFail($course_id);
        if ( $course != null) {
            if ($course->type == Course::REALTIME ) {
                $class = ClassRealTime::where('course_id',$course_id)->get()->first();

                if ($class == null) {
                    $data = [
                        'start_time' => $request->start_time,
                        'topic' => $course->name,
                        'duration' => 60,
                        'agenda' => 'Clase en tiempo real'
                    ];
                    $meeting = $this->create($data);
                    $Newmeeting = new ZoomMeeting();
                    $Newmeeting->zoom_id = $meeting['data']['id'];
                    $Newmeeting->join_url = $meeting['data']['join_url'];
                    $Newmeeting->password = $meeting['data']['password'];
                    $Newmeeting->topic = 'Clase en tiempo real';
                    $Newmeeting->start_time = $request->start_time;
                    $Newmeeting->duration = 60;
                    $Newmeeting->agenda = 'agenda';
                    $Newmeeting->save();

                    $class = new ClassRealTime();
                    $class->course_id = $course_id;
                    $class->nextclass = $request->start_time;
                    $class->zoom_id = $Newmeeting->id;
                    $class->save();
                }else {

                    $data = [
                        'start_time' => $request->start_time,
                        'topic' => $course->name,
                        'duration' => 60,
                        'agenda' => 'Clase en tiempo real'
                    ];
                    $meeting = $this->create($data);
                    $Newmeeting = new ZoomMeeting();
                    $Newmeeting->zoom_id = $meeting['data']['id'];
                    $Newmeeting->join_url = $meeting['data']['join_url'];
                    $Newmeeting->password = $meeting['data']['password'];
                    $Newmeeting->topic = $request->topic;
                    $Newmeeting->start_time = $request->start_time;
                    $Newmeeting->duration = $request->duration;
                    $Newmeeting->agenda = $request->agenda;
                    $Newmeeting->save();


                    $class->course_id = $course_id;
                    $class->nextclass = $request->start_time;
                    $class->zoom_id = $Newmeeting->id;
                    $class->update();
                }

                return $this->createResponse([
                    'status' => 201,
                    'message' => 'Se guardo correctamente la siguiente clase.'
                ]);

            }else {
                return $this->errorResponse("Curso no es realtime.", 400);
            }

        }else {
            return $this->errorResponse("Curso no encontrado.", 400);
        }
    }


    /**
     * @OA\Get(
     *     path="/realtime/comment/{course_id}",
     *     summary="Obtener comentarios de un curso",
     *     tags={"Realtime Class"},
     *      @OA\Parameter(
     *          name="course_id",
     *          description="Identificador de cursos",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *     @OA\Response(
     *         response=200,
     *         description="OK",
     *     ),
     *
     *     @OA\Response(
     *         response="400",
     *         description="Failed",
     *     ),
     *
     *
     *     deprecated=false
     * )
     */
    public function get_comments($course_id,Request $request)
    {
        try {

            $class = ClassRealTime::findOrFail($course_id);
            if ($class == null) {
                return $this->errorResponse('Clase no encontrada.', 400);
            }

            $count = CommentClassModel::where('class_real_time_id',$course_id)
            ->where('type',CommentClassModel::COMMENT)
            ->count();
            if (isset($request->skip)) {
                $skip = $request->skip;
            }else {
                $skip = 0;
            }

            $limit = $count - $skip; // the limit
            $comments = CommentClassModel::where('type',CommentClassModel::COMMENT)->skip($skip)->take($limit)->get();

            return $this->successResponse([
                'status' => 200,
                'data' =>
                    $comments
                ,
            ]);


        } catch (\Exception $exception) {
            return $this->errorResponse($exception->getMessage(), 400);
        }


    }

    /**
     * @OA\Post(
     *     path="/realtime/comment/{course_id}",
     *     summary="Registrar un comentario",
     *     tags={"Realtime Class"},
     *
     *      @OA\Parameter(
     *          name="course_id",
     *          description="Identificador de comentario",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *     @OA\RequestBody(
     *         required=true,
     *         @OA\MediaType(
     *             mediaType="multipart/form-data",
     *             @OA\Schema(
     *                 @OA\Property(
     *                     property="comment",
     *                     type="string",
     *                 ),
     *
     *                 example={}
     *             )
     *         )
     *     ),
     *
     *     @OA\Response(
     *         response=201,
     *         description="OK",
     *     ),
     *
     *     @OA\Response(
     *         response="400",
     *         description="Failed",
     *     ),
     *
     *     security={{"apiAuth": {} }},
     *
     *     deprecated=false
     * )
     */
    public function add_comment($course_id,Request $request)
    {
        try {

            $auction = ClassRealTime::findOrFail($course_id);
            if ($auction == null) {
                return $this->errorResponse('Subasta no encontrada.', 400);
            }

            $comment = new CommentClassModel();
            $comment->user_id = auth()->user()->id;
            $comment->comment = $request->comment;
            $comment->class_real_time_id = $course_id;
            $comment->type = CommentClassModel::COMMENT;
            $comment->save();

            return $this->successResponse([
                'status' => 201,
                'data' =>
                    $comment
                ,
            ]);


        } catch (\Exception $exception) {
            return $this->errorResponse($exception->getMessage(), 400);
        }
    }

    /**
     * @OA\Delete(
     *     path="/realtime/comment/{comment_id}",
     *     summary="Registrar un comentario",
     *     tags={"Realtime Class"},
     *
     *
     *     @OA\Response(
     *         response=201,
     *         description="OK",
     *     ),
     *
     *     @OA\Response(
     *         response="400",
     *         description="Failed",
     *     ),
     *
     *     security={{"apiAuth": {} }},
     *
     *     deprecated=false
     * )
     */
    public function delete_comment($comment_id)
    {
        try {

            $comment = CommentClassModel::findOrFail($comment_id);
            if ($comment == null) {
                return $this->errorResponse('Comentario no encontrada.', 400);
            }

            if (auth()->user()->id == $comment->user_id ) {
                $comment->delete();
                return $this->successResponse([
                    'status' => 200,
                    'data' =>
                        $comment
                    ,
                ]);
            }
            return $this->errorResponse('No tiene permisos para esta acción.', 400);



        } catch (\Exception $exception) {
            return $this->errorResponse($exception->getMessage(), 400);
        }
    }

    /**
     * @OA\Get(
     *     path="/realtime/apuntes/{course_id}",
     *     summary="Obtener comentarios de un curso",
     *     tags={"Realtime Class"},
     *      @OA\Parameter(
     *          name="course_id",
     *          description="Identificador de cursos",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *     @OA\Response(
     *         response=200,
     *         description="OK",
     *     ),
     *
     *     @OA\Response(
     *         response="400",
     *         description="Failed",
     *     ),
     *
     *
     *     deprecated=false
     * )
     */
    public function get_apunte($course_id,Request $request)
    {
        try {

            $class = ClassRealTime::findOrFail($course_id);
            if ($class == null) {
                return $this->errorResponse('Subasta no encontrada.', 400);
            }

            $count = CommentClassModel::where('class_real_time_id',$course_id)
            ->where('type',CommentClassModel::APUNTE)
            ->count();
            if (isset($request->skip)) {
                $skip = $request->skip;
            }else {
                $skip = 0;
            }
            $limit = $count - $skip; // the limit
            $comments = CommentClassModel::where('type',CommentClassModel::APUNTE)->skip($skip)->take($limit)->get();

            return $this->successResponse([
                'status' => 200,
                'data' =>
                    $comments
                ,
            ]);


        } catch (\Exception $exception) {
            return $this->errorResponse($exception->getMessage(), 400);
        }


    }

    /**
     * @OA\Post(
     *     path="/realtime/apuntes/{course_id}",
     *     summary="Registrar un comentario",
     *     tags={"Realtime Class"},
     *
     *      @OA\Parameter(
     *          name="course_id",
     *          description="Identificador de comentario",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *     @OA\RequestBody(
     *         required=true,
     *         @OA\MediaType(
     *             mediaType="multipart/form-data",
     *             @OA\Schema(
     *                 @OA\Property(
     *                     property="apunte",
     *                     type="string",
     *                 ),
     *
     *                 example={}
     *             )
     *         )
     *     ),
     *
     *     @OA\Response(
     *         response=201,
     *         description="OK",
     *     ),
     *
     *     @OA\Response(
     *         response="400",
     *         description="Failed",
     *     ),
     *
     *     security={{"apiAuth": {} }},
     *
     *     deprecated=false
     * )
     */
    public function add_apunte($course_id,Request $request)
    {
        try {

            $auction = ClassRealTime::findOrFail($course_id);
            if ($auction == null) {
                return $this->errorResponse('Curso no encontrada.', 400);
            }

            $comment = new CommentClassModel();
            $comment->user_id = auth()->user()->id;
            $comment->comment = $request->comment;
            $comment->class_real_time_id = $course_id;
            $comment->type = CommentClassModel::APUNTE;
            $comment->save();

            return $this->successResponse([
                'status' => 201,
                'data' =>
                    $comment
                ,
            ]);


        } catch (\Exception $exception) {
            return $this->errorResponse($exception->getMessage(), 400);
        }
    }

    /**
     * @OA\Delete(
     *     path="/realtime/apuntes/{apuntes_id}",
     *     summary="Registrar un comentario",
     *     tags={"Realtime Class"},
     *
     *
     *     @OA\Response(
     *         response=201,
     *         description="OK",
     *     ),
     *
     *     @OA\Response(
     *         response="400",
     *         description="Failed",
     *     ),
     *
     *     security={{"apiAuth": {} }},
     *
     *     deprecated=false
     * )
     */
    public function delete_apunte($apuntes_id)
    {
        try {

            $comment = CommentClassModel::findOrFail($apuntes_id);
            if ($comment == null) {
                return $this->errorResponse('Apunte no encontrada.', 400);
            }

            if (auth()->user()->id == $comment->user_id ) {
                $comment->delete();
                return $this->successResponse([
                    'status' => 200,
                    'data' =>
                        $comment
                    ,
                ]);
            }
            return $this->errorResponse('No tiene permisos para esta acción.', 400);



        } catch (\Exception $exception) {
            return $this->errorResponse($exception->getMessage(), 400);
        }
    }
}
